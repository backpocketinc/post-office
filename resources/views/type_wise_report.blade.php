@extends('layouts.app')
@section('title', 'Categories')
@section('content')
        <!--Morris Chart CSS -->
<link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css')}}">
<!-- jvectormap -->
<link href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
<style>
    .t-custom{
        font-size: 19px;
        padding-top: 8px;
    }
</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="#">Autoresponders Report</a></li>
                        <li class="active">{{ ucfirst( $type ) }}</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="m-b-5 m-t-10">
                <h2 class="page-title">{{ ucfirst( $data->title ) }} Report -  <span class="label label-rose">{{ ucfirst( $type ) }}</span></h2>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fa fa-envelope-open"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Opened</p>
                            <h3 class="card-title">{{ $open }}</h3>
                        </div>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">mouse</i>
                        </div>
                        <div class="card-content">
                            <p class="category">Clicked</p>
                            <h3 class="card-title">{{ $click }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="fa fa-line-chart"></i>
                        </div>
                        <div class="card-content">
                            <p class="category">Most open & click</p>
                            <p class="card-title m-b-5 m-t-5">
                                <span class="text-muted"> <i class="fa fa-mouse-pointer"></i> <span> {{ $fav_open_time  }} </span> </span>
                                <span class="text-muted"><i class="fas fa-envelope-open"></i><span> {{ $fav_click_time }}</span> </span>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
            <!-- 24 hour performance  -->

            <div class="card">
                <!-- /primary heading -->
                <div class="card-header card-header-text" data-background-color="rose">
                    <h3 class="card-title"> <i class="fa fa-line-chart"></i> 24-hour performance</h3>
                </div>
                <div class="card-content">
                    <div class="text-center">
                        <ul class="list-inline chart-detail-list">
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #7e57c2;"></i>Open</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #34d3eb;"></i>Click</h5>
                            </li>
                        </ul>
                    </div>
                    <div id="morris-line-example" style="height: 300px;"></div>
                </div>
            </div>
                    <!-- Donut Chart -->
            <div class="col-lg-6">
                <div class="row">
                    <div class="card">
                        <!-- /primary heading -->
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title"> Top locations by opens</h3>
                        </div>

                        <div class="card-content">
                            <div class="material-datatables">
                                <table id="load_datatable" class="table table-bordered table-no-bordered table-hover" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>Open</th>
                                        <th>Click</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $count = 0;
                                    if(count($countries) > 0){
                                    foreach( $countries as $key => $value ){

                                    foreach( $value as $index  => $item ){ $count++; ?>
                                    <tr>

                                        <td> <?= $index ?></td>
                                        <td><?= $item['open'] ?></td>
                                        <td><?= $item['click'] ?></td>
                                    </tr>
                                    <?php  } } } ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /Portlet -->
            </div>
                    <!-- stats about pc, mobile, iphone -->
                    <!-- Donut Chart -->
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="card">
                                <!-- /primary heading -->
                                <div class="card-header card-header-text" data-background-color="rose">
                                    <h3 class="card-title"> User Agents </h3>
                                </div>
                                <div class="card-content">
                                    <div class="col-md-6" style="border-right: 1px solid #e1e1e1;">
                                        <div class="portlet-body">
                                            <h4 class="portlet-title text-dark"> Open </h4>
                                            <div id="morris-donut-open" style="height: 300px;"></div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet-body">
                                            <h4 class="portlet-title text-dark"> Click </h4>
                                            <div id="morris-donut-click" style="height: 300px;"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <?php foreach( $agent_label as $value){ ?>
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: <?= $value['color'] ?>"></i> <?= $value['agent'] ?> </h5>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>

                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> Autoresponders World Map Report  </h3>
                    </div>
                    <div class="card-content">
                        <div id="world-map-markers" style="height: 500px"></div>
                    </div>
                </div>

        </div>
    </div>

<!--  Stats Modal -->
<!--  Modal content for the above example -->
<div id="showStatsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center" id="myLargeModalLabel">Large modal</h4>
            </div>
            <div class="modal-body">
               <div id="modal_data"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- world map  -->

<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
{{--<script src="{{ asset('assets/jvectormap/gdp-data.js') }}"></script>--}}

{{--<script src="{{ asset('assets/jvectormap/jvectormap.init.js') }}"></script>--}}
<script>
    $(document).ready(function(){
        // datatable stats
        $('#load_datatable').DataTable({
        });
    });

    function showStat(e){
        var type = $(e).data('type');
        var id = $(e).data('id');
        var action = 'type_wise';
        $('#myLargeModalLabel').text(type + ' Stats');

        $('#loading').show();

        $.post('{{ route('type-wise-stats') }}',
                {id:id, type:type, action:action},
                function(data){

                    $('#modal_data').html(data);

                    $('#showStatsModal').modal('show');

                    $('#loading').hide();
                });
    }

    var marker = {!! $world_json !!}

    ! function($) {
        "use strict";

        var VectorMap = function() {
        };

        VectorMap.prototype.init = function() {
            //various examples
            $('#world-map-markers').vectorMap({
                map : 'world_mill_en',
                scaleColors : ['#ea6c9c', '#ea6c9c'],
                normalizeFunction : 'polynomial',
                hoverOpacity : 0.7,
                hoverColor : false,
                regionStyle : {
                    initial : {
                        fill : '#5fbeaa'
                    }
                },
                markerStyle: {
                    initial: {
                        r: 9,
                        'fill': '#a288d5',
                        'fill-opacity': 0.9,
                        'stroke': '#fff',
                        'stroke-width' : 7,
                        'stroke-opacity': 0.4
                    },

                    hover: {
                        'stroke': '#fff',
                        'fill-opacity': 1,
                        'stroke-width': 1.5
                    }
                },
                backgroundColor : 'transparent',


                markers : marker
            });
            },
            //init
                $.VectorMap = new VectorMap, $.VectorMap.Constructor =
                VectorMap
    }(window.jQuery),

//initializing
            function($) {
                "use strict";
                $.VectorMap.init()
            }(window.jQuery);
</script>

<!--Morris Chart-->
<script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/plugins/morris/raphael-min.js') }}"></script>
<script>
    var chart_data = {!! $chart !!};

    !function($) {
        "use strict";
        var MorrisCharts = function() {};

        // donut chart
        //creates Donut chart
        MorrisCharts.prototype.createDonutChart = function(element, data, colors) {
            Morris.Donut({
                element: element,
                data: data,
                resize: true, //defaulted to true
                colors: colors
            });
        },
        //creates line chart
        MorrisCharts.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
            Morris.Line({
                element: element,
                data: data,
                xkey: xkey,
                ykeys: ykeys,
                labels: labels,
                fillOpacity: opacity,
                pointFillColors: Pfillcolor,
                pointStrokeColors: Pstockcolor,
                behaveLikeLine: true,
                gridLineColor: '#eef0f2',
                hideHover: 'auto',
                resize: true, //defaulted to true
                lineColors: lineColors
            });
        },
                MorrisCharts.prototype.init = function() {

                    //create line chart
                    var $data  = chart_data;
                    this.createLineChart('morris-line-example', $data, 'hour', ['a', 'b'], ['Opens', 'Clicks'],['0.1'],['#ffffff'],['#999999'], ['#7e57c2', '#34d3eb']);

        //creating donut chart
        var $donutOpen = <?= $user_agent_open ?> ;
        var $donutClick = <?= $user_agent_click ?> ;
        this.createDonutChart('morris-donut-open', $donutOpen, [
            <?php foreach( $agent_label as $value){ ?>
                <?= "'". $value['color'] . "',";  ?>
            <?php } ?>
        ]);

        this.createDonutChart('morris-donut-click', $donutClick, [
            <?php foreach( $agent_label as $value){ ?>
                <?= "'". $value['color'] . "',";  ?>
            <?php } ?>

        ]);
    },
            //init
        $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
    }(window.jQuery),

//initializing
            function($) {
                "use strict";
                $.MorrisCharts.init();
            }(window.jQuery);
</script>
@endsection