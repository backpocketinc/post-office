
<div class="card">
    <div class="card-header card-header-tabs" data-background-color="rose">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <span class="nav-tabs-title">Trending Subjects</span>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#today" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-home"></i></span>
                            <span class="hidden-xs">Today</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#last7days" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-user"></i></span>
                            <span class="hidden-xs">Last Seven Days</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#lastmonth" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                            <span class="hidden-xs">Last Month</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-content">
        <div class="tab-content">
            <div class="tab-pane active" id="today">
                <div class="all_comp material-datatables" id="">
                    <table class="table table-colored table-inverse table-hover table-striped table-bordered  no-footer" width="100%">
                        <thead>
                        <tr>
                            <th width="65%">Subject</th>
                            @if(Auth::User()->type=='a')
                                <th width="100px">Use Counter</th>
                                <th width="200px">Latest Use</th>
                            @endif
                            <th width="150px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($topSubjects_today) >0 ){
                        foreach( $topSubjects_today as $value ){ ?>
                        <tr>
                            <td><?= $value->subject ?></td>
                            @if(Auth::User()->type=='a')
                                <td><?= $value->use_counter ?></td>
                                <td><span class="label label-primary"><?= $value->updated_at ?></span></td>
                            @endif
                            <td><button class="btn btn-xs btn-purple"  data-id="<?= $value->id ?>" title="Use Me" data-toggle="tooltip" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button></td>
                        </tr>
                        <?php }
                        }else{ ?>
                        <tr>
                            <td colspan="4">No Record found yet.</td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="last7days">
                <div class="all_comp" id="">
                    <table class="table table-colored table-inverse table-hover table-striped table-bordered  no-footer">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            @if(Auth::User()->type=='a')
                                <th>Use Counter</th>
                                <th>Latest Use</th>
                            @endif
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($topSubjects_last7Days) >0 ){
                        foreach( $topSubjects_last7Days as $value ){ ?>
                        <tr>
                            <td><?= $value->subject ?></td>
                            @if(Auth::User()->type=='a')
                                <td><?= $value->use_counter ?></td>
                                <td><span class="label label-primary"><?= $value->updated_at ?></span></td>
                            @endif
                            <td><button class="btn btn-xs btn-purple"  data-id="<?= $value->id ?>" title="Use Me" data-toggle="tooltip" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button></td>
                        </tr>
                        <?php }
                        }else{ ?>
                        <tr>
                            <td colspan="4">No Record found yet.</td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="lastmonth">
                <div class="all_comp" id="">
                    <table class="table table-colored table-inverse table-hover table-striped table-bordered  no-footer">
                        <thead>
                        <tr>
                            <th>Subject</th>
                            @if(Auth::User()->type=='a')
                                <th>Use Counter</th>
                                <th>Latest Use</th>
                            @endif
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($topSubjects_lastMonth) >0 ){
                        foreach( $topSubjects_lastMonth as $value )
                        { ?>
                        <tr>
                            <td><?= $value->subject ?></td>
                            @if(Auth::User()->type=='a')
                                <td><?= $value->use_counter ?></td>
                                <td><span class="label label-primary"><?= $value->updated_at ?></span></td>
                            @endif
                            <td><button class="btn btn-xs btn-purple"  data-id="<?= $value->id ?>" data-toggle="tooltip" title="Use Me" onclick="submitUseMe(this)" ><i class="material-icons">mouse</i></button></td>
                        </tr>
                        <?php }
                        }else{ ?>
                        <tr>
                            <td colspan="4">No Record found yet.</td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <form action="{!! Route('new-template.create') !!}" method="get" style="display: none;" id="use_me">
                    <input type="hidden" name="use_me" value="">
                </form>
                <script>
                    function submitUseMe(x){
                        $('input[name="use_me"]').val($(x).data('id'));
                        $('#use_me').submit();
                    }
                </script>
            </div>
        </div>
    </div>
</div>

<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>