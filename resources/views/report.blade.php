@extends('layouts.app')
@section('title', 'Report')
@section('content')
    <style>
        .widget-box-two .widget-two-icon{
            border: none !important;;
        }
    </style>

    <div class="content">
        <!-- Start content -->
        <div class="content-fluid">
            <div class="col-xs-12">
                <div class="page-title-box">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="#">Autoresponders Report </a></li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card-box widget-box-two widget-two-brown">
                    <i class="fa fa-clock-o widget-two-icon"></i>
                    <div class="wigdet-two-content text-white">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" data-toggle="tooltip" title="Total Subjects">Most open time</p>
                        <h2 class="text-white"><span data-plugin="counterup">{{ $fav_open_time  }}</span></h2>
                        <!--<p class="m-0"><b>Last:</b> 30.4k</p>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card-box widget-box-two widget-two-primary">
                    <i class="fa fa-clock-o widget-two-icon"></i>
                    <div class="wigdet-two-content text-white">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" data-toggle="tooltip" title="Total Subjects">Most click time</p>
                        <h2 class="text-white"><span data-plugin="counterup">{{ $fav_click_time  }}</span></h2>
                        <!--<p class="m-0"><b>Last:</b> 30.4k</p>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card-box">
                    <div class="row">

                        <div class="col-xs-12 bg-white">
                            <h3> Autoresponders Report  </h3>
                            <div class="table-responsive">
                                <table id="load_datatable" class="table table-bordered table-no-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>subject</th>
                                    <th>Autoresponder</th>
                                    <th>Action</th>
                                    <th>Country</th>
                                    <th>IP</th>
                                    <th>City</th>
                                    <th>Region</th>
                                    <th>longituhe</th>
                                    <th>Latituhe</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="4">No Record found yet.</td>
                                </tr>
                                </tbody>

                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function(){
                // datatable

                var table =  $('#load_datatable').DataTable({
                    "pageLength":10,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                    responsive:true,

                    ajax: "{!! Route('load-emailtracking-stats') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'subject', name: 'subject'},
                        {data: 'type', name: 'type'},
                        {data: 'stat_type', name: 'stat_type'},
                        { data: 'country', name: 'country' },
                        { data: 'ip', name: 'ip' },
                        { data: 'city', name: 'city' },
                        { data: 'region', name: 'region' },
                        { data: 'longi', name: 'longi' },
                        { data: 'lati', name: 'lati' },
                        { data: 'date_time', name: 'date_time' },
                        //{ data: 'user_agent', name: 'user_agent' },
                        //{ data: 'referrer', name: 'referrer' },



                    ]

                });
            });
        </script>



    </div>




@endsection
