
@extends('layouts.app')
@section('title', 'Users')
@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Broadcast Opened By Emails Report </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> Opened By Emails</h3>
                    </div>
                    <div class="card-content">
                        <div class="col-sm-12 sendemail hidden">
                            <div class="btn-group pull-right">
                                <button onclick="openedEmail()" type="button" class="btn btn-success  "><i class="fa fa-send"></i> Send Email</button>
                            </div>
                        </div>
                        <div class="material-datatables">
                            <table id="load_datatable" class="table table-bordered table-no-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="{!! Route('new-template.create') !!}" method="get" id="OpenEmails">
            <input type="hidden" name="open" value="{{ $id }}">
        </form>
    </div>
    <script>

        function openedEmail(){
            $('#OpenEmails').submit();
        }

        $(document).ready(function(){

            $('#load_datatable').DataTable({

                "pageLength":25,
                "order": [[0, 'desc']],
                processing: true,
                serverSide: true,
                responsive:true,
                "initComplete": function (settings, json) {
                    if(json.data.length >0 ){
                        $('.sendemail').removeClass('hidden');
                    }
                },

                ajax: "{!! Route('load-open-by_emails') !!}?camp_title_id={{$id}}",
                columns: [
                    {data: 'id', name: 'autoresponder_new_stats.id'},
                    {data: 'email', name: 'autoresponder_new_stats.email'},
                    {data: 'type', name: 'autoresponder_new_stats.type'},

                    //{ data: 'updated_at', name: 'updated_at' }
                ]
            });
        });
   </script>

@endsection
