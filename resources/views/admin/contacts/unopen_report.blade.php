@extends('layouts.app')
@section('title', 'Users')
@section('content')
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Broadcast Unopened By Emails Report </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header card-header-text" data-background-color="rose">
                        <h3 class="card-title"> Unopened By Emails</h3>
                    </div>
                    <div class="card-content">
                        <div class="col-sm-12 sendemail hidden">
                            <div class="btn-group pull-right">
                                <button onclick="unopenedEmail()" type="button" class="btn btn-success  "><i class="fa fa-send"></i> Send Email </button>
                            </div>
                        </div>
                        <div class="material-datatables">
                            <table id="load_datatable_unopen" class="table table-bordered table-no-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<form action="{!! Route('new-template.create') !!}" method="get" id="unOpenEmails">
    <input type="hidden" name="unopen" value="{{ $id }}">
</form>

    <script>
        function unopenedEmail(){
            $('#unOpenEmails').submit();
        }

        $(document).ready(function(){
            $('#load_datatable_unopen').DataTable({
                "pageLength":25,
                "order": [[0, 'desc']],
                processing: true,
                serverSide: true,
                "initComplete": function (settings, json) {
                    if(json.data.length >0 ){
                        $('.sendemail').removeClass('hidden');
                    }
                },
                ajax: "{!! Route('load-unopen-by_emails') !!}?camp_title_id={{$id}}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'email', name: 'email'},
                    {data: 'type', name: 'type'},
                    //{ data: 'updated_at', name: 'updated_at' }
                ]
            });
        });
    </script>

@endsection
