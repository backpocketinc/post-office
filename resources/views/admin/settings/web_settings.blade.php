@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-colorpicker.min.css') }}">
<style>
    .thumbnail{
        background-color: lightgrey;
    }
</style>
<div class="content">
    <!-- Start content -->
    <div class="container-fluid">
        <div class="col-xs-12">
            <div class="page-title-box pull-right">
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                    <li> <a href="#">Web Settings</a></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <script>
                    swal('Success', '{{ session("success") }}', 'success');
                </script>

            @endif

            @if (session('error'))
                    <script>
                        swal('Error', '{{ session("error") }}', 'error');
                    </script>
            @endif
            <div class="col-xs-12">
                <div class="row">
                    <div class="card">
                        <div class="card-header card-header-text" data-background-color="rose">
                            <h3 class="card-title">Application Settings</h3>
                        </div>
                        <div class="card-content">
                            <form method="post" enctype="multipart/form-data" onsubmit="swal('alert', 'Settings disabled for demo', 'warning');  return false;">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $data->id }}">
                                <div class="col-md-12">

                                    <div class="col-md-3 col-sm-3">
                                        <p class="text-center">Logo</p>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail">
                                                <img src="{{ isset($data->logo) && $data->logo != '' ? asset('assets/img/app/'.$data->logo) : asset('assets/images/logo/wordmarkwhite-40.png') }}" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                            <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select Image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="logo" data-field="login_bg" />
                                        </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-3">
                                        <p class="text-center"> Small Logo </p>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail">
                                                <img src="{{ isset($data->logo_sm) && $data->logo_sm != ''? asset('assets/img/app/'.$data->logo_sm) : asset('assets/images/logo/iconwhite-40.png') }}" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                            <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select Image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="logo_sm" data-field="login_bg" />
                                        </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-3">
                                        <p class="text-center"> Favicon </p>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail">
                                                <img src="{{ isset($data->favicon) && $data->favicon !='' ? asset('assets/img/app/'.$data->favicon) : asset('assets/img/apple-icon.png')   }}" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                            <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select Image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="favicon" data-field="login_bg" />
                                        </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-12">
                                <div class="col-md-3 col-sm-3">
                                    <p class="text-center">Login</p>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="{{ isset($data->login_bg) && $data->login_bg ? asset('assets/img/app/'.$data->login_bg) : asset('assets/img/login.jpeg')   }}" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="login_bg" data-field="login_bg" />
                                        </span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3">
                                    <p class="text-center">Register </p>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="{{ isset($data->register_bg) && $data->register_bg ? asset('assets/img/app/'.$data->register_bg) : asset('assets/img/register.jpeg') }}"  alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="register_bg" data-field="register_bg" />
                                        </span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <p class="text-center">Forget Password</p>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="{{ isset($data->forgot_password_bg) && $data->forgot_password_bg ? asset('assets/img/app/'.$data->forgot_password_bg) : asset('assets/img/forget.jpeg') }}"  alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="forgot_password_bg" data-field="forgot_password_bg" />
                                        </span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <p class="text-center">Sidebar</p>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="{{ isset($data->sidebar_bg) && $data->sidebar_bg ? asset('assets/img/app/'.$data->sidebar_bg) : asset('assets/img/sidebar.jpg') }}"  alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                        <span class="btn btn-rose btn-round btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="sidebar_bg" data-field="sidebar_bg" />
                                        </span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">

                                    <div class="form-group account-btn m-t-10">
                                        <label class="col-xs-2 control-label">Body background:</label>
                                        <div data-color-format="rgb" data-color="#{{ $data->body_bg != '' ? $data->body_bg : '#EEEEEE' }}" class="colorpicker-default input-group col-xs-10">
                                            <input type="text" name="body_bg" readonly="readonly" value="#{{ $data->body_bg != '' ? $data->body_bg : '#EEEEEE' }}" class="form-control">
                                            <span class="input-group-btn add-on">
                                                <button class="btn btn-white" type="button">
                                                    <i style="background-color: #{{ $data->body_bg }};margin-top: 2px;"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group account-btn m-t-10">
                                        <label class="col-xs-2 control-label">Sidebar background:</label>
                                        <div class="radio radio-inline ">
                                            <label for="white">
                                                <input type="radio" id="white" name="sidebar_bg_color" value="white" {{ $data->sidebar_bg_color == 'white' ? 'checked' : '' }} > White
                                            </label>
                                            <label for="black">
                                                <input type="radio" id="black" name="sidebar_bg_color"  value="black" {{ $data->sidebar_bg_color == 'black' || $data->sidebar_bg_color == '' ? 'checked' : '' }}> Black
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group account-btn m-t-10">
                                        <label class="col-xs-2 control-label">Sidebar nav active:</label>
                                        <div class="radio radio-inline ">

                                            <label for="purple">
                                                <input type="radio" id="purple" name="sidebar_active" value="purple" {{ $data->sidebar_active == 'purple'? 'checked' : '' }} > Purple
                                            </label>
                                            <label for="blue">
                                                <input type="radio" id="blue" name="sidebar_active"  value="blue" {{ $data->sidebar_active == 'blue'? 'checked' : '' }}> Blue
                                            </label>

                                            <label for="green">
                                                <input type="radio" id="green" name="sidebar_active"  value="green" {{ $data->sidebar_active == 'green'? 'checked' : '' }}> Green
                                            </label>

                                            <label for="orange">
                                                <input type="radio" id="orange" name="sidebar_active"  value="orange" {{ $data->sidebar_active == 'orange'? 'checked' : '' }}> Orange
                                            </label>

                                            <label for="red">
                                                <input type="radio" id="red" name="sidebar_active"  value="red" {{ $data->sidebar_active == 'red'? 'checked' : '' }}> Red
                                            </label>

                                            <label for="rose">
                                                <input type="radio" id="rose" name="sidebar_active"  value="rose" {{ $data->sidebar_active == 'rose' || $data->sidebar_active == '' ? 'checked' : '' }}> Rose
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-success pull-right"> Save </button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="{{ asset('assets/js/bootstrap-colorpicker.min.js') }}"></script>
    <!--  add category -->
    <script>
        $(document).ready(function() {
            // color picker
            $('.colorpicker-default').colorpicker({
                format: 'hex'
            });
            // ajax submit form

        });

    </script>

@endsection
