@extends('layouts.app')
@section('title', 'Categories')
@section('content')
    <style>
        form#add_category .loader {
            float: right;
            margin-left: 5px;
            margin-top: 6px;

        }
        form#add_category .loader img{
            display: none;
        }
        .add_more{
            float: right;
            margin-bottom: 5px;;
        }
        span a{ color: white; }
        .modal-lg{ width:95% }
    </style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">

            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Autoresponder Settings </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif


                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Settings</h3>
                    </div>
                    <div class="panel-body">


                            <ul class="nav nav-pills nav-pills-warning">
                                <li class="active"><a data-toggle="tab" href="#aweber_tab">Aweber</a></li>
                                <li><a data-toggle="tab" href="#get_response_tab">Get Response</a></li>
                                <li><a data-toggle="tab" href="#mailchimp_tab">Mail Chimp</a></li>
                                <!--<li><a data-toggle="tab" href="#sendlane_tab">Sendlane</a></li>-->
                                <li><a data-toggle="tab" href="#activecampaign_tab">Active Campaign</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="aweber_tab" class="tab-pane fade in active">

                                    <!--  ************** Aweber Information ***************** -->
                                    <div class="form-group">
                                        <div class="alert alert-info">
                                            Aweber IAccount Detail                                                <!--<span><a href="{!! url('help') !!}" target="_blank" ><small><u>Help <i class="fa fa-question-circle"></i></u></small></a></span>-->
                                            <i><a href="javascript:void(0)" id="aweberHelp"  ><small><u>step by step instructions <i class="fa fa-question-circle"></i></u></small></a></i>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-purple  " data-toggle="modal" data-target="#aweberModal" >Add More</button>
                                    </div>
                                    <table class="table table-response table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Consumer Key</th>
                                            <th>Consumer Secret</th>
                                            <th>Token Info</th>
                                            <th>Status</th>
                                            <th colspan="2">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        if(count($aweber_data)>0){
                                        foreach($aweber_data as $k=>$v){
                                        ?>
                                        <tr>
                                            <td class="aweber_{!! $v['id'] !!}">{!! $v['account_title'] !!}</td>
                                            <td>{!! '**************'. substr( $v['aweber_key'], -4 ) !!}</td>
                                            <td>{!! '**************'.substr( $v['aweber_secret'], -4) !!}</td>
                                            <td>
                                                <div class="col-md-10">
                                                    <button class="btn btn-xs btn-bordered btn-warning  " data-id="{!! $v['id'] !!}" onclick="GetAccessToken(this)" type="button">Get Token</button>
                                                    <a href="//www.aweber.com/users/" target="_blank" class="btn btn-xs btn-bordered btn-primary  " >Go To  Aweber</a>
                                                    <?php
                                                    if(isset($v['aweber_token'])){
                                                    if($v['aweber_token']== null || $v['accessTokenKey']==null){
                                                    ?>
                                                    <p class="help-text">
                                                        <small>
                                                            <span>Click on Get Token, as you fill key and secret.</span><br>
                                                            <span class="text-danger">Aweber Access Token is not generated.</span>
                                                        </small>
                                                    </p>
                                                    <?php
                                                    }else{
                                                    ?>
                                                    <p class="help-text text-success"><small>Aweber Access Token is generated and saved.</small></p>
                                                    <?php
                                                    }
                                                    }else{
                                                    ?>
                                                    <p class="help-text text-danger"><small>Aweber Access Token is not generated.</small></p>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                            <td> <i class="{!! $v['status'] !!}"></i></td>
                                            <td with="100px">

                                                <a onclick="deleteRow(this)" data-id="{!! $v['id'] !!}" data-obj="aweber_settings" href="javascript:;" data-toggle="tooltip" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                <button onclick="import_emails(this)" data-class="aweber_{!! $v['id'] !!}" data-toggle="tooltip" data-title="Aweber" data-id="{!! $v['id'] !!}" data-obj="aweber_settings" data-toggle="tooltip" title="Import emails from Aweber account" class="btn btn-xs btn-info"> <i class="fa fa-download"></i> </button>


                                            </td>

                                        </tr>
                                        <?php
                                        }
                                        }else{
                                            echo '<tr><td colspan="6">No data found</td></tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <div id="aweberModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                    <h4 class="modal-title text-center">Add/Update Aweber</h4>
                                                </div>

                                                <form class="form-horizontal" role="form" id="aweber_form" >

                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="">
                                                    <input type="hidden" name="object" value="aweber_settings">

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="account_title" class="col-md-12">Title *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="account_title" id="account_title" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="aweber_key" class="col-md-12">Consumer Key *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text"  name="aweber_key" id="aweber_key" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="aweber_secret" class="col-md-12">Consumer Secret *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="aweber_secret" id="aweber_secret" class="form-control" value="" required>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="modal-footer">
                                                        <div class="pull-left">
                                                            <input class="imp_category" type="hidden" name="import_category" value="">
                                                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-success btn-xs">Save</button>
                                                            <button type="button" data-modal="aweberModal" data-form="aweber_form" data-heading="Aweber" onclick="save_and_import_emails(this)" class="btn btn-linkedin btn-xs">Save and Import Emails</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                                <div id="get_response_tab" class="tab-pane fade">
                                    <!--  ************** Get Response Information ***************** -->
                                    <div class="form-group">
                                        <div class="alert alert-info">
                                            Get Response IAccount Detail                                                <!--<span><a href="{!! url('help') !!}" target="_blank" ><small><u>Help <i class="fa fa-question-circle"></i></u></small></a></span>-->
                                            <i><a href="javascript:void(0)" id="getresHelp"  ><small><u>step by step instructions <i class="fa fa-question-circle"></i></u></small></a></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-purple  " data-toggle="modal" data-target="#getResponseModal" >Add More</button>

                                    </div>
                                    <table class="table table-response table-striped  table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>API Key</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(count($get_response_data)>0){
                                        foreach($get_response_data as $k=>$v){
                                        ?>
                                        <tr>
                                            <td class="getresponse_{!! $v['id'] !!}">{!! $v['account_title'] !!}</td>
                                            <td>{!! '**************'. substr( $v['get_response_key'], -4) !!}</td>
                                            <td> <i class="{!! $v['status']  !!}"></i></td>
                                            <td>
                                                <a onclick="deleteRow(this)" data-id="{!! $v['id'] !!}" data-obj="getresponse_settings" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                <button onclick="import_emails(this)" data-class="getresponse_{!! $v['id'] !!}" data-toggle="tooltip" data-title="Getresponse" data-id="{!! $v['id'] !!}" data-obj="getresponse_settings" data-toggle="tooltip" title="Import emails from Getresponse account" class="btn btn-xs btn-info"> <i class="fa fa-download"></i> </button>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        }else{
                                            echo '<tr><td colspan="4">No data found</td></tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                    <div id="getResponseModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                    <h4 class="modal-title text-center">Add/Update Get Response</h4>
                                                </div>

                                                <form class="form-horizontal" role="form" id="getresponse_form" >

                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="">
                                                    <input type="hidden" name="object" value="getresponse_settings">

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="account_title" class="col-md-12">Title *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="account_title" id="account_title" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="get_response_key" class="col-md-12">API Key *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="get_response_key" id="get_response_key" class="form-control" required value="" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <div class="pull-left">
                                                            <input class="imp_category" type="hidden" name="import_category" value="">
                                                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-success btn-xs">Save</button>
                                                            <button type="button" data-modal="getResponseModal" data-form="getresponse_form" data-heading="Get Response" onclick="save_and_import_emails(this)" class="btn btn-linkedin btn-xs">Save and Import Emails</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

                                </div>
                                <div id="mailchimp_tab" class="tab-pane fade">

                                    <!--  ************** MailChimp Information ***************** -->
                                    <div class="form-group">
                                        <div class="alert alert-info">
                                            MailChimp IAccount Detail                                                <!--<span><a href="{!! url('help') !!}" target="_blank" ><small><u>Help <i class="fa fa-question-circle"></i></u></small></a></span>-->
                                            <i><a href="javascript:void(0)" id="mailchimpHelp"  ><small><u>step by step instructions <i class="fa fa-question-circle"></i></u></small></a></i>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-purple  " data-toggle="modal" data-target="#mailChimpModal" >Add More</button>

                                    </div>
                                    <table class="table table-response table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>API Key</th>
                                            <th> Status </th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(count($mailchimp_data)>0){
                                        foreach($mailchimp_data as $k=>$v){
                                        ?>
                                        <tr>
                                            <td class="mailchimp_{!! $v['id'] !!}">{!! $v['account_title'] !!}</td>
                                            <td>{!! '**************'. substr( $v['mailchimp_key'], -4) !!}</td>
                                            <td> <i class="{!! $v['status'] !!}"></i></td>
                                            <td>
                                                <a onclick="deleteRow(this)" data-id="{!! $v['id'] !!}" data-obj="mailchimp_settings" href="javascript:;" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                <button onclick="import_emails(this)" data-class="mailchimp_{!! $v['id'] !!}"  data-title="Mailchimp" data-id="{!! $v['id'] !!}" data-obj="mailchimp_settings" data-toggle="tooltip" title="Import emails from Mailchimp account" class="btn btn-xs btn-info"> <i class="fa fa-download"></i> </button>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        }else{
                                            echo '<tr><td colspan="4">No data found</td></tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <div id="mailChimpModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                    <h4 class="modal-title text-center">Add/Update MailChimp</h4>
                                                </div>

                                                <form class="form-horizontal" role="form" id="mailchimp_form" >
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="">
                                                    <input type="hidden" name="object" value="mailchimp_settings">

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="account_title" class="col-md-12">Title *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="account_title" id="account_title" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="mailchimp_key" class="col-md-12">API Key *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="mailchimp_key" id="mailchimp_key" class="form-control" required value="" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <div class="pull-left">
                                                            <input class="imp_category" type="hidden" name="import_category" value="">
                                                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-success btn-xs">Save</button>
                                                            <button type="button" data-modal="mailChimpModal" data-form="mailchimp_form" data-heading="Mailchimp" onclick="save_and_import_emails(this)" class="btn btn-linkedin btn-xs">Save and Import Emails</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

                                </div>

                                <div id="activecampaign_tab" class="tab-pane fade">

                                    <!--  ************** activecampaign Information ***************** -->
                                    <div class="form-group">
                                        <div class="alert alert-info">
                                            Active Campaign Account Detail
                                        <!--<span><a href="{!! url('help') !!}" target="_blank" ><small><u>Help <i class="fa fa-question-circle"></i></u></small></a></span>-->
                                            <i><a href="javascript:void(0)" id="activeHelp"><small><u>step by step instructions <i class="fa fa-question-circle"></i></u></small></a></i>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-purple  " data-toggle="modal" data-target="#activecampaignModal" >Add More</button>

                                    </div>
                                    <table class="table table-response table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Domain</th>
                                            <th>API Key</th>
                                            <th>Status</th>
                                            <th width="80">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        if(count($active_campaign)>0){
                                        foreach($active_campaign as $k=>$v){
                                        ?>
                                        <tr>
                                            <td class="activecampaign_{!! $v['id'] !!}">{!! $v['account_title'] !!}</td>
                                            <td>{!! $v['domain'] !!}</td>
                                            <td>{!! '**************'. substr( $v['key'], -4) !!}</td>
                                            <td> <i class="{{ $v['status'] }}"></i> </td>
                                            <td>
                                                <a onclick="deleteRow(this)" data-id="{!! $v['id'] !!}" data-obj="activecampaign_settings" href="javascript:;" title="Delete" data-toggle="tooltip" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                <button onclick="import_emails(this)" data-class="activecampaign_{!! $v['id'] !!}" data-title="Active Campaign" data-id="{!! $v['id'] !!}" data-obj="activecampaign_settings" title="Import emails from activecampaign account" class="btn btn-xs btn-info"> <i class="fa fa-download"></i> </button>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        }else{
                                            echo '<tr><td colspan="6">No data found</td></tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <!-- activecampaign modal -->
                                    <div id="activecampaignModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                    <h4 class="modal-title text-center">Add/Update Active Campaign</h4>
                                                </div>

                                                <form class="form-horizontal" role="form" id="activecampaign_form" name="activeampaign">

                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="">
                                                    <input type="hidden" name="object" value="activecampaign_settings">

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="account_title" class="col-md-12">Title *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="account_title" id="account_title" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="activecampaign_domain" class="col-md-12">Url *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="activecampaign_domain" id="activecampaign_domain" class="form-control" required value="" >
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="activecampaign_key" class="col-md-12">API Key *:</label>
                                                            <div class="col-md-12">
                                                                <input type="text" name="activecampaign_key" id="activecampaign_key" class="form-control" required value="" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="pull-left">
                                                            <input class="imp_category" type="hidden" name="import_category" value="">
                                                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                                                            <button type="submit" class="btn btn-success btn-xs">Save</button>
                                                            <button type="button" data-modal="activecampaignModal" data-form="activecampaign_form" data-heading="Activecampaign" onclick="save_and_import_emails(this)" class="btn btn-linkedin btn-xs">Save and Import Emails</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- common modal for import settings -->
        <div id="importSettingsModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h4 class="modal-title text-center" id="import_pop_title"></h4>
                    </div>
                    <form class="import_settings" role="form" id="importsettings_form" >
                        {{ csrf_field() }}
                        <input type="hidden" class="account_id" name="account">
                        <input type="hidden" class="target" name="target">

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="category" class="col-md-12">Category:</label>
                                <div class="col-md-12">
                                    <input type="text" name="category" id="category" class="form-control" placeholder="Enter category title" value="" required>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="modal-footer">
                            <div class="pull-left">
                                <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-success btn-xs">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.modal -->

        <!-- common modal for share emails -->
        <div id="shareSettingsModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h4 class="modal-title text-center" id="share_pop_title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category" class="col-md-12">Sharing Code:</label>
                            <div class="col-md-6">
                                <p id="created_code"></p>
                            </div>
                            <div class="col-md-6">
                                <button onclick="copyToClipboard('hidden_code')" type="button" class="btn btn-primary btn-sm clipboard"> Copy</button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <input type="hidden" value="" id="hidden_code">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->

        <!-- help moda -->
        <div id="activehelpModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h3 class="modal-title text-center">Active Campaign step by step instructions</h3>
                    </div>
                    <div class="modal-body">
                        <p>Follow following steps to create Active Campaign account & Api access</p>
                        <h4>Step 1:</h4>
                        <p> Hit this link to <a href="http://www.activecampaign.com/free/" target="_blank">Create account</a></p>
                        <p><img src="{{ asset('assets/images/help/active-create-account.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <p> Complete your first step by adding your company name and email address then hit Create Account button </p>
                        <h4>Step 2:</h4>

                        <p>Create list & add contacts </p>
                        <p><img src="{{ asset('assets/images/help/active-create-list.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>
                        <ol>
                            <li> Create list </li>
                            <li> Add contacts to list </li>
                        </ol>

                        <h4>Step 3:</h4>
                        <p> Get Api credentials </p>
                        <p><img src="{{ asset('assets/images/help/active-api-credentials.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>
                        <ol>
                            <li> Click on your account link.</li>
                            <li> Click on My Settings.</li>
                            <li> Click on Developer.</li>
                            <li> copy url and api key.</li>
                        </ol>
                        <h4>Step 4:</h4>
                        <p><img src="{{ asset('assets/images/help/active-campaign-add-account.png') }}" alt="" class="img-responsive img-thumbnail" > </p>

                        <h4>Done</h4>

                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->

        <div id="getresponseModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h3 class="modal-title text-center">GetResponse step by step instructions</h3>
                    </div>
                    <div class="modal-body">
                        <p>Follow following steps to create GetResponse account & Api access</p>
                        <h4>Step 1:</h4>
                        <p> Hit this link to <a href="https://www.getresponse.com/" target="_blank">Create account</a></p>
                        <p><img src="{{ asset('assets/images/help/get-response-1.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <p> Complete your first step by adding your company email address and password then hit Sign Up button.</p>
                        <p> Getresponse system will send you active account email. Active your account.</p>
                        <h4>Step 2:</h4>

                        <p>Complete account information </p>
                        <p><img src="{{ asset('assets/images/help/getresponse-complete-account.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>

                        <h4>Step 3:</h4>
                        <p> Verify your account </p>
                        <p><img src="{{ asset('assets/images/help/getres-add-mobile-code.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <p> verify your account by submitting code send to your mobile number.</p>
                        <br>

                        <h4>Step 4:</h4>
                        <p> Get Api key</p>
                        <p><img src="{{ asset('assets/images/help/getresponse-copy-apikey.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>
                        <ol>
                            <li>Click on My Account</li>
                            <li>Click on Account Detail</li>
                            <li>Click on API & OAuth</li>
                            <li>Copy Api key </li>
                        </ol>
                        <h4>Step 5:</h4>
                        <p> Add Api key</p>
                        <p><img src="{{ asset('assets/images/help/add_apikey.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>
                        <p> Add title and api key then save it. </p>
                        <h4>Done</h4>

                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        <div id="mailchimpModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h3 class="modal-title text-center">Mailchimp step by step instructions</h3>
                    </div>
                    <div class="modal-body">
                        <p>Follow following steps to create Mailchimp account & Api access</p>
                        <h4>Step 1:</h4>
                        <p> Hit this link to <a href="https://www.getresponse.com/" target="_blank">Create account</a></p>
                        <p><img src="{{ asset('assets/images/help/mailchimp-signup.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <p> Complete your first step by adding your email address , username, password then hit Get Started button.</p>
                        <p> Mailchimp system will send you active account email. Active your account.</p>
                        <p> Complete all Following steps </p>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="{{ asset('assets/images/help/mailchimp-first-lastname.png') }}" alt="" class="img-responsive img-thumbnail" >
                                <img src="{{ asset('assets/images/help/mailchimp-companyname.png') }}" alt="" class="img-responsive img-thumbnail" >
                            </div>
                            <div class="clearfix"></div>
                            <p></p>
                            <div class="col-md-12">
                                <img src="{{ asset('assets/images/help/mailchimp-add-address.png') }}" alt="" class="img-responsive img-thumbnail" >
                                <img src="{{ asset('assets/images/help/mailchimp-selling.png') }}" alt="" class="img-responsive img-thumbnail" >
                            </div>
                        </div>

                        <br>
                        <p></p>
                        <p></p>
                        <ol>
                            <li>Add first name and last name</li>
                            <li>Tell us about your business</li>
                            <li>Add your address</li>
                            <li>Does your business sell anything online?</li>
                            <li>Connect social media</li>
                        </ol>

                        <h4>Step 2:</h4>
                        <p> Creat Api key</p>
                        <div class="col-md-12">

                            <img src="{{ asset('assets/images/help/mailchimp-get-api.png') }}" alt="" class="img-responsive img-thumbnail" >
                            <p></p>
                            <p></p>

                            <img src="{{ asset('assets/images/help/mailchimp-copy-api.png') }}" alt="" class="img-responsive img-thumbnail" >

                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <p></p>
                        <p></p>
                        <p></p>
                        <ol>
                            <li>Click on My Account</li>
                            <li>Click on Account</li>
                            <li>Click Extras</li>
                            <li>Click Api keys</li>
                            <li>Click create Api key </li>
                            <li>Copy Api key </li>
                        </ol>
                        <br>
                        <h4>Step 3:</h4>
                        <p> Add Api key</p>
                        <p><img src="{{ asset('assets/images/help/mailchimp-saveit.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                        <br>
                        <p> Add title and api key then save it. </p>
                        <h4>Done</h4>


                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->

        <div id="aweberHelpModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h3 class="modal-title text-center">Aweber step by step instructions</h3>
                    </div>
                    <div class="modal-body">
                        <p>Follow following steps to create Aweber Developer account & Api access</p>
                        <h4>Step 1:</h4>
                        <p> Hit this link to <a href="https://labs.aweber.com/" target="_blank">Create developer account</a></p>
                        <div class="row">
                            <div class="col-md-12">
                                <p><img src="{{ asset('assets/images/help/aweber-create-account.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                                <p><img src="{{ asset('assets/images/help/aweber-account-info.png') }}" alt="" class="img-responsive img-thumbnail" > </p>
                            </div>
                        </div>
                        <p>Click on Free Developer Account button.</p>
                        <p> A popup create account form will appear. </p>
                        <p>Add your account information and click on I Agree, Create An Account Button.</p>
                        <p> Aweber system will send you active account email. Active your account.</p>
                        <br>
                        <h4>Step 2:</h4>
                        <p> Create New App</p>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="{{ asset('assets/images/help/aweber-create-new-app.png') }}" alt="" class="img-responsive img-thumbnail" >
                                <img src="{{ asset('assets/images/help/aweber-copy-key.png') }}" alt="" class="img-responsive img-thumbnail" >
                            </div>
                        </div>
                        <br>
                        <p>Log in to your account</p>
                        <p>Click on Create New App</p>
                        <p>Fill new app information and submit form.</p>
                        <br>
                        <h4>Step 3:</h4>
                        <p> Add Api key</p>
                        <div class="row">
                            <div class="col-md-12">
                                <img src="{{ asset('assets/images/help/aweber-copy-key.png') }}" alt="" class="img-responsive img-thumbnail" >
                                <img src="{{ asset('assets/images/help/aweber-add-key-info.png') }}" alt="" class="img-responsive img-thumbnail" >
                            </div>
                        </div>
                        <br>
                        <p></p>
                        <p>Copy Consumer Key and Consumer Secret</p>
                        <p>Go to <a href="{{ route('gen_settings.index') }}"> Application </a></p>
                        <p>Add api information</p>

                        <h4>Step 4:</h4>
                        <p> Generate Access token</p>
                        <p><img src="{{ asset('assets/images/help/aweber-get-token-access.png') }}" alt="" class="img-responsive img-thumbnail" ></p>
                        <br>

                        <p> Add title, Consumer Key and Consumer Secret then save it.</p>
                        <h4>Done</h4>
                        <p></p>
                        <p><img src="{{ asset('assets/images/help/aweber-cuccess.png') }}" alt="" class="img-responsive img-thumbnail" ></p>

                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        <!-- common modal for import settings -->

    </div>
    <!--  add category -->

    <script>

        $(document).ready(function(){

            $('#activeHelp').click(function () {

                $('#activehelpModal').modal('show');
            });
            $('#getresHelp').click(function () {

                $('#getresponseModal').modal('show');
            });
            $('#mailchimpHelp').click(function () {

                $('#mailchimpModal').modal('show');
            });
            $('#aweberHelp').click(function () {

                $('#aweberHelpModal').modal('show');
            });

        });


        // save shared code
        // share emails popup
        function share_emails_code(e){

            $('.clipboard').html('Copy');
            $('.clipboard').addClass('btn-primary').removeClass('btn-success');

            var type = $(e).data('title');
            var account_id = $(e).data('id');
            $('#loading').show();

            $.post('{{ route("sharing-code-gen") }}',
                    {type:type, account_id: account_id},
                    function(data) {
                        if (data == 3) {
                            swal("Warning!", "The code is already generated!", "warning");
                        } else {

                            $('#created_code').html(data);
                            $('#hidden_code').val(data);
                            $('#share_pop_title').html(type + ' sharing code');
                            $('#shareSettingsModal').modal('show');
                            $('#loading').hide();
                        }
                    }
            );
        }

        // copy to clipboard

        function copyToClipboard(elementId) {
            // Create a "hidden" input
            var aux = document.createElement("input");

            aux.setAttribute("value", document.getElementById(elementId).value);
            // Append it to the body
            document.body.appendChild(aux);
            // Highlight its content
            aux.select();
            // Copy the highlighted text
            document.execCommand("copy");
            // Remove it from the body
            document.body.removeChild(aux);

            $('.clipboard').text('copied to clipboard');
            $('.clipboard').addClass('btn-success').removeClass('btn-primary');
        }
        // import emails
        function import_emails(e){

            var title_class = $(e).data('class');
            var title = $('.'+title_class).html();
            $('#category').val(title+ ' Emails');

            $('.target').val($(e).data('obj'));
            $('.account_id').val($(e).data('id'));
            $('#import_pop_title').html($(e).data('title'));
            $('#importSettingsModal').modal('show');
        }

        // import emails
        function save_and_import_emails(e){
            var form = $(e).data('form');
            var modal = $(e).data('modal');
            //var $input = $('#'+form+ ' input[type=text]').val();

            var isValid;
            $('#'+form+ ' input[type=text]').each(function() {
                var element = $(this);
                if (element.val() == "") {
                    isValid = false;
                }
            });

            if(isValid == false){
                swal('Alert', 'Please fill required fields!','warning');
                return;
            }

            var title = $('#'+form+ ' #account_title').val();

            $('#category').val(title+ ' Emails');

            $('#import_pop_title').html($(e).data('heading'));

            $('#importsettings_form').removeClass('import_settings');
            $('#importSettingsModal button:nth-child(2)').attr('type', 'button').addClass('cat_btn');
            $('#importSettingsModal button:nth-child(2)').attr('onclick', 'submit_save_import("'+form+'")');
            $('#'+modal).modal('hide');
            $('#importSettingsModal').modal('show');
        }

        function submit_save_import(form){

            if( $('#category').val() == '' ){
                swal('Alert', 'Category Required!','warning');
                return;
            }

            var title =  $('#category').val();

            $('#'+form+ ' input[name="import_category"] ').val(title);


            $('#importSettingsModal').modal('hide');
            $('#'+form + ' button[type=submit]').click();
        }

        $(document).ready(function() {

            $(".import_settings").submit(function(){
                var category = $('#category').val();

                $('#loading').show();
                var data = new FormData(this);

                $.ajax({
                    url: "{{ route('fetch-emails') }}",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();

                        if(result == '1'){
                            $('#importSettingsModal').modal('hide');
                            swal({
                                title: "Good job!",
                                text: "Email Templates has been Cued to import successfully!",
                                type: "success"
                            });

                        }else if( result == 3 ){
                            swal("Alert!", category+" already in record please create other!"    , "warning");
                        }
                        else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                        //$('input[name="import_category"]').val('');
                    }
                });

                return false;
            });

            $("#aweber_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });


            $("#getresponse_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });


            $("#mailchimp_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });

            $("#sendlane_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });
            $("#activecampaign_form").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                commonSubmitReq(data);
                return false;
            });
        });

        function commonSubmitReq(data){
            $.ajax({
                url: "<?php  echo route('gen_settings.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();

                    var obj = $.parseJSON(result);

                    if(obj.res ==3){
                        swal("Error!", obj.msg, "error");
                    }else
                    if(obj.res == 1){
                        swal({
                            title: "Good job!",
                            text: "Record has been saved successfully!",
                            type: "success",
                            confirmButtonText: "OK"
                        }, function(){
                            $('#loading').show();
                            location.reload();
                        });

                    }else{
                        swal("Error!", "Something went wrong.", "error");
                    }
                    $('.imp_category').val('');
                },
                error: function(error){
                    swal("Error!", "" +
                            "Invalid account credentials!", "error");
                    $('#loading').hide();
                }
            });
        }

        function GetAccessToken(x){
            $('#loading').show();
            var id = $(x).data("id");
            $.post("<?php echo route('get-aweber-auth'); ?>", { request:'aweber_access_token', id: id},
                    function(result){
                        $('#loading').hide();
                        var parsed = '';
                        if(parsed = JSON.parse(result)){
                            if(parsed.success == '1'){
                                window.location = parsed.redirect;
                            }else if(parsed.err == 1){
                                swal("Error!", parsed.err_message, "error");
                            }
                        }else if(result == '2'){
                            swal("Error!", "Access token could not generate yet, something went wrong.", "error");
                        }else{
                            swal("Error!", "Something went wrong.", "error");
                        }
                    }
            );
        }
    </script>

@endsection
