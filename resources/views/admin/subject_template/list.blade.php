@extends('layouts.app')
@section('title', 'Import Template')
@section('content')
    <style>
        #category_search{
          margin-top: -13px;
            padding-left: 5px;
        }
        #load_datatable_wrapper .row:first-child .col-sm-6{
            width:36%;
        }
        .select-with-transition{
            margin-top: -13px !important;
        }
    </style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">

            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="{{ url('/library') }}">Email Template</a></li>
                        <li class="active"> List </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header card-header-text" data-background-color="rose">
                    <h3 class="card-title">Email Templates</h3>
                </div>
                <div class="card-content">
                    <div class="col-xs-12 bg-white">
                        <div class="pull-right">
                        {{--<input id="category_search" class="form-control"  autocomplete="off">--}}
                        <select name="" id="category_search" data-live-search="true" class="selectpicker" data-style="select-with-transition btn-xs" data-size="7" title="Select category">
                            <option value="">All</option>
                            @foreach($email_category as $value)

                                <option   value="{{ $value->category }}">{{ $value->category }}</option>
                            @endforeach
                        </select>
                        </div>

                        <table id="load_datatable" width="100%" class="table table-bordered table-no-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Subject</th>
                                <th>category</th>
                                <th>Dated</th>
                                <th width="135">Status&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">No Record found yet.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal email content view -->
        <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                        <h4 class="modal-title text-center">Email Content</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                               <div id="load_content"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                // datatable
               var table =  $('#load_datatable').DataTable({
                    "pageLength":25,
                    "order": [[0, 'desc']],
                    processing: true,
                    serverSide: true,
                   responsive:true,

                    "drawCallback": function () {
                        //table_draw();
                        $("[name='status']").bootstrapSwitch();
                        changeStatus();
                    },

                    ajax: "{!! Route('load-subjects') !!}",
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'subject', name: 'subject'},
                        {data: 'category', name: 'email_category.category'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                        //{ data: 'updated_at', name: 'updated_at' }
                    ]
                });

                $('#category_search').change(function(){
                    table.columns( 2 ).search( this.value ).draw();
                } );
            });

            function viewContent(e){

                $('#loading').show();
                var id = $(e).data('id');
                $('#load_content').html('');
                if(id !="" ){
                    $.post("<?php echo route('view-email-content'); ?>", {id:id},
                            function(data){
                                $('#load_content').html(data);
                                $('#con-close-modal').modal('show');
                                $('#loading').hide();
                            }
                    )
                }else{
                    $('.load-subject').hide();
                    $('#loading').hide();
                }
            }
            function addCategory(){
                $("#con-close-modal").modal('show');
            }
        </script>

    </div>



@endsection
