@extends('layouts.app')
@section('title', 'Email Template')
@section('content')
<?php
$useMeSubject = '';
$useMeId = 0;
$checkWizardStep = 0;
if(isset($_REQUEST['use_me'])){
    $useMeId = $_REQUEST['use_me'];
    if($useMeId!=''){
        $useMeSubject = \App\SubjectTemplate::where('id', '=', $useMeId)->value('subject');
        $checkWizardStep = 1;
    }
}
?>
<style>
.cke_reset{
    border: none!important;
    border-bottom: 0.2px solid #ecebeb!important;
}


    .load-subject{
        margin-top: 20px;
    }
    .load-subject li{
        list-style: none;
        padding: 1px 3px 0px 3px;
        border-bottom: 1px solid #e2e2e2;
        background-color: #f3f3f3;
    }
    .load-subject li:hover{
        background-color: #00acc1;
        color: white;
        cursor: pointer;
    }
    .mobile-body{
        position: absolute;
        top: 12.5%;
        left: 8%;
        width: 85%;
        text-align: justify;
        height: 75%;
        overflow: auto;
        padding: 15px 15px 15px 8px;
    }
    .mobile-view, #desktop, #mobile, #landscape{
        position: relative;
    }

    #desktop > img, .landscape-view > img, .mobile-view > img{
        width: 100%;
    }
    .desktop-body{
        position: absolute;
        top: 3.7vw;
        width: 91.3%;
        left: 2vw;
        text-align: justify;
        height: 60%;
        overflow-x: hidden;
        padding: 20px;
    }
    .landscape-body{
        position: absolute;
        top: 12%;
        width: 75%;
        text-align: justify;
        height: 72%;
        overflow-y: auto;
        left: 13%;
        padding: 15px;
    }
    #search_subject, .input-group, #search_subject input, #search_subject label {
        width: 100%;
        text-align: left;
    }
    div.dataTables_wrapper div.dataTables_processing{
        top:5% !important;
    }
    .pagination{
        width: 100%;
    }
    #cke_1_contents{
        height: 485px !important;
    }
    .desktop_img{
        height: 700px;
    }
    .cke_bottom{
        display: none;
    }

    #mobile_editor #cke_content{
        margin-left: 17px;
        margin-right: -7px;
        margin-top: 63px;
    }
    #cke_2_contents{
        height: 500px !important;
    }
    #cke_contents{

    }

    .mobile_editor{
        position: absolute;
        top: 11.1%;
        left: 18px;
        width: 88%;
        height: 79%;
        overflow: auto;

    }
    .desktop_editor{
        position: absolute;
        top: 32.9%;
        width: 89.5%;
        left: 5%;
        height: 39.5%;
        overflow: auto;
    }
    /* */
    @media (max-width:1333px){
        .desktop_editor{
            /*top: 32.9%;*/
        }
    }
    @media (max-width:1266px){
        .desktop-body{
            /*width: 89%;*/
        }
        /*editor mobile*/
        #mobile_editor #cke_content{
            /*width:111% !important;*/
            /*margin-left: -1vw;*/
        }
        .mobile_editor {
            left: 1.4vw;
            width: 87%;
        }
    }
    @media (max-width:1239px){
        .desktop_editor{
            /*top: 34%;*/
            /*height: 39.3%;*/
        }
    }
    @media (max-width:1222px){
        /*.desktop-body{*/
            /*width: 89%;*/
        /*}*/
    }

    @media (max-width:1142px){
        .desktop_editor{
            /*height: 39%;*/
        }
        #cke_contents{
            width:100%;
        }
    }

    @media (max-width:1116px){
        .desktop-body{
            /*top: 3.45vw;*/
        }
    }

    @media (max-width:1040px){
        .desktop-body{
            top: 4.3vw;
            width: 90%;
        }
        .mobile-body{
            /*left: 1.7vw;*/
            /*width: 87%;*/
        }
    }
    /* mobile */
    @media (max-width:991px){
        .mobile-body {
            top: 10.9%;
            left: 4%;
            width: 91.4%;
            height: 78.5%;
        }
        .landscape-body{
            top: 12%;
            width: 78.5%;
            left: 11%;
        }
        /*mobile editor*/
        .mobile_editor {
            width: 87.9%;
            left: 6%;
            top: 15%;
            height: 73.5%;
        }
        #mobile_editor #cke_content {
            margin-right: 16px;
        }
        .desktop_editor {
            top: 46.5%;
            width: 90.5%;
            height: 31%;
            left: 4%;
        }
        .desktop-body{
            top: 5vw;
            width: 89.3%;
            left: 4vw;
        }

    }
    @media (max-width:840px){
        .desktop_editor {
            top: 47.5%;
            height: 30.7%;
        }
        .landscape-body {
            width: 78%;
        }
    }

    @media (max-width:844px){
        .mobile_editor {
            top: 16%;
            height: 67%;
        }
        .desktop_editor {
            top: 46.5%;
            height: 31%;
        }
    }

    @media (max-width:830px){
        .desktop_editor {
            top: 48.5%;
            height: 30%;
        }
        .mobile_editor {
            top: 18.3%;
            height: 70.5%;
        }
    }
    @media (max-width:800px) {
        .desktop_editor {
            top: 48.5%;
            height: 29.9%;
        }
    }
    @media (max-width:770px){
        .desktop_editor {
            top: 48.6%;
            height: 29.8%;
        }
        .mobile_editor {
            top: 19%;
            height: 70%;
        }
    }
    @media (max-width:680px){


        .desktop_img {
            height: 400px;
        }
        .desktop_editor {
            top: 56.3%;
            height: 24.4%;
        }
        .mobile_editor {
            top: 21.8%;
            height: 67.5%;
        }
    }
    @media (max-width:663px){
        .desktop_editor {
            top: 57%;
            height: 24%;
        }
        .landscape-body {
            width: 76.6%;
            left: 12%;
            height: 71%;
        }
    }

    @media (max-width:580px){
        .desktop_editor {
            top: 57.2%;
            height: 24%;
        }
        .mobile_editor {
            top: 24.8%;
            height: 64.9%;
        }
        .landscape-body {
            width: 75%;
        }
    }
    @media (max-width:555px){
        .desktop_editor {
            top: 59%;
            height: 22.5%;
        }
        .mobile_editor {
            top: 28%;
            height: 62%;
        }
    }
    @media (max-width:520px){
        .desktop_img {
            height: 350px;
        }
        .desktop_editor {
            top: 610px;
            height: 21%;
        }
        .mobile_editor {
            top: 31%;
            height: 59.4%;
        }
    }

    @media (max-width:500px){
        .desktop-body{
            left: 2vw;
            width: 92%;
        }
        .landscape-body {
            width: 78%;
            left: 10%;
        }
        .mobile_editor {

            top: 34.5%;
            height: 56.4%;
        }
    }
    @media (max-width:460px) {
        .desktop_editor {
            top: 561px;
            height: 28.5%;
        }
    }
    @media (max-width:454px){
        .desktop_editor {
            top: 562px;
            height: 22.5%;
        }
        .mobile_editor {
            top: 37%;
            height: 54%;
        }
    }
    @media (max-width:452px) {
        .desktop_editor {
            top: 65%;
            height: 19.5%;
        }
    }
    @media (max-width:452px) {
        .desktop_editor {
            top: 62%;
            height: 20.5%;
        }
    }

    @media (max-width:442px){
        .mobile_editor {
            top: 40%;
            height: 51%;
        }
    }
    @media (max-width:424px){
        .mobile_editor {
            top: 43%;
            height: 49%;
        }
        .landscape-body { width: 77%; left: 10%;
        }
    }
.btn-cstm .btn-sm{
    padding: 5px 13px !important;
}

    .wizard > .content > .body label{ margin-top: 0; }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th,
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th{ padding: 0; }
    .wizard > .steps > ul > li {
        width: 20% !important;
    }
    div.dataTables_wrapper{ top: 10% !important; }
    .btn-linktracker{padding: 8px 7px 7px 7px;}
    table tr td{  padding-left: 10px !important;}

    .multiselect-container .checkbox input[type="checkbox"]{ opacity: 1 !important; }

    .pointer{ cursor: pointer; }
    .cke_contents{ height: 500px!important; }
    .radio{  width: 100% !important; }
    .radio b{ margin-left: 25px!important;  }
    .cat_button .multiselect{float: right; }

    .email-body p img[border="0"] {
        height: 0!important;
        width: 0!important;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="col-sm-12">
            <!--      Wizard container        -->
            <div class="row">
                <div class="wizard-container">
                    <div class="card wizard-card active" data-color="rose" id="wizardProfile">
                        <form id="subject-template" action="#" method="post" novalidate="novalidate">
                            {{ csrf_field() }}

                            <input type="hidden" name="id" value="{!! ( isset( $content->id) )?$content->id:'' !!}">
                            <input type="hidden" name="subject_id"  value="{!! (isset($subject->id))?$subject->id:'' !!}">

                            <input type="hidden" name="category_id"  value="0">
                            <input type="hidden" name="send_type"  value="{{ (isset($unopenemails) && is_array($unopenemails) )? $unopenemails['type']: ''  }}">
                            <!--        You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                            <div class="wizard-header">
                                <h3 class="wizard-title">
                                    Build Your Email
                                </h3>
                            </div>
                            <div class="wizard-navigation">
                                <ul class="nav nav-pills">
                                    <li class="active" style="width: 33.3333%;">
                                        <a href="#SearchSubject" data-toggle="tab" aria-expanded="true">Search Subject Lines</a>
                                    </li>
                                    <li id="tab-mdyemail" style="width: 33.3333%;">
                                        <a href="#modifyEmail" data-toggle="tab">Modify Email</a>
                                    </li>
                                    <li style="width: 33.3333%;">
                                        <a href="#Preview" data-toggle="tab">Preview</a>
                                    </li>
                                    <li style="width: 33.3333%;">
                                        <a href="#Send" data-toggle="tab">Send</a>
                                    </li>
                                </ul>
                                <div class="moving-tab" style="width: 146.443px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s;">About</div></div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="SearchSubject">
                                    @if(count($count_cats) > 0 )
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label test-right"></label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <div id="search_subject"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 cat_button">
                                                        <select id="dropdown" multiple="multiple" class="selectpicker hidden" data-style="select-with-transition">
                                                            @foreach( $count_cats as $value)
                                                                <option value="{{ $value['id'] }}"> {{ $value['category'] }} ( {{ $value['total'] }} )</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="material-datatables">
                                        <!--  <div class="load-subject col-md-12"></div>-->
                                        <table id="load_datatable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th width="70%">Result</th>
                                                <th>Category</th>
                                                <th>Category</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td colspan="5">No Record found yet.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="tab-pane" id="modifyEmail">
                                    <div class="row">

                                        <div class="pull-right">
                                            <button type="button" class="btn btn-success btn-sm m-r-10" onclick='addGallery()' ><i class="fa fa-image"></i> Image Gallery</button>
                                        </div>
                                        <div class="">
                                            <label class="col-md-2  control-label">Subject:</label>
                                            <div class="col-md-8">
                                                <input id="searched_subject" name="subject" value="{!! (isset($subject->subject)) ? $subject->subject : '' !!}" class="form-control" autocomplete="off" required>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                        </div>
                                        @if( !is_array($unopenemails))
                                            <br>
                                            <div class="clearfix"></div>
                                            <div class="form-group">
                                                <label class="col-md-2  control-label">Personalize:</label>
                                                <div class="col-md-8">
                                                    <span class="pointer label label-success">%first_name%</span><span>&nbsp;&nbsp;</span>
                                                    <span class="pointer label label-info">%last_name%</span><span>&nbsp;&nbsp;</span>
                                                    <span class="pointer label label-primary">%email_address%</span>
                                                </div>
                                            </div>
                                            <hr>
                                            <br>
                                        @endif

                                            <div id="ck_tool_mobile"></div>
                                            <div id="ck_tool_desktop"></div>
                                                <br>
                                                <?php
                                                $header = $footer = "";
                                                if( isset($header_footer->header) && $header_footer->header != ""){
                                                    $header = $header_footer->header;
                                                };

                                                if( isset($header_footer->footer) && $header_footer->footer != ""){
                                                    $footer = $header_footer->footer;
                                                }
                                                $content = $header . '<br>' . $footer;
                                                ?>


                                            <div id="desktop_editor">
                                                <div id="emb-preview-chrome" class="c-email-preview">
                                                    <div class="c-email-preview__header" style="width:100%">
                                                        <textarea id="contents" rows="5" placeholder="some details" ><?= $content ?> </textarea>
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                            </div>
                                        <div class="clearfix"></div>
                                        <div class="col-lg-12">
                                            <button type="button" class="btn pull-right btn-primary btn-xs" onclick='emailTips()' > Email Tips</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Preview">
                                    <div class="">

                                        <div class="form-group ">

                                            <div class="btn-group">
                                                <button type="button" onclick="$('#profile').removeClass('hidden'); $('#desktop').addClass('hidden');"  class="btn btn-rose">Mobile</button>
                                                <button type="button" onclick="$('#profile').addClass('hidden'); $('#desktop').removeClass('hidden');" class="btn btn-linkedin">Desktop</button>
                                            </div>
                                            <div class="active" id="profile">
                                                <div class="col-md-4" id="mobile">
                                                    <div class="text-center mobile-view">
                                                        <img src="{{ asset('assets/images/mobile.png')  }}" alt="">
                                                        <div class="mobile-body">
                                                            <div class="mobile-subject">
                                                                <b><span></span></b>
                                                            </div>
                                                            <div class="email-body slimScrollBar">
                                                                <p><span></span> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8" id="landscape">
                                                    <div class="text-center landscape-view">
                                                        <img src="{{ asset('assets/images/landscap.png')  }}" alt="">
                                                        <div class="landscape-body">
                                                            <div class="subject">
                                                                <b><span></span></b>
                                                            </div>
                                                            <div class="email-body">
                                                                <p><span></span> </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="hidden" id="desktop">
                                                <img src="{{ asset('assets/images/desktop.png')  }}" alt="">
                                                <div class="desktop-body">
                                                    <div class="subject">
                                                        <!-- loading subject -->
                                                        <b><span></span></b>
                                                    </div>
                                                    <div class="email-body">
                                                        <!-- loading content -->
                                                        <p><span></span> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="Send">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-md-12 grand_parent" >
                                                <div class="form-group col-md-12">
                                                    <label class="col-md-2 control-label">Title:</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="email_campaign_title" id="email_campaign_title" value="{{ (isset($unopenemails) && is_array($unopenemails) )? $unopenemails['title']: ''  }}" class="form-control"  required>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label class="col-md-2 control-label">Select Category:</label>
                                                    <div class="col-md-8">
                                                        <select name="email_category" id="email_category" class="form-control" required>
                                                            <option></option>
                                                            @foreach( $cats as $value)
                                                                <option {{ (isset($unopenemails) && is_array($unopenemails) && $unopenemails['category'] == $value->id )? 'selected' : '' }}  value="{{ $value->id }}"> {{ $value->category }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1 text-right">
                                                        <button type="button" class="btn btn-rose btn-xs" title="Add More" data-toggle="tooltip" onclick="addCategory()"><i class="fa fa-plus"></i> Category</button>
                                                    </div>
                                                </div>
                                                @if( !is_array($unopenemails))
                                                    <div class="col-md-12">
                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-8 btn-groups btn-cstm">
                                                            <button id="" type="button" onclick='$("#download_file_html").submit();' class="btn btn-sm btn-facebook "> <i class="fa fa-download"></i> Download Html</button>
                                                            <button id="" type="button" onclick='$("#autoresponders,.append_api_keys,.append_campaigns").removeClass("hidden"); $("#smtp").addClass("hidden"); $("#contact_lists, #smtp_sent_type").attr("name", "");$(".schedule_btn").removeClass("hidden");' class="btn btn-sm btn-linkedin "> <i class="fa fa-send"></i> Autoresponders </button>
                                                            <button id="" type="button" onclick='$("#autoresponders,.append_api_keys,.append_campaigns").addClass("hidden"); $("#smtp").removeClass("hidden");$("#contact_lists").attr("name", "contact_lists[]");$("#smtp_sent_type").attr("name", "send_type").val("smtp_broad");$(".schedule_btn").addClass("hidden");' class="btn btn-sm btn-tumblr "> <i class="fa fa-send"></i> SMTP </button>
                                                        </div>
                                                    </div>
                                                    <div id="autoresponders" class="form-group col-md-12 hidden">
                                                        <label class="col-md-2 control-label">Select Autoresponder:</label>
                                                        <div class="col-md-8">
                                                            <select name="type" id="" class="form-control autoresponder" onchange="load_select_api_key(this)" >
                                                                <option></option>
                                                                @foreach($autorsponders as  $v)
                                                                    <option value="{{ $v['value'] }}">{{$v['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1 text-right">
                                                            <button type="button" class="btn btn-rose btn-xs" title="Add More" data-toggle="tooltip" onclick="addMoreARGroup()" ><i class="fa fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                    <div id="smtp" class="hidden">
                                                        @if( count($email_sender ) > 0 )
                                                            <div class="form-group col-md-12">
                                                                <label class="col-md-2 control-label">Select Email Sender:</label>
                                                                <div class="col-md-8">
                                                                    <select name="email_sender" id="" class="form-control autoresponder" onchange='$("#smtp_lists").removeClass("hidden")' >
                                                                        <option></option>
                                                                        @foreach($email_sender as $val)
                                                                            <option value="{{$val->id}}">{{$val->title}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="form-group col-md-12 m-t-10">
                                                                <label class="col-md-2 control-label"></label>
                                                                <div class="col-md-8 text-danger">You haven't configure any email sender yet!</div>
                                                            </div>
                                                        @endif

                                                        <div class="form-group col-md-12 hidden" id="smtp_lists">
                                                            <label class="col-md-2 control-label">Select List(s):</label>
                                                            <div class="col-md-8">
                                                                <select name="contact_lists[]" id="contact_lists" class="form-control" multiple="multiple" >

                                                                    @foreach($contact_lists as $val)
                                                                        <option value="{{$val->id}}">{{$val->list_name}} - {{ $val->type }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" id="smtp_sent_type"  >
                                                    </div>
                                                @else
                                                    <div class="form-group col-md-12">
                                                        <label class="col-md-2 control-label">Select Email Sender:</label>
                                                        <div class="col-md-8">
                                                            <select name="email_sender" id="" class="form-control autoresponder">
                                                                <option></option>
                                                                @foreach($email_sender as $val)
                                                                    <option value="{{$val->id}}">{{$val->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="append_api_keys col-md-12" ></div>
                                                <div class="append_campaigns col-md-12" ></div>
                                            </div>
                                        </div>
                                        <div class="row append_new_AR_group"></div>
                                        <div id="scheduled" class="hidden">
                                            <?php
                                            $star = '<span class="text-danger">*</span>';
                                            ?>
                                            <h4 style="background-color: #f2f2f2; padding: 15px">Schedule Broadcast</h4>
                                            <div class="col-md-12 m-t-20">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Send Date {!! $star !!}:</label>
                                                    <div class="col-md-8">
                                                        <input type="text" name="active_date" value="" id="date_picker" class="form-control"  >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 m-t-20" >
                                                <div class="form-group" >
                                                    <label class="col-md-2  control-label">Send Time {!! $star !!}:</label>
                                                    <div class="col-md-8" >
                                                        <div>
                                                            <div class="col-md-2" >
                                                                <select id="hour" class="form-control"  >
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12" selected="">12</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <select id="minute" class="form-control"  >
                                                                    <option value="00" selected="">00</option>
                                                                    <option value="05">05</option>
                                                                    <option value="10">10</option>
                                                                    <option value="15">15</option>
                                                                    <option value="20">20</option>
                                                                    <option value="25">25</option>
                                                                    <option value="30">30</option>
                                                                    <option value="35">35</option>
                                                                    <option value="40">40</option>
                                                                    <option value="45">45</option>
                                                                    <option value="50">50</option>
                                                                    <option value="55">55</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2" >
                                                                <select id="meridian" class="form-control"  >
                                                                    <option selected="">am</option>
                                                                    <option>pm</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input type="button" class="btn btn-next btn-fill btn-rose btn-wd" name="next" value="Next">
                                    <input type="button" class="btn btn-finish btn-fill btn-rose btn-wd" name="finish" value="Finish" style="display: none;">
                                </div>
                                <div class="pull-left">
                                    <input type="button" class="btn btn-previous btn-fill btn-default btn-wd disabled" name="previous" value="Previous">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- wizard container -->
        </div>
    </div>
</div>


<form id="download_file_html" action="{!! route('download-email') !!}" method="post" style="display: none;" >
    <input type="hidden" name="file_subject" value="" >
    <input type="hidden" name="file_content" value="">
    <input type="hidden" name="file_type" value="html">
</form>
<form id="download_file_text" action="{!! route('download-email') !!}" method="post" style="display: none;" >
    <input type="hidden" name="file_subject" value="" >
    <input type="hidden" name="file_content" value="">
    <input type="hidden" name="file_type" value="text">
</form>

<!-- modal gallery -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"> </div>

<!-- email tips -->
<div id="email_tips" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title text-center" id="custom-width-modalLabel">5 simple steps to make your email design responsive</h3>
            </div>
            <div class="modal-body">
                <p>Email newsletters created with MailerLite are optimized for mobile devices automatically. In order to achieve the responsive look we’ve made a few changes to the email styles:</p>
                <ul class="">
                    <li> Font size for  headlines – 24 px
                    <li> Font size for text – 16px </li>
                    <li> All images are shown in one column and over the entire length </li>
                    <li> All buttons are shown in one column and over the entire length </li>
                    <li> While most of the work is done for you, there are extra things you can do to make your emails look even better on a mobile device! </li>
                </ul>
                <h4>#1. Short subject line + preheader</h4>

                <p><img src="{{asset('assets/images/email-tip-1.jpg')}}" alt="" class="img-responsive"></p>

                <p>The preheader is the first bit of text that shows up in many email clients immediately after the subject line.</p>
                <p>Those snippets appear in most mobile devices and in some desktop clients, such as Gmail and Outlook.</p>
                <p>iPhone cuts off subject lines after 35 characters in portrait view, but displays about 140 characters of preheader or two full lines (that’s a lot!).</p>
                <p></p>Don’t use preheader to say… Can’t see images? Click here to view this email in your browser.</p>
                <p>It makes way more sense to write short subject line and then expand your message with preheader.</p>
                <p>For example, instead of subject line “Summer is finally here: 50% off all dresses�?, write “Summer if finally here�? as a subject line and then “50% off all dresses�? in preheader.</p>
                <h4>#2. CTA designed for fingers, not a mouse</h4>
                <p><img src="{{asset('assets/images/email-tip-2.jpg')}}" alt="" class="img-responsive"></p>
                <p>Your call-to-action is the most important part of your email.</p>
                <p>If you include several links in the text, make sure there’s enough space between then and can’t tap the wrong link.</p>
                <p>The way we code the buttons in MailerLite the whole area is clickable, not just the text in the middle. This way it’s even more user friendly on smaller devices.</p>
                <h4>#3. Short paragraphs</h4>

                <p>Keep paragraphs short, so it’s easy for readers to skim the message and understand what they are supposed to do next.</p>
                <p>Also a longer paragraph will mean more vertical scrolling on mobile device, so the rule “less is more�? applies very well here..</p>

                <h4>#4. Power of images</h4>
                <p> Image is a powerful tool to convince your customers to act. Let’s say you are selling plane tickets to Nice in France. What will you include in your newsletter? Price. Date. And… Photo of sunset in Nice. That will greatly help your customers to imagine what they can get if they purchase the ticket. It’s much easier to sell sunsets than flight tickets, right?</p>
                <p>Use images (photos, videos or infographics) that convey or support your message and get strategic about where you place those images, as they’re key to drawing the eye (and moving the scroll bar) down the screen.</p>
                <h4>#5. Mobile friendly landing pages</h4>
                <p>Now you have an email that looks incredible on any mobile device. A reader clicks on your call-to-action and finds a mess of a landing page. That’s it.</p>
                <p>Your reader closes the window, eats breakfast and forgets you exist. What a shame! It’s so important to have landing pages optimized for mobile devices.</p>
                <p>We have several landing page templates that are ready out-of-the-box, fully responsive and just waiting for you to add your texts and media.</p>

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- Add new category modal -->
<div id="add_category" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center">Add New Category</h4>
            </div>
            <form method="post" id="category_form" action="#">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-md-2 control-label">Categor:</label>
                        <div class="col-md-8">
                            <input type="text" name="category" id="category" class="form-control" required="">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-success replace_now">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->
<!-- Find synonyms modal -->
<div id="place_synonyms_here" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center">Find and Replace Synonym</h4>
            </div>
            <div class="modal-body place_synonyms_here"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary replace_now">Change</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<!-- Find synonyms modal -->
<div id="" class="modal fade show_useMe_content">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center"><small>Selected Subject</small>: <span id="choosen_subject"></span></h4>
                <p><strong>Would you like to model this email associated with selected subject line?</strong></p>
                <button type="button" class="btn btn-primary  useMeContent">Yes</button>
                <button type="button" class="btn btn-warning  useMeContent_no" >No</button>
                <button type="button" class="btn btn-purple" onclick="jqueryClickToLoadContent('previous')" >Previous</button>
                <button type="button" class="btn btn-linkedin" onclick="jqueryClickToLoadContent('next')" >Next</button>
                <button type="button" class="btn btn-danger  useMeContent_setEmpty"  data-dismiss="modal" >Cancel</button>
            </div>
            <div class="modal-body">
                <p class="put_useMe_content"></p>
            </div>
            <div class="modal-footer">
                <p><strong>Would you like to model this email associated with selected subject line?</strong></p>
                <button type="button" class="btn btn-primary  useMeContent">Yes</button>
                <button type="button" class="btn btn-warning  useMeContent_no">No</button>
                <button type="button" class="btn btn-purple " onclick="jqueryClickToLoadContent('previous')" >Previous</button>
                <button type="button" class="btn btn-linkedin" onclick="jqueryClickToLoadContent('next')" >Next</button>
                <button type="button" class="btn btn-danger  useMeContent_setEmpty"  data-dismiss="modal" >Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<style>
    body{
        max-width: inherit;
    }
    .user-details img{
        height: 45px !important;
    }
</style>

<input type="hidden" id="useMeIdInput" value="<?php  echo $useMeId; ?>" >
<input type="hidden" id="checkWizardStep" value="<?php echo $checkWizardStep; ?>" >
<input type="hidden" id="moveNextStep" value="0" >
<input type="hidden" id="loadNewSubject" value="humanClick" >
<input type="hidden" id="aweber_list" value="0" >

<input type="hidden" id="ar_type" value="">

<div id="header" class="hidden">
    <?= ( isset($header_footer->header) && $header_footer->header != "")? $header_footer->header : '' ?>
</div>
<div id="footer" class="hidden">
    <?= ( isset($header_footer->footer) && $header_footer->footer != "")? $header_footer->footer : '' ?>
</div>

</div>
<script src="{!! asset('assets/plugins/ckeditor/ckeditor.js') !!}"></script>
<script src="{!! asset('assets/js/bootstrap-multiselect.js') !!}?v={{ time() }}"></script>

<script>

    function show_schedule(){
        $('#scheduled').removeClass('hidden');
        $('#hour').attr('name', 'hour');
        $('#minute').attr('name', 'minute');
        $('#meridian').attr('name', 'meridian');
        $('#date_picker').attr('name', 'active_date');
    }

    //if human click then load new subject, if code click() then do not load new subject
    var checkWhichButtonClickedToLoadContent = 1;

    var editor_name = "mobile";

    $(document).ready(function(){

        // ck editor int
        var element = CKEDITOR.replace('contents',{
            allowedContent: true
        });

        // Date Picker
        $('#date_picker').datetimepicker({
            format: 'yy-mm-dd',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            },

        });

        // mobile window auto focus
        $('#steps-uid-0-t-1').click(function () {
            //CKEDITOR.instances.content.focus();
        });

        // prefrence add to editor
        $('.pointer').click(function () {
            var prefrence = $(this).text();
                CKEDITOR.instances['contents'].focus();
                CKEDITOR.instances['contents'].insertHtml(prefrence);
        });
        // ck editor tool bar
        // setTimeout(
        //     function()
        //     {
        //         // mobile window
        //         var ck_tools =  $('#cke_1_top').html();
        //
        //         $('#cke_1_top').html('');
        //         $('#ck_tool_mobile').html(ck_tools);
        //         // desktop view tool bar
        //
        //         var ck_tool =  $('#cke_2_top').html();
        //
        //         $('#cke_2_top').html('');
        //         $('#ck_tool_desktop').html(ck_tool).hide();
        //         $('#cke_2_top, #cke_1_top').hide();
        //         $('#cke_contents').css('border','none');
        //
        //     }, 1500);

        var checkWizardStep = $("#checkWizardStep");
        var useMesubject = '<?php echo $useMeSubject; ?>';
        if(useMesubject!=''){
            $('#searched_subject').val(useMesubject);
        }

        $('.first').on("click", function(){ $(checkWizardStep).val(0); });
        $("a[href='#previous']").on("click", function(){
            var step = $('.current a').attr("id");
            if(step=='steps-uid-0-t-0'){
                $(checkWizardStep).val(0);
            }
        });
       

        // ######### onchange desktop editor ########## //
        editor = CKEDITOR.instances.contents;
        editor.on('blur', function() {
            var mobile_data = CKEDITOR.instances.contents.getData();
        });
        // datatable
        $('#load_datatable').DataTable({
            "columnDefs": [
                {
                    "targets": [ 1 ],
                    "visible": false,
                    "searchable": true
                },
                { className: "cat_align", "targets": [2] }
            ],

            "oSearch": {"sSearch": useMesubject},

            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-6' i>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-3'><'col-sm-6'p>><'row'<'col-sm-6' i>>",

            //"aLengthMenu": [[50,100,150,200,250,300,-1], [50,100,150,200,250,300,"All"]],
            "aLengthMenu": [[10,25,50,100,150,200,250,300], [10,25,50,100,150,200,250,300]],

            "pageLength":25,
            "order": [[0, 'desc']],
            processing: true,
            serverSide: true,
            responsive:true,
            "initComplete": function (settings, json) {
                $("#load_datatable_filter").detach().appendTo('#search_subject');
            },
            "drawCallback": function () {
                var useMeIdInput =  $("#useMeIdInput").val();
                if(useMeIdInput!=''){
                    load_subFromUseMeSubject(useMeIdInput);
                    $("#useMeIdInput").val('');
                }
            },

            ajax: "{!! Route('choose-subject') !!}",
            columns: [
                {data: 'subject', name: 'subject'},
                {data: 'email_category_id', name: 'email_category_id'},
                {data: 'category', name: 'category'}

            ]
        });
        // Setup dropdown
        $('#dropdown').multiselect({
            onChange: function(element, checked) {
                var cities = $('#dropdown option:selected');
                var selected = [];
                $(cities).each(function(index, city) {
                    selected.push([$(this).val()]);
                });
                var regex = selected.join("|");
                $('#load_datatable').DataTable().column(1).search(
                    regex, true, true
                ).draw();
            }
        });
        // ajax submit form

        $(document).on('click','.wizard-footer .btn-finish', function () {
            var Title = $('#email_campaign_title');
            var Category = $('#email_category');
            if( Title.val() ==''){
                Title.focus();
            }else if( Category.val() == '' ){
                Category.focus();
            }else{
                $("#subject-template").submit();
            }
        });

        $("#subject-template").submit(function(){
            $('#loading').show();
            var data = new FormData(this);
            data.append('contents', CKEDITOR.instances.contents.getData());
            data.append('content_copy', CKEDITOR.instances.contents.getData());
            data.append('resend_emails_id', '{{(isset($unopenemails) && is_array($unopenemails) )? $unopenemails['title_id'] : '' }}');
            /*var check = checkGetResponseChecked();
            if(check==0){ $('#loading').hide(); return false;  }*/
            $.ajax({
                url: "<?php  echo route('new-template.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    var res_obj = JSON.parse(result);

                    if(Number(res_obj.id)==0){
                        swal("Error!", res_obj.msg, "error");
                    }

                    if(Number(res_obj.id) == 1 || Number(res_obj.id) > 1){
                        $('#loading').hide();
                        // swal("Good job!", "Subject has been saved successfully.", "success")
                        if(Number(result) > 1){
                            $('input[name="id"]').val(res_obj.id);
                        }
                        swal({
                            title: "Good job!",
                            text: res_obj.msg,
                            type: "success",
                            confirmButtonText: "OK"
                        }, function(){
                            window.location = "<?php echo route('home'); ?>";
                        });

                    }else if(res_obj.date_err == 1){
                        swal("Error!", res_obj.msg, "error");
                        $('#loading').hide();
                    }
                    else{
                        swal("Error!", "Something went wrong.", "error");
                        $('#loading').hide();
                    }
                }
            });
            return false;
        });
        //end ajax submit form

        $("#emailSearchBtn").click(function(){
            //alert('ok');
            $('.load-subject').show();
            var subject = $("#search_subject").val();
            if(subject !="" ){
                $('.load-subject').html('loading...');
                $.post("<?php echo route('choose-subject'); ?>", {subject:subject},
                    function(data){
                        $('.load-subject').html(data);
                    }
                )
            }else{
                $('.load-subject').hide();
            }
        });

        // download html template file
        $("#download_file_html").submit(function(){
            $("[name='file_subject']").val($("[name='subject']").val());
            //$("[name='file_content']").val(CKEDITOR.instances.content.getData());
            return true;
        });

        // download html template file
        $("#download_file_text").submit(function(){
            $("[name='file_subject']").val($("[name='subject']").val());
            return true;
        });

        // on next button click loading data to preview
        $('.wizard-navigation ul li:nth-child(3), .wizard-footer .btn-next').click(function(){

            var subject = $("#searched_subject").val();
            var content =  CKEDITOR.instances.contents.getData();
            $(".subject span").html(subject + '<br><br>');
            // short subject for mobile view

            if( subject.length > 32 ){
                $(".mobile-subject span").html(subject.substring(0,33)+"..." + '<br><br>');
            }else{
                $(".mobile-subject span").html(subject + '<br><br>');
            }
            $(".email-body span").html(content );
            // toastr["success"]("Status Updated successfully!");
        });


        // add new category submit function
        $('#category_form').submit(function () {
            var data = new FormData(this);

            $('#loading').show();

            $.ajax({
                url: "<?php  echo route('category.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){

                    if(result == 'al'){
                        $('#loading').hide();
                        swal('Warning', 'Category already in record!' ,'warning');
                    }
                    else{
                        var category = $('#category').val();
                        $('#email_category').append('<option selected value="'+result+'">'+category+'</option>');
                        $("#add_category").modal('hide');
                        $('#loading').hide();
                    }
                }
            });

            return false;
        });
    });

    // load add category modal
    function addCategory(){
        $("#add_category").modal('show');
    }

    // load modal
    function addGallery(){

        $.get("<?php echo route('gallery.index'); ?>", function(data){
            $( "#con-close-modal").html(data);
        });

        $("#con-close-modal").modal('show');
    }

    //
    function jqueryClickToLoadContent(direction){
        if($("input[name='sub']:checked").length==0){
            swal("Error!", "Please select any Subject first.", "error");
        }else{
            $("#loadNewSubject").val("codeClick");
            if(direction=='previous'){
                $("input[name='sub']:checked").parents("tr").prev().find("input[name='sub']").click();
            }
            if(direction=='next'){
                $("input[name='sub']:checked").parents("tr").next().find("input[name='sub']").click();
            }
        }
    }

    // Email tips
    function emailTips(){
        $("#email_tips").modal('show');
    }

    function load_sub(e){

        $("#loading").show();
        var for_id = $(e).attr('id');
        var content_id = $(e).data('content');
        var index = $(e).data('index');
        var subject = $('.cls_'+index).text();
        var loadNewSubject = $("#loadNewSubject");
        //do not change if codeClick
        if($(loadNewSubject).val()=='humanClick'){
            $('#searched_subject').val(subject);
            $("#choosen_subject").html(subject);
        }
        $(loadNewSubject).val('humanClick');
        //load_subFromUseMeSubject(for_id);
        $.post("<?php echo route('load-searched-email'); ?>", { content_id:content_id},
            function(res){

                var setDataEmpty = 0;
                var setDataFill = 0;
                var moveNextStep = Number($("#moveNextStep").val());
                var checkWizardStep = Number($("#checkWizardStep").val());

                $(".put_useMe_content").html(res);
                $(".show_useMe_content").modal('show');
                $("#loading").hide();

                var header = $('#header').html();
                var footer = $('#footer').html();

                var email_data =  header  + res  + footer;
                CKEDITOR.instances.contents.setData(email_data);
                $(".useMeContent").on('click', function(){
                    $(".show_useMe_content").modal('hide');
                    $("#tab-mdyemail a").trigger('click');
                    console.log('clicked');

                    if($('.current a').attr("id")=='steps-uid-0-t-0'){
                        moveNextStep = 1;
                    }
                    moveNextStepAfterSearch(checkWizardStep,moveNextStep);
                });
                $(".useMeContent_no").on('click', function(){
                    CKEDITOR.instances.contents.setData('');
                    $(".show_useMe_content").modal('hide');
                    if($('.current a').attr("id")=='steps-uid-0-t-0'){
                        moveNextStep = 1;
                    }
                    moveNextStepAfterSearch(checkWizardStep,moveNextStep);
                });
                $(".useMeContent_setEmpty").on('click', function(){
                    //CKEDITOR.instances.content.setData('');
                    CKEDITOR.instances.contents.setData('');
                });

            });
    }


    function addMoreARGroup(){
        var group = '<div class="col-md-12 grand_parent" style="background-color: rgba(243, 243, 243, 0.6); padding-top: 15px; margin-bottom: 15px;" >' +
            '<div class="form-group col-md-12">' +
            '<label class="col-md-2 control-label">Select Autoresponder:</label>' +
            '<div class="col-md-8">' +
            '<select name="type" id="" class="form-control autoresponder" required="" onchange="load_select_api_key(this)" >' +
            '<option></option>' +
                @foreach($autorsponders as  $v)
                    '<option value="{{ $v['value'] }}">{{$v['name']}}</option>'+
                @endforeach

                    '</select>' +
            '</div>' +
            '<div class="col-md-1 text-right">' +
            '<button type="button" class="btn btn-danger btn-sm" title="Remove" data-toggle="tooltip" onclick="RemoveARGroup(this)" ><i class="fa fa-minus"></i></button>' +
            '</div>' +
            '</div>' +
            '<div class="append_api_keys col-md-12" ></div>' +
            '<div class="append_campaigns col-md-12" ></div>' +
            '</div>';

        $(".append_new_AR_group").append(group);
    }

    function RemoveARGroup(x){
        swal({
            title: "Are you sure?",
            text: "You cannot reover selected details!",
            type: "error",
            showCancelButton: true,
            cancelButtonClass: 'btn-default btn-md ',
            confirmButtonClass: 'btn-danger btn-md ',
            confirmButtonText: 'Confirm!'
        }, function(){
            var parents = $(x).parents(".grand_parent");
            $(parents).remove();
        });
    }

    function moveNextStepAfterSearch(a,b){
        if( (a==0 && b==1) || (b==1) ){
            $("a[href='#next']").click();
            $("#checkWizardStep").val(1);
            $("#moveNextStep").val(0)
        }
    }


    function setNewDataInEditor(x, data){
        if(x==1){
            //CKEDITOR.instances.content.setData(data);
            CKEDITOR.instances.contents.setData(data);
        }else{
            //CKEDITOR.instances.content.setData('');
            CKEDITOR.instances.contents.setData('');
        }
    }

    function load_subFromUseMeSubject(id){
        if(id!=0 && id!='' && id!=null){

            $("#choosen_subject").html($('#s_'+id).text());

            $("#loading").show();
            $.post("<?php echo route('load-searched-email'); ?>", { use_me_content_id:id},
                function(data){

                    $(".put_useMe_content").html(data);
                    $(".show_useMe_content").modal('show');
                    $("#loading").hide();

                    var header = $('#header').html();
                    var footer = $('#footer').html();

                    var email_data =  header  + data  + footer;

                    //CKEDITOR.instances.content.setData(email_data);
                    CKEDITOR.instances.contents.setData(email_data);

                    var checkWizardStep = Number($("#checkWizardStep").val());

                    $(".useMeContent").on('click', function(){
                        //CKEDITOR.instances.content.setData(data);
                        $(".show_useMe_content").modal('hide');
                        if($('.current a').attr("id")=='steps-uid-0-t-0'){
                            moveNextStep = 1;
                        }
                        moveNextStepAfterSearch(checkWizardStep,moveNextStep);
                    });
                    $(".useMeContent_no").on('click', function(){
                        //CKEDITOR.instances.content.setData('');
                        CKEDITOR.instances.contents.setData('');
                        $(".show_useMe_content").modal('hide');
                        if($('.current a').attr("id")=='steps-uid-0-t-0'){
                            moveNextStep = 1;
                        }
                        moveNextStepAfterSearch(checkWizardStep,moveNextStep);
                    });
                    $(".useMeContent_setEmpty").on('click', function(){
                        //CKEDITOR.instances.content.setData('');
                        CKEDITOR.instances.contents.setData('');
                    });
                });
        }
    }

    function load_content(e){
        var content_id =   $(e).val();
        $.post("<?php echo route('load-searched-email'); ?>", { content_id:content_id},
            function(data){
                //$('#content').val(data);
                //CKEDITOR.instances.content.setData(data);
                CKEDITOR.instances.contents.setData(data);
            });
    }

    function load_select_api_key(x){

        $('#scheduled').addClass('hidden');
        $('.schedule_btn').remove();

        var api_type = $(x).val();
        $(x).attr('name', 'api_key_type_'+api_type+'[]');

        var parents = $(x).parents(".grand_parent");

        $(parents).find(".append_api_keys").html("");
        $(parents).find(".append_campaigns").html("");

        var type = $(x).val();

        $('#ar_type').val(type);

        if(type==''){ return false; }

        $('#loading').show();

        $.get("<?php echo route('load_ar_api_key'); ?>", { type:type},
            function(res_data){
                $(parents).find(".append_api_keys").html(res_data);
                $('#loading').hide();
            });

        // show buttons
        var $input = $('<li onclick="show_schedule()" class="schedule_btn" aria-hidden="false" style="display: block;"><a href="javascript:void(0);">Schedule Late</a></li>');
        $('ul[aria-label=Pagination] li:first-child').after($input);
    }

    function load_ar_campains(x){
        var parents = $(x).parents(".grand_parent");
        $(parents).find(".append_campaigns").html("");
        var type   = $(parents).find(".autoresponder").val();
        // test
        var type = $('#ar_type').val();
        var api_key = $(x).val();
        // changing name attribute
        $(x).attr('name', 'api_key_'+type+'_id[]');

        if(type=='' && api_key!=''){
            swal("Error!", "Please Select Auto Responder and API Key!", "error");
            return false;
        }

        $('#loading').show();

        $.get("<?php echo route('load_ar_campaings'); ?>", { type:type, api_key: api_key},
            function(res_data){

                if(res_data=='get_response_api_failed'){
                    swal("Error!", "Get Response API Key not found. Please provide API Key in Settings!", "error");
                }

                if(res_data=='aweber_credentials_failed'){
                    swal("Error!", "Please provide aweber Consumer Key and Secret, and generate Aweber Access Token in Settings page.!", "error");
                }

                if(res_data=='mailchimp_credentials_failed'){
                    swal("Error!", "MailChimp API Key not found. Please provide API Key in Settings!", "error");
                }
                if(res_data=='sendlane_credentials_failed'){
                    swal("Error!", "Sendlane API Key not found. Please provide API Key in Settings!", "error");
                }

                if(res_data!='get_response_api_failed' && res_data!='aweber_credentials_failed' && res_data!='mailchimp_credentials_failed'){

                    $(parents).find(".append_campaigns").html(res_data);

                    $('[data-toggle="tooltip"]').tooltip();

                    var searched_subject = $('#searched_subject').val();
                    //$(parents).find("input[name='newsletter_title']").val(searched_subject);
                    //$(parents).find("input[name='title']").val(searched_subject);
                    //$(parents).find("input[name='broadcast_title']").val(searched_subject);
                    $(parents).find(".newsletter_title").val(searched_subject);
                    $(parents).find(".title").val(searched_subject);
                    $(parents).find(".broadcast_title").val(searched_subject);
                    $(parents).find(".active_title").val(searched_subject);

                }

                $('#loading').hide();

            });
    }

    //checking all required info for get response newslettter sending
    function checkGetResponseChecked(){

        var checkbox        = Number($("#send_get_response").val());
        var aweber_checkbox = Number($("#send_aweber_broadcast").val());
        var mailchimp_check = Number($("#send_mailchimp_broadcast").val());

        var msgg = '';

        if(aweber_checkbox==1){

            if($("#send_aweber_broadcast").is(":checked")){
                var broadcast_title  = $('input[name="broadcast_title"]').val();
                var zone            = $('select[name="zone"]').val();
                var send_aweber_broadcast  = $('select[name="selectedList"]').val();

                if(broadcast_title=='' || broadcast_title==null){ msgg = 'Please Provide a title.' }
                if(zone=='' || zone==null){ msgg = 'Please select TimeZone.' }
                if(send_aweber_broadcast=='' || send_aweber_broadcast==null){ msgg = 'Please select Aweber Receiver List(s).' }
            }

        }

        if(mailchimp_check==1){

            if($("#send_mailchimp_broadcast").is(":checked")){
                var mc_title     = $('input[name="title"]').val();
                var mc_from_name = $('input[name="from_name"]').val();
                var mc_reply_to  = $('input[name="reply_to"]').val();
                var mc_list_id   = $('select[name="list_id"]').val();

                if(mc_title=='' || mc_title==null){ msgg = 'Please provide a Title.' }
                if(mc_from_name=='' || mc_from_name==null){ msgg = 'Please mention From Name.' }
                if(mc_reply_to=='' || mc_reply_to==null){ msgg = 'Please give Reply to Email.' }
                if(mc_list_id=='' || mc_list_id==null){ msgg = 'Please select MailChimp Receiver List(s).' }
            }

        }

        if(checkbox==1){
            if($("#send_get_response").is(":checked")){

                var title       = $('input[name="newsletter_title"]').val();
                var fromFieldId = $('select[name="fromFieldId"]').val();
                var belongsTo  = $('select[name="campaignId"]').val();
                var selectedCampaigns  = $('#selectedCampaigns').val();

                if(title=='' || title==null){ msgg = 'Please provide title.' }
                if(fromFieldId=='' || fromFieldId==null){ msgg = 'Please select From Email address.' }
                if(belongsTo=='' || belongsTo==null){ msgg = 'Please select Belongs-to Campaign.' }
                if(selectedCampaigns=='' || selectedCampaigns==null){ msgg = 'Please select Campaign(s).' }
            }
        }

        if(msgg!=''){
            swal("Error!", msgg, "error");
            msgg = '';
            return 0;
        }
        return 1;
    }
    useMesubject = '';
</script>

<script type="text/javascript">
    $(document).ready(function() {
        demo.initMaterialWizard();
        setTimeout(function() {
            $('.card.wizard-card').addClass('active');
        }, 600);
    });
</script>


@endsection

