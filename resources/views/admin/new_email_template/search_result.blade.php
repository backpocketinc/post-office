<?php

$html = '';


if(count($data)>0){

    $html .= '<div class="col-md-12">
                <p>
                    <strong>Please choose subject and Email Content to Use.</strong>
                    <br>
                    <small>You can modify it as you want.</small>
                </p>
                <hr>
            </div>';

    foreach($data as $value){

        $content = str_replace("<br />","",$value->content);
        $content = substr($content , 0, 150);

        $html .= '
        <div class="col-md-6">
            <div class="radio radio-single" >
                <input type="radio" id="sub_'.$value->subject_id.'" name="sub" aria-label="Single radio One"  onchange="load_sub(this)">
                <label for="sub_'.$value->subject_id.'"> <span> <b >'.$value->subject.'</b></span></label>
            </div>
            <div class="radio radio-info" style="margin-left: 5px; margin-top: -20px;" >
                <input type="radio" id="con_'.$value->contents_id.'" name="con" value="'.$value->contents_id.'" aria-label="Single radio One" onchange="load_content(this)">
                <label for="con_'.$value->contents_id.'"> <span> '.  $content .'...</span></label>
            </div>
        </div>';
    }
    echo $html;
}else{
    ?>
    <div class="col-md-6"><p>No Result Found.</p></div>
    <?php
}

?>