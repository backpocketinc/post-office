<?php

//foreach($data as $k=>$v){
//
//    if(!isset($v->campaignId ));
//    $data = array();
//    echo 'cc';
//}
$fromField = false;
foreach( $from_fields as $k => $v ){
    if(isset( $v->fromFieldId)){
        $fromField = true;
    }else{
        $message = $from_fields->message;
    }
}

if(count($data)>0 && $fromField == true ){
    $star = '<span class="text-danger">*</span>';
?>
<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <!--<div class="col-md-12">
        <div class="form-group">
            <label class="col-md-2 control-label">Title {!! $star !!}:</label>
            <div class="col-md-8">
                <input type="text" name="newsletter_title[]" value="" class="form-control newsletter_title" required autocomplete="off" >
            </div>
        </div>
    </div>-->

    <div class="col-md-12 hidden">
        <div class="form-group">
            <label class="col-md-2 control-label">ReplyTo Email:</label>
            <div class="col-md-8">
                <input type="email" name="replyto_email[]" value="{{ Auth::user()->email }}" class="form-control" >
            </div>
        </div>
    </div>

    <div class="col-md-12 hidden" >
        <div class="form-group">
            <label class="col-md-2  control-label">Select From Email {!! $star !!}:</label>
            <div class="col-md-8">
                <select name="fromFieldId[]" class="form-control" required >

                    <?php
                        if(count($from_fields)>0){

                            foreach( $from_fields as $k => $v ){
                            ?><option value="{!! $v->fromFieldId !!}"
                            <?php
                                    if($v->isDefault=='true'){
                                        echo 'selected ';
                                        echo ' style="font-weight: bold;" ';
                                        $default = ' (Default) ';
                                    }else{ $default =''; }
                                    ?>
                                    >
                                {!! $v->email !!} {!! $default !!}
                            </option><?php
                            }
                        }
                    ?>
                </select>
                <p class="help-text"><small>(Default) means This is default email address in your Get Response account.</small></p>
            </div>
        </div>
    </div>

    <div class="col-md-12 hidden">
        <div class="form-group">
            <label class="col-md-2  control-label">Belongs To {!! $star !!}:</label>
            <div class="col-md-8">
                <select name="campaignId[]" id="campaignId" class="form-control" required >
                    <!--<option disabled selected ></option>-->
                    <?php
                    if(count($data)>0){
                        foreach($data as $k=>$v){
                            ?><option value="{!! $v->campaignId !!}">{!! $v->name !!}</option><?php
                        }
                    }
                    ?>
                </select>
                <p class="help-text"><small>Select a campaign to which that Newsletter belongs.</small></p>
            </div>
        </div>
    </div>

        <div class="col-md-12 {{ (count($data) == 1)? 'hidden' : '' }}" style="margin-top: 20px;" >
            <div class="form-group" >
                <label class="col-md-2  control-label">Select Campaign(s) {!! $star !!}:</label>
                <div class="col-md-8" >
                    <select name="selectedCampaigns[{{$id}}][]" id="get_response_dropdown{{$id}}" class="form-control" required multiple >

                        <?php
                        if(count($data)>0){
                            foreach($data as $k=>$v){ ?>
                                <option {{ (count($data) == 1)? 'selected' : '' }} value="{!! $v->campaignId !!}">{!! $v->name !!}</option><?php
                            }
                        }
                        ?>
                    </select>
                    <p class="help-text"><small>Select a campaign(s) to which you want to send this Newsletter.</small></p>
                </div>
            </div>
        </div>


    <div class="col-md-12 m-t-20 hidden">
    <div class="form-group">
        <label class="col-md-2  control-label"></label>
        <div class="col-md-8">
            <label for="send_get_response">
                <input type="checkbox" checked name="send_get_response" id="send_get_response" value="1" > Send Email
            </label>
        </div>
    </div>
</div>
<script src="{!! asset('assets/js/bootstrap-multiselect.js') !!}?v={{ time() }}"></script>
<script>
    $(document).ready(function () {

        $('#get_response_dropdown{{$id}}').multiselect({
            allSelectedText: 'All',
            maxHeight: 200,
            includeSelectAllOption: true,
            nonSelectedText: 'Select Campaign(s)',
            nSelectedText: 'selected',
            allSelectedText: 'All selected',
            dropUp: true,
        });
    });
</script>
<?php

}else{
    ?>
<div class="col-md-12">
    <div class="col-md-2"></div>
    <div class="col-md-6"><p class="text-danger"> {{ (isset($message))? $message: ' No Result Found!.' }}</p></div>
</div>

<?php } ?>



