@extends('admin.layouts.app')
@section('title','Users')
@section('pageheading','Users')
@section('content')
    <style>
        .label {
            margin-right: 2px;
            font-size: 10px!important;
            text-transform: lowercase!important;
        }
    </style>
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="background: #ffffff;">
        <section>
            @include('admin.partials.errors')
            <div class="add_more pull-right m-b-5">
                <button type="button" class="btn btn-primary" onclick='addUser()' ><i class="fa fa-user-plus" aria-hidden="true"></i> Create User</button>
                <button type="button" class="btn btn-default" onclick="refresh()"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
            </div>
            <div class="material-datatables">
                <table class="table table-striped table-blue" id="load_datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Type</th>
                            <th>Dated</th>
                            <th>Status&nbsp;|&nbsp;Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4">No Record found yet.</td>
                        </tr>
                        </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </section>
    </div>


<!--  add user -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="javascript:void(0)" class="pull-right text-inverse" data-dismiss="modal">
                    <i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></a>
                <h4 class="modal-title text-center">Add New User</h4>
            </div>
            <form id="add_user" method="post" role="form" >
                {{csrf_field()}}
                <input type="hidden" name="id" value="" id="id" >
                <div class="modal-body">
                    <div class="box-radius p-20">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Name:</label>
                                <input type="text" name="first_name" class="form-control" value="" id="first_name" placeholder="first name" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Last Name:</label>
                                <input type="text" name="last_name" class="form-control" value="" id="last_name" placeholder="first name" required >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Username:</label>
                                <input type="text" name="username" class="form-control" value="" id="username" placeholder="username" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Email:</label>
                                <input type="email" name="email" class="form-control" value="" id="email" placeholder="valid email" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Phone:</label>
                                <input type="phone_no" name="phone_no" class="form-control" value="" id="phone_no" placeholder="valid email" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Password:</label>
                                <input type="text" name="password" class="form-control" value="" id="unencrypted_password" placeholder="password" required >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="c_info"></div>
                </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-xs" data-dismiss="modal">Cancel</button>
                        <button onclick="$('#add_user').trigger('submit');" type="button" class="btn btn-xs btn-orange">Save</button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /.modal -->
    <div class="modal fade" id="infModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="javascript:void(0)" class="pull-right text-inverse" data-dismiss="modal">
                        <i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></a>
                    <h4 class="modal-title text-center">Infusionsoft Subscriptions detail:<span id="shareTitleId"></span> </h4>
                </div>
                <div class="modal-body">
                    <div class="box-radius p-20">
                        <div class="inf_data"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
<script>
    function infPkgDetail(e){
        var id = $(e).data('id');
        if(id !=''){
            $('#loading').show();
            $.post('{{route('infu-pkg-info')}}',
                {_token:'{{csrf_token()}}', id:id},
                function (data) {
                    $('.inf_data').html(data);
                    $('#infModal').modal('show');
                    $('#loading').hide();
            });
        }
    }

    $(document).ready(function(){
        //saving new user
        $("#add_user").submit(function(){
            $('#loading').show();
            var data = new FormData(this);
            $.ajax({
                url: "<?php echo route('users.store'); ?>",
                data: data,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(result){
                    $('#loading').hide();

                    if(result.msg!='0'){

                        if(result.msg=='1'){
                            swal("Success!", "User has been saved successfully", "success");
                            $('#add_user')[0].reset();
                            $("#id").val("");
                            $("#con-close-modal").modal('hide');
                            refresh();
                        }
                        if(result.msg=='2'){
                            $('#add_user #email').focus();
                            swal("OOPS!", "User with this email already exist!", "warning");
                        }
                    }else{
                        swal("Error!", "Something went wrong.", "error");
                    }
                }
            });
            return false;
        });

        $('#load_datatable').DataTable({
            "pageLength":25,
            "order": [[0, 'desc']],
            'columnDefs': [
                {
                    'targets': 8,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        $(td).css('text-align', 'center');
                    }
                }
            ],

            processing: true,
            serverSide: true,
            responsive: true,
            "initComplete": function (settings, json) {

            },
            ajax: "{!! Route('load-users') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'phone_no', name: 'phone_no'},
                {data: 'type', name: 'type'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
                //{ data: 'updated_at', name: 'updated_at' }
            ]
        });

    });

    function addUser(){
        $('#add_user')[0].reset();
        $("#con-close-modal").modal('show');
    }

    function editRow(x){
        //$('#loading').show();
        if(x!=''){
            $.post("<?php echo url('user/loadEdit'); ?>",
                {id: x, _token:'{{csrf_token()}}'},
                function(result){
                    if(result!='0'){

                        $.each(result, function(k,v){
                            var ref = $("#add_user").find("#"+k);
                            $(ref).val(v);
                            $("#con-close-modal").modal('show');
                        });

                        $('#email_category > option').attr('selected',false);

                    }else{
                        swal("Error!", "Something went wrong.", "error");
                    }
                $('#loading').hide();
            }

            );
        }
    }

    // delete shortcut
    function deleteRow(ev) {
        var e = $(ev);
        var id = e.data('id');
        if (id != '') {
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary shortcut! all data of this user will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function () {
                $.post('{{ route('delete-user') }}',
                    {id: id, _token: '{{csrf_token()}}'},
                    function (data) {
                        if (data == 1) {
                            refresh();
                            // cancel subscriptions
                            $.post('{{route('cancel-user-subscription')}}',
                                { '_token':'{{ csrf_token() }}', id:id},
                                function(data){
                                    if(data.status == 1){
                                        toastr["success"]('Shortcut deleted successfully!');

                                    }else if(data.status == 2){
                                        toastr["error"](data.msg, 'error');
                                    }
                                });
                        } else {
                            toastr["error"]('Some error occurred!');
                        }

                    });
            });
        }
    }

    function change_status(e){
        var id = $(e).data('id');
        $.post( '{{ route('user-status') }}',
            {id:id, _token:'{{csrf_token()}}'},
            function(data){
                if(data.msg == 1){
                    toastr["success"]('User Unblocked successfully!');

                }
                if(data.msg == 0){
                    toastr["success"]('User blocked successfully!');
                }
                $('#loading').hide();
            });
    }

</script>

@endsection
