@extends('layouts.app')
@section('title', 'Import Template')
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">

                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="#">Templates</a></li>
                        <li class="active"> List </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="card-box">

                    <div class="row">
                        <div class="col-xs-12 bg-white">
                            <table class="table table-colored table-inverse table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Dated</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="4">No Record found yet.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>


    </div>



@endsection
