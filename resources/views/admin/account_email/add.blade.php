@extends('layouts.app')
@section('title', 'Categories')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content">
        <!-- Start content -->
        <div class="container-fluid">

            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Email Settings </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="">
                    <div class="row">
                        <div class="card">
                            <div class="card-header card-header-text" data-background-color="rose">
                                <h3 class="card-title">Email Settings</h3>
                            </div>
                            <div class="card-content">

                                <form action="" method="post" id="EmailSettings" autocomplete="off">
                                    {{ csrf_field() }}
                                    <div class="form-group col-md-6">
                                        <label for=""> APPLICATION NAME </label>
                                        <input type="text" class="form-control" name="APP_NAME" value="{{ trim( $mail['APP_NAME'] ) }}" required  autocomplete="off"  onkeyup="replaceIt(this)" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL FROM ADDRESS </label>
                                        <input type="text" class="form-control" name="MAIL_FROM_ADDRESS" value="{{ trim( $mail['MAIL_FROM_ADDRESS'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL FROM NAME </label>
                                        <input type="text" class="form-control" name="MAIL_FROM_NAME" value="{{ trim( $mail['MAIL_FROM_NAME'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL DRIVER </label>
                                        <input type="text" class="form-control" name="MAIL_DRIVER" value="{{ trim( $mail['MAIL_DRIVER'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL HOST </label>
                                        <input type="text" class="form-control" name="MAIL_HOST" value="{{ trim( $mail['MAIL_HOST'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL PORT </label>
                                        <input type="text" class="form-control" name="MAIL_PORT" value="{{ trim( $mail['MAIL_PORT'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL USERNAME </label>
                                        <input type="text" class="form-control" name="MAIL_USERNAME" value="{{ trim( $mail['MAIL_USERNAME'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL PASSWORD </label>
                                        <input type="text" class="form-control" name="MAIL_PASSWORD" value="{{ trim( $mail['MAIL_PASSWORD'] ) }}" required  autocomplete="off" onkeyup="replaceIt(this) "/>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> MAIL ENCRYPTION </label>
                                        <input type="text" class="form-control" name="MAIL_ENCRYPTION" value="{{ trim( $mail['MAIL_ENCRYPTION'] ) }}" autocomplete="off"  onkeyup="replaceIt(this)" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for=""> TEST EMAIL </label>
                                        <input type="email" class="form-control" name="email" id="test_email" placeholder="Enter test email id" required  autocomplete="off"/>
                                        <small>  </small>
                                    </div>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-success"> Test Email </button>
                                </form>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header card-header-text" data-background-color="rose">
                                <h3 class="card-title">Email Template Settings</h3>
                            </div>
                            <div class="card-content">
                                <form class="form-horizontal" role="form" id="email_settings" >
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ (isset($data->id) && $data->id !="")? $data->id :''  }}">

                                    <div class="form-group">
                                        <div class="alert alert-info">Registration Notification</div>
                                    </div>

                                        <div class="form-group">
                                            <label class="control-label">Subject:</label>
                                            <div class="">
                                                <input id="registration_subject" name="registration_subject" placeholder="Provide subject for registration notification" value="{!! (isset($data->registration_subject)) ? $data->registration_subject : '' !!}" class="form-control" required autocomplete="off" >
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class=" control-label">Content:</label>
                                            <div class="">
                                                <textarea class="form-control" id="registration_content" rows="5" placeholder="some details" > {!! (isset($data->registration_content)) ? $data->registration_content : '' !!}</textarea>
                                                <div class="register m-t-5">
                                                    <a href="javascript:void(0)"> <span class="label label-success"> %name% </span> </a>
                                                    <a href="javascript:void(0)"> <span class="label label-info"> %email% </span> </a>
                                                    <a href="javascript:void(0)"> <span class="label label-danger"> %password% </span> </a>
                                                    <a href="javascript:void(0)"> <span class="label label-warning"> %status% </span> </a>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <div class="alert alert-info">Status Notification</div>
                                    </div>

                                    <div class="form-group">
                                        <label class=" control-label">Subject:</label>
                                        <div class="">
                                            <input id="status_subject" name="status_subject" placeholder="Provide subject for status notification" value="{!! (isset($data->status_subject)) ? $data->status_subject : '' !!}" class="form-control" autocomplete="off" >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label class=" control-label">Content:</label>
                                        <div class="">

                                            <textarea class="form-control" id="status_content" rows="5" placeholder="some details" > {!! (isset($data->status_content)) ? $data->status_content : '' !!}</textarea>
                                            <div class="status m-t-5">
                                                <a href="javascript:void(0)"> <span class="label label-success"> %name% </span> </a>
                                                <a href="javascript:void(0)"> <span class="label label-info"> %email% </span> </a>
                                                <a href="javascript:void(0)"> <span class="label label-danger"> %password% </span> </a>
                                                <a href="javascript:void(0)"> <span class="label label-warning"> %status% </span> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label"></label>
                                        <div class="">
                                            <button class="btn w-md btn-bordered btn-success  " type="submit">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <script src="{!! asset('assets/plugins/ckeditor/ckeditor.js') !!}"></script>
    <script>

        function replaceIt(e){
            $(e).val( $(e).val().replace(/ /g, "") );
        }

        $(document).ready(function() {
            // ck editor1
            CKEDITOR.replace('status_content',{
                allowedContent: true
            });
            // ck editor2
            CKEDITOR.replace('registration_content',{
                allowedContent: true
            });

            // status tags add

            $('.status a').click(function(){
                var tag =  $(this).html();
                CKEDITOR.instances.status_content.insertHtml(tag);
            });
            // registration tags add

            $('.register a').click(function(){
                var tag =  $(this).html();
                CKEDITOR.instances.registration_content.insertHtml(tag);
            });

            // Email setting
            $("#EmailSettings").submit(function(){
                $('#loading').show();
                var data = new FormData(this);

                $.ajax({
                    url: "<?php  echo route('email-settings-store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();
                        if(result.success == 'success'){

                            $.post('{{ route('test-email') }}',
                                { '_token': '{{ csrf_token() }}', email: $('#test_email').val() },
                                function (data) {
                                    if(data.success == 'success') {
                                        swal({
                                            title: "Good job!",
                                            text: "Email settings have been saved successfully!",
                                            type: "success",
                                            confirmButtonText: "OK"
                                        });
                                    }else{
                                        swal("Error!", data.error, "error");
                                    }
                            });

                        }else{
                            swal("Error!", result.error, "error");
                            $('#loading').hide();
                        }
                    }
                });

                return false;
            });


            // ajax submit form
            $("#email_settings").submit(function(){
                $('#loading').show();
                var data = new FormData(this);

                data.append('registration_content', CKEDITOR.instances.registration_content.getData());
                data.append('status_content', CKEDITOR.instances.status_content.getData());

                $.ajax({
                    url: "<?php  echo route('email-settings.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();
                        if(result == '1'){
                            swal({
                                title: "Good job!",
                                text: "Email settings have been saved successfully!",
                                type: "success",
                                confirmButtonText: "OK"
                            });

                        }else{
                            swal("Error!", "Something went wrong.", "error");
                            $('#loading').hide();
                        }
                    }
                });

                return false;
            });

        });

    </script>

@endsection
