<style>
    #load_pixabay{
        margin-top: 18px;
        display: inline-block;
        width: 100%;
    }
    .col-sm-3, .col-sm-2{
        position: relative;
    }
    .col-sm-3 .caption, .col-sm-2 .caption{
        position: absolute;
        top: -10px;
        right: 14px;
    }
    .col-sm-3 .caption2, .col-sm-2 .caption2{
        position: absolute;
        top: -10px;

    }

    .col-sm-3 img, .col-sm-2 img {
        margin-bottom: 17px;
        height: 110px;
        width: 200px;
    }
    .caption a,.caption2 a{
        opacity: 0.7 !important;
    }
    .caption a:hover, .caption2 a:hover{
        opacity: 1;
    }
    #add_file{
        display: none;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
            <h4 class="modal-title text-center">Choose Image</h4>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#home1" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-image"></i></span>
                                    <span class="hidden-xs">Your Images</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#gallery" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-file-image"></i></span>
                                    <span class="hidden-xs">Gallery</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#profile1" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-search-plus"></i></span>
                                    <span class="hidden-xs">Search Images From Pixabay</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div class="tab-pane active" id="home1">
                                <div class="col-md-12">
                                    <form id="browser_file" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <button id="browse_btn" type="button"  class="btn btn-success btn-sm"> <i class="fa fa-download"></i> Browse  </button>
                                        <input type="file" name="image" id="add_file">
                                    </form>
                                    <p></p>
                                </div>
                                <?php
                                $count = 0;
                                foreach($your_gallery as $value) { $count++; ?>
                                <div class="col-sm-2" id="item_{{ $value->id }}">
                                    <img id='your_gallery_{{ $count }}' src="{{asset('assets/gallery/'.$value->images.'')}}" alt="image" class="img-responsive img-rounded" width="200">
                                    <div id="{{ $count }}" data-param="your_gallery_" class='caption' onclick='load_image(this)' title='Insert Image'>
                                        <a href='#' class='btn btn-info btn-xxs' role='button'><i class='fa fa-download'></i></a>
                                    </div>
                                    <div class='caption2' onclick="deleteRow(this)" data-id="{{ $value->id }}" data-obj="gallery" title='Insert Image'>
                                        <a href='#' class='btn btn-danger btn-xxs' role='button'><i class='fa fa-trash'></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="tab-pane" id="gallery">

                                <div class="col-md-12">
                                    <form id="browser_file" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                      
                                      <input type="file" name="image" id="add_file">
                                    </form>
                                    <p></p>
                                </div>
                                <?php
                                $count = 0;
                                foreach($gallery as $value) { $count++; ?>
                                <div class="col-sm-2" id="item_{{$value->id}}">
                                    <img id='gallery_{{ $count }}' src="{{asset('assets/gallery/'.$value->images.'')}}" alt="image" class="img-responsive img-rounded" width="200">
                                    <div id='{{ $count }}' data-param='gallery_' class='caption' onclick='load_image(this)' title='Insert Image'>
                                        <a href='#' class='btn btn-info btn-xxs' role='button'><i class='fa fa-download'></i></a>
                                    </div>
                                    <?php if(Auth::User()->type == 'a'){ ?>
                                    <div class='caption2' onclick="deleteRow(this)" data-id="{{ $value->id }}" data-obj="gallery" title='Insert Image'>
                                        <a href='#' class='btn btn-danger  btn-xxs' role='button'><i class='fa fa-trash'></i></a>
                                    </div>
                                   <?php } ?>
                                </div>
                              <?php } ?>
                            </div>

                            <div class="tab-pane" id="profile1">
                                <div class="col-md-12">
                                <form id="gallery_form">
                                    <div class="col-md-8">
                                            <div class="input-group m-t-10">
                                                <input type="text" id="keyword" name="search" class="form-control" placeholder="Image search" autocomplete="off" required>
                                                <span class="input-group-btn">
                                                <button type="submit" id="Pixabay" class="btn   btn-primary">Submit</button>
                                                </span>
                                            </div>
                                        </div>
                                    <div class="col-md-4">
                                        <div class="button-list">
                                            <p></p>
                                            <button id="prev" type="submit" class="btn btn-icon  btn-default "> <i class="fa fa-chevron-left"></i> </button>
                                            <button class="btn btn-icon  btn-linkedin current-state" value="1"> <span>1</span> </button>
                                            <button id="next" type="submit" class="btn btn-icon  btn-default"> <i class="fa fa-chevron-right"></i> </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div id="load_pixabay">
                                    <!-- load result -->
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
   $(document).ready(function(){

       var count = $('.current-state').val();

       $('#next').click(function(){
          count =+count + +1;

       });

       $('#prev').click(function(){
           count = count-1;
       });


       $('#gallery_form').submit(function(){

           if(count < 1){
               return false;
           }

       var keyword =  $('#keyword').val();
       var page =  count;
       if(keyword!=""){
           $('#loading').show();
           $.get("<?php echo route('gallery.create'); ?>",
               {keyword:keyword, page:page},
               function(data){
                   if(data) {
                       $('#load_pixabay').html(data);
                       $('#loading').hide();
                      $('.current-state').val(count);
                      $('.current-state span').html(count);
                   }
               });
       }
           return false;
    });

       $('#browse_btn').click(function(){
           $('#add_file').click();
       });

   });


   function load_image(e)
   {
       var id =  $(e).attr('id') ;
       var param =   $(e).data('param') ;
       var img_src = $('#'+param+id).attr('src');

       if(typeof img_src !== "undefined" && img_src !== false)
       {
           //destroy
           var img = '<img src="'+img_src+'" class="img-responsive"/>';
           $('#con-close-modal,.modal-backdrop').modal('hide');

           CKEDITOR.instances['contents'].insertHtml(img);
       }else
       {
           alert("Select an image to insert");
       }
   }

   $(function() {

       $("#add_file").change(function (){
           $('#loading').show();

           var data = new FormData($("#browser_file")[0]);
           $.ajax({
               url: "<?php echo route('browse-gallery'); ?>",
               data: data,
               contentType: false,
               processData: false,
               type: 'POST',
               success: function(result){
                $('#con-close-modal,.modal-backdrop').modal('hide');
                  if(result.msg == 1){ 
                   CKEDITOR.instances['contents'].insertHtml(result.file);
                   $('#loading').hide();
                 }else if(result.msg == 2){
                  swal('Alert', result.error, 'warning');
                 }
                   
               }
           });

       });
   });


</script>










