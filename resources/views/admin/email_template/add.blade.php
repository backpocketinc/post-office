@extends('layouts.app')
@section('title', 'Email Template')
@section('content')

<style>
    form#add_category .loader {
        /* float: right;
        margin-left: 5px;
        margin-top: 6px; */

    }

    form#add_category .loader img {
        /* display: none; */
    }
</style>

<div class="content">
    <!-- Start content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box pull-right">
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li> <a href="{{ url('/library') }}">Email Template</a></li>
                        <li class="active"> Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif
            <div class="card">
                <div class="card-header card-header-text" data-background-color="rose">
                    <h3 class="card-title">Add New Email Template</h3>
                </div>
                <div class="card-content">


                    <form class="form-horizontal" method="post" role="form" id="email-template">
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="{!! ( isset( $content->id) )?$content->id:'' !!}">
                        <input type="hidden" name="subject_id" value="{!! (isset($subject->id))?$subject->id:'' !!}">

                        <div class="form-group">
                            <label class=" control-label">Unique Token:</label>
                            <input type="text" class="form-control" name="token"
                                value="{!! (isset($subject->token))?$subject->token:'' !!}" maxlength="8" readonly>
                        </div>

                        <div class="form-group">
                            <label class=" control-label">Domain:</label>
                            <select name="domain" class="form-control" id="">
                                {{-- @foreach($categories as $category) --}}
                                <option value="1">DryClean</option>
                                <option value="2">BookKeeper</option>
                                {{-- @endforeach --}}
                            </select>
                        </div>


                        <div class="form-group">
                            <label class=" control-label">Title:</label>
                            <input type="text" name="subject"
                                value="{!! (isset($subject->subject))?str_replace('"', '', $subject->subject):"" !!}" class="form-control" placeholder="subject of template" required >
                            </div>
                            <input type="hidden" name="category_id" value="0">
                            <div class="form-group">
                                <label class=" control-label">Text:</label>
                                <textarea class="form-control" id="content" rows="5" placeholder="some details" > {!! (isset($content->content))?$content->content:'' !!} </textarea>
                            </div>
                            <div class="form-group account-btn m-t-10">
                                <button class="btn w-md btn-bordered btn-success" type="submit">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                    <h4 class="modal-title text-center">Add New Category</h4>
                </div>
                <form id="add_category" method="post" role="form" >

                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="category"  class="form-control" id="field-1" placeholder="category name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <textarea class="form-control" id="field-2" name="details" placeholder="Write something about category"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-info  ">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    

    <script src="{!! asset('assets/plugins/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('assets/plugins/ckeditor/jquery-ckeditor.js') !!}"></script>


                            <script>
                                $(document).ready(function(){

            //Ckeditor
           
            //         ck editor initialize
            CKEDITOR.replace('content',{
                allowedContent: true
            });

            //saving email-template
            $("#email-template").submit(function(e){
                e.preventDefault();
                $('#loading').show();
                
                var data = new FormData(this);
                // var data = $("#email-template").serialize();
                // data += '&content=' + CKEDITOR.instances.content.getData();
                // console.log(data);
                data.append('content', CKEDITOR.instances.content.getData());
                $.ajax({
                    url: "<?php echo route('email-template.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        if(result=='1'){
                            // $('#email-template')[0].reset();
                            $("#con-close-modal").modal('hide');
                            $('#loading').hide();
                            swal("Good job!", "Email template has been saved successfully.", "success")

                        }else{
                            $('#loading').hide();
                            $('#email-template')[0].reset();
                            swal("Error!", "Something went wrong.", "error");
                        }
                    }
                });

                return false;
            });


            //saving new category
            $("#add_category").submit(function(){

                $('#loading').show();
                var data = new FormData(this);

                $.ajax({
                    url: "<?php echo route('category.store'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        if(result=='1'){
                            $('#add_category')[0].reset();
                            $("#con-close-modal").modal('hide');
                            $('#loading').hide();
                            swal("Good job!", "Category has been saved successfully.", "success")
                        }else{
                            $('#loading').hide();
                            swal("Error!", "Something went wrong.", "error");
                        }
                    }
                });

                return false;
            });

        });

        function addCategory(){
            $("#con-close-modal").modal('show');
        }
                            </script>

                            @endsection