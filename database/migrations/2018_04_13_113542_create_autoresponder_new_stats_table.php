<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoresponderNewStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('autoresponder_new_stats')) {
            Schema::create('autoresponder_new_stats', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('email_campaign_title_id')->length(10);
              $table->text('subject');
              $table->integer('subject_id')->length(10)->nullable();
              $table->integer('email_category_id')->length(10)->unsigned();
              $table->string('email')->nullable();
              $table->string('ip');
              $table->string('country');
              $table->integer('user_id')->length(10);
              $table->string('email_brodcast_id');
              $table->string('time_zone');
              $table->string('region');
              $table->string('city');
              $table->string('longi');
              $table->string('lati');
              $table->text('response');
              $table->string('type');
              $table->string('date_time');
              $table->string('stat_type')->nullable();
              $table->string('user_agent')->nullable();
              $table->string('referrer')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoresponder_new_stats');
    }
}
