<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_sender')) {
            Schema::create('email_sender', function (Blueprint $table) {
                $table->increments('id');
                 $table->string('title');
                 $table->string('host')->nullable();
                 $table->string('port')->nullable();
                 $table->string('user')->nullable();
                 $table->string('password')->nullable();
                 $table->tinyInteger('smtp_ssl')->default(0);
                 $table->string('domain')->nullable();
                 $table->integer('user_id')->length(10)->unsigned();
                 $table->string('api_key', 500)->nullable()->comment('public_api, access_key');
                 $table->string('private_api_key', 500)->nullable()->comment('access_secret');
                 $table->string('from_name')->nullable();
                 $table->string('from_email');
                 $table->string('type');
                 $table->tinyInteger('status');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_sender');
    }
}
