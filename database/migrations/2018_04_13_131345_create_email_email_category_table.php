<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateEmailEmailCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('email_category')) {
            Schema::create('email_category', function (Blueprint $table) {
                $table->integer('id');
                $table->string('category');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }

        $now = DB::raw('NOW()');

        $data[] = [
            'id' => 3,
            'category' => 'User Created Emails',
            'created_at' => $now
        ];

        $data[] = [
            'id' => 6,
            'category' => 'Imported Emails',
            'created_at' => $now
        ];

        DB::table('email_category')->insert($data );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_category');
    }
}
