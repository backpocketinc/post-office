<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignSendListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('campaign_send_list')) {
            Schema::create('campaign_send_list', function (Blueprint $table) {
                $table->increments('id');
                 $table->integer('account_id')->length(10);
                 $table->integer('campaign_title_id')->length(10)->unsigned();
                 $table->string('list_id')->nullable();
                 $table->integer('group_id')->length(10);
                 $table->string('type')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_send_list');
    }
}
