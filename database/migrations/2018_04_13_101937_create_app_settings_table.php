<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('app_settings')) {
            Schema::create('app_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('logo')->nullable();
                $table->string('logo_sm')->nullable();
                $table->string('footer_text')->nullable();
                $table->string('login_bg')->nullable();
                $table->string('register_bg')->nullable();
                $table->string('sidebar_bg')->nullable();
                $table->string('forgot_password_bg')->nullable();
                $table->string('favicon')->nullable();
                $table->string('body_bg')->nullable();
                $table->string('sidebar_active')->nullable();
                $table->string('sidebar_bg_color')->nullable();
                $table->string('version')->nullable();
                $table->tinyInteger('mail_conf')->default(0);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });

            // add admin data
            DB::table('app_settings')->insert(
                [
                    'logo'                  => '',
                    'logo_sm'               => '',
                    'footer_text'           => '',
                    'login_bg'              => '',
                    'register_bg'           => '',
                    'sidebar_bg'            => '',
                    'forgot_password_bg'    => '',
                    'favicon'               => '',
                    'body_bg'               => '',
                    'sidebar_active'        => '',
                    'sidebar_bg_color'      => '',
                    'version'               => '1.1',
                    'mail_conf'             => 0
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_settings');
    }
}
