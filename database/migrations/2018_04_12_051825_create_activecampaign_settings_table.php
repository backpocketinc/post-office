<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivecampaignSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('activecampaign_settings')) {
            Schema::create('activecampaign_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->length(10)->nullable()->unsigned();
                $table->string('account_title');
                $table->string('activecampaign_key');
                $table->string('activecampaign_domain');
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activecampaign_settings');
    }
}
