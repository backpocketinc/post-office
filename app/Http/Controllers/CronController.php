<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\Http\Controllers\Admin\MailChimpController;

use App\Aweber_Setting;
use App\GetResponse_Setting;
use App\MailChimp_Setting;
use App\SendLane_Setting;
use App\Activecampaign_Setting;
use App\Http\Controllers\Controller;

use App\Aweber;
use App\Settings;
use Mailchimp\Mailchimp;
use Illuminate\Support\Facades\Config;
use App\Exceptions;
use App\EmailBroadcast;

use PHPMailer\PHPMailer\PHPMailer;

use \Mailjet\Resources;


class CronController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{

            $this->MailChimpStats();

        }catch (Exception $e)
        {

        }
    }

    public function MailChimpStats()
    {

        $mc = new MailChimpController();
        $mc->updateStats();
    }
    function fetchEmailsCron()
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);
        $category = DB::table('email_sub_category')->select('id','user_id', 'type','account_id', 'status')->where( ['status' => 0, 'category_id' => 6 ] )->get();
        //$category = DB::table('email_sub_category')->select('id','user_id', 'type','account_id', 'status')->where( ['status' => 1, 'category_id' => 6, 'id' => 28 ] )->get();

        foreach ($category as $item) {

            $account_id = $item->account_id;
            $user_id = $item->user_id;
            $autoresponder_type = $item->type;
            $subcategory_id = $item->id;
            try {
                // active campaign
                if ($autoresponder_type == 'activecampaign') {
                    $activecampaign_setting = Activecampaign_Setting::where('user_id', $user_id)->where('id', $account_id)->first();

                    if (count($activecampaign_setting) > 0) {

                        $api_key = $activecampaign_setting->activecampaign_key;
                        $domain = $activecampaign_setting->activecampaign_domain;
                        $url = $domain . '/admin/api.php';
                        // fetch message list for ids

                        $body_list = [
                            'api_key' => $api_key,
                            'api_action' => 'message_list',
                            'api_output' => 'json',
                            'ids' => 'all'
                        ];

                        $response = $this->Curl($url, $body_list, 'get', $get_response_key = '');
                        $response = json_decode($response);

                        if ($response->result_code != 1) {
                            exit;
                        }
                        // api list
                        $autoresponder_list = array();
                        foreach ($response as $item) {
                            if (isset($item->id)) {
                                array_push($autoresponder_list, $item->id);
                            }
                        }

                        //echo count($autoresponder_list).'<br>';

                        // fetc list from db

                        $list = DB::table('imported_campaign_list')
                            ->select(['campaign_id'])
                            ->where(
                                [
                                    'user_id' => $user_id,
                                    'type' => $autoresponder_type,
                                    'account_id' => $account_id
                                ]
                            )
                            ->whereIn('campaign_id', $autoresponder_list)
                            ->get();

                        // list of database
                        $database_list = array();
                        foreach ($list as $value) {
                            if (isset($value->campaign_id)) {
                                array_push($database_list, $value->campaign_id);
                            }
                        }

                        $insertable_list = array_diff($autoresponder_list, $database_list);

                        $list_total = count($insertable_list);
                        $count = 0;
                        foreach ($insertable_list as $message_id) {
                            $count++;

                            // fetch message templates
                            $body_list = [
                                'api_key' => $api_key,
                                'api_action' => 'message_view',
                                'api_output' => 'json',
                                'id' => $message_id
                            ];
                            // email templates api request
                            $response = $this->Curl($url, $body_list, 'get', $get_response_key = '');
                            $response = json_decode($response);

                            if (isset($response->subject) && isset($response->html)) {

                                $subject = $response->subject;
                                $content = $response->html;

                                $subject_id = DB::table('email_subjects')->insertGetId(
                                    [
                                        'user_id' => $user_id,
                                        'email_category_id' => '6',
                                        'sub_category_id' => $subcategory_id,
                                        'subject' => $subject,
                                        'status' => 1
                                    ]
                                );

                                DB::table('email_contents')->insert(
                                    [
                                        'email_category_id' => 6,
                                        'user_id' => $user_id,
                                        'subject_id' => $subject_id,
                                        'content' => $content,
                                        'status' => 1
                                    ]
                                );

                                // if not in list add email templates to database
                                DB::table('imported_campaign_list')->insert(
                                    ['user_id' => $user_id, 'campaign_id' => $message_id, 'type' => $autoresponder_type, 'account_id' => $account_id]
                                );
                            }

                            if ($count == $list_total) {
                                $category = DB::table('email_sub_category')->where(['id' => $subcategory_id])->update(['status' => 1]);
                            }
                        }
                    }
                }
                //getresponse
                if ($autoresponder_type == 'getresponse') {
                    $get_response = GetResponse_Setting::where('user_id', $user_id)->where('id', $account_id)->first();
                    if (count($get_response) > 0) {

                        $key = $get_response->get_response_key;

                        $autoresponder_list = array();
                        $ind = 0;
                        for ($page = 1; $page < 700; $page++) { $ind++;

                            $url = 'https://api.getresponse.com/v3/newsletters?fields=newsletterId&perPage=50&page='.$page.'';
                            $method = 'get';
                            $response = $this->Curl($url, '', $method, $key);
                            $response = json_decode($response);


                            if (count($response) > 0 )
                            {

                                foreach($response as $res)
                                {
                                    array_push($autoresponder_list, $res->newsletterId);
                                }

                            }else{
                                break;
                            }

                        }

                        //echo count($autoresponder_list).'<br>';

                        // fetc list from db

                        $list = DB::table('imported_campaign_list')
                            ->select(['campaign_id'])
                            ->where(
                                [
                                    'user_id' => $user_id,
                                    'type' => $autoresponder_type,
                                    'account_id' => $account_id
                                ]
                            )
                            ->whereIn('campaign_id', $autoresponder_list)
                            ->get();

                        // list of database
                        $database_list = array();
                        foreach ($list as $value) {
                            if (isset($value->campaign_id)) {
                                array_push($database_list, $value->campaign_id);
                            }
                        }
                        $insertable_list = array_diff($autoresponder_list, $database_list);

                        $list_total = count($insertable_list);
                        $count = 0;
                        foreach ($insertable_list as $message_id) {
                            $count++;

                            $url = 'https://api.getresponse.com/v3/newsletters/' . $message_id . '?fields=subject,content';

                            $method = 'get';
                            $response = $this->Curl($url, '', $method, $key);
                            $response = json_decode($response);

                            $subject = $response->subject;

                            //$content = $response->content->plain;
                            if ($response->content->plain == "")
                            {
                                $content = $response->content->html;
                            }else if ($response->content->html == "")
                            {
                                $content = $response->content->plain;
                            }

                            if (isset($subject) && isset($content)) {

                                $subject_id = DB::table('email_subjects')->insertGetId(
                                    [
                                        'user_id' => $user_id,
                                        'email_category_id' => '6',
                                        'sub_category_id' => $subcategory_id,
                                        'subject' => $subject,
                                        'status' => 1
                                    ]
                                );

                                DB::table('email_contents')->insert(
                                    [
                                        'email_category_id' => 6,
                                        'user_id' => $user_id,
                                        'subject_id' => $subject_id,
                                        'content' => $content,
                                        'status' => 1
                                    ]
                                );

                                // populate list

                                DB::table('imported_campaign_list')->insert(
                                    ['user_id' => $user_id, 'campaign_id' => $message_id, 'type' => $autoresponder_type, 'account_id' => $account_id]
                                );

                            }
                            if ($count == $list_total) {

                                $category = DB::table('email_sub_category')->where(['id' => $subcategory_id])->update(['status' => 1]);
                            }
                        }
                    }
                }
                // aweber

                if ($autoresponder_type == 'aweber') {

                    $settings = Aweber_Setting::where('user_id', $user_id)->where('id', $account_id)->first();

                    if (count($settings) > 0) {

                        require_once(getcwd() . '/aweber_api/aweber_api.php');

                        $aweber = new \AWeberAPI($settings->aweber_key, $settings->aweber_secret);
                        $aweber->adapter->debug = false;

                        $account = $aweber->getAccount($settings->accessTokenKey, $settings->accessTokenSecret);
                        $list = $account->lists;
                        $response = $list->data['entries'];

                        $aweber_account_id = $account->url;

                        // api list
                        $autoresponder_list = array();
                        foreach ($response as $item) {

                            if (isset($item['id'])) {
                                array_push($autoresponder_list, $item['id']);
                            }
                        }
                        //echo count($autoresponder_list).'<br>';
                        // fetc list from db

                        $list = DB::table('imported_campaign_list')
                            ->select(['campaign_id'])
                            ->where(
                                [
                                    'user_id' => $user_id,
                                    'type' => $autoresponder_type,
                                    'account_id' => $account_id
                                ]
                            )
                            ->whereIn('campaign_id', $autoresponder_list)
                            ->get();

                        // list of database
                        $database_list = array();
                        foreach ($list as $value) {
                            if (isset($value->campaign_id)) {
                                array_push($database_list, $value->campaign_id);
                            }
                        }
                        $insertable_list = array_diff($autoresponder_list, $database_list);
                        $count = 0;

                        $list_total = count($insertable_list);
                        foreach ($insertable_list as $listId) {
                            $count++;
                            // populate list

                            $url1 = "$aweber_account_id/lists/$listId/broadcasts";
                            $parameters = array(
                                'status' => 'sent'
                            );
                            $res = $aweber->adapter->request('get', $url1, $parameters, array());

                            foreach ($res['entries'] as $value) {

                                $broadID = explode('/', $value['self_link']);

                                $broadID = end($broadID);

                                $url2 = $aweber_account_id . '/lists/' . $listId . '/broadcasts/' . $broadID;

                                $res = $aweber->adapter->request('get', $url2, array(), array());

                                $content = $res['body_html'];
                                $subject = $res['subject'];

                                if (isset($subject) && isset($content)) {

                                    $subject_id = DB::table('email_subjects')->insertGetId(
                                        [
                                            'user_id' => $user_id,
                                            'email_category_id' => '6',
                                            'sub_category_id' => $subcategory_id,
                                            'subject' => $subject,
                                            'status' => 1
                                        ]
                                    );

                                    DB::table('email_contents')->insert(
                                        [
                                            'email_category_id' => 6,
                                            'user_id' => $user_id,
                                            'subject_id' => $subject_id,
                                            'content' => $content,
                                            'status' => 1
                                        ]
                                    );

                                    DB::table('imported_campaign_list')->insert(
                                        ['user_id' => $user_id, 'campaign_id' => $listId, 'type' => $autoresponder_type, 'account_id' => $account_id]
                                    );
                                }
                            }

                            if ($count == $list_total) {
                                $category = DB::table('email_sub_category')->where(['id' => $subcategory_id])->update(['status' => 1]);
                            }

                        }
                    }
                }

                // mailchimp

                if ($autoresponder_type == 'mailchimp') {

                    $mailchimp = MailChimp_Setting::where('user_id', $user_id)->where('id', $account_id)->first();

                    if (count($mailchimp) > 0) {

                        $api_key = $mailchimp->mailchimp_key;

                        $mc = new Mailchimp($api_key);

                        $result_cr = $mc->get('campaigns', [
                            'fields' => 'campaigns',
                            'count' => 10000

                        ]);

                        $subject_array = array();

                        $autoresponder_list = array();
                        foreach ($result_cr as $value) {
                            if (isset($value->id)) {
                                array_push($autoresponder_list, $value->id);
                            }
                            if (isset($value->settings->subject_line)) {
                                array_push($subject_array, array($value->id => $value->settings->subject_line));
                            }
                        }
                        // fetc list from db

                        $list = DB::table('imported_campaign_list')
                            ->select(['campaign_id'])
                            ->where(
                                [
                                    'user_id' => $user_id,
                                    'type' => $autoresponder_type,
                                    'account_id' => $account_id
                                ]
                            )
                            ->whereIn('campaign_id', $autoresponder_list)
                            ->get();

                        // list of database
                        $database_list = array();
                        foreach ($list as $value) {
                            if (isset($value->campaign_id)) {
                                array_push($database_list, $value->campaign_id);
                            }
                        }

                        $insertable_list = array_diff($autoresponder_list, $database_list);

                        foreach ($subject_array as $value) {

                            $list_total = count($value);
                            $count = 0;

                            foreach ($value as $index => $subject) {
                                $count++;


                                $result = $mc->get('campaigns/' . $index . '/content', []);

                                if (isset($result['plain_text'])) {
                                    $content = $result['plain_text'];
                                }

                                if (isset($subject) && isset($content)) {

                                    $subject = str_replace('*|FNAME|*', '', $subject);

                                    $subject = stripslashes($subject);
                                    $subject = strip_tags($subject);

                                    $subject = preg_replace('/[^[:print:]]/', '', $subject);

                                    $subject_id = DB::table('email_subjects')->insertGetId(
                                        [
                                            'user_id' => $user_id,
                                            'email_category_id' => '6',
                                            'sub_category_id' => $subcategory_id,
                                            'subject' => $subject,
                                            'status' => 1
                                        ]
                                    );

                                    DB::table('email_contents')->insert(
                                        [
                                            'email_category_id' => 6,
                                            'user_id' => $user_id,
                                            'subject_id' => $subject_id,
                                            'content' => $content,
                                            'status' => 1
                                        ]
                                    );

                                    // if not in list add email templates to database
                                    DB::table('imported_campaign_list')->insert(
                                        ['user_id' => $user_id, 'campaign_id' => $index, 'type' => $autoresponder_type, 'account_id' => $account_id]
                                    );
                                }

                                if ($count == $list_total) {

                                    $category = DB::table('email_sub_category')->where(['id' => $subcategory_id])->update(['status' => 1]);
                                }
                            }
                        }
                    }
                }
            }catch(\Exception $e)
            {
                //
            }
        }

//27888
    } // end function

    /*
     *  autoresponder fetch lists
     */
    function autoresponderFetchLists()
    {

        $settings = Aweber_Setting::all();

        if (count($settings) > 0) {
            $count = 0;
            foreach ($settings as $item) { $count++;
                require_once(getcwd() . '/aweber_api/aweber_api.php');
                try {
                    $aweber = new \AWeberAPI($item->aweber_key, $item->aweber_secret);
                    $aweber->adapter->debug = false;
                    try {
                        $account = $aweber->getAccount($item->accessTokenKey, $item->accessTokenSecret);
                    }catch(\Exception $e)
                    {
                        //echo  $e->getMessage();
                    }
                    //exit;
                    $list = $account->lists;
                    $response = $list->data['entries'];
                    $aweber_account_id = $account->url;
                    $url = $aweber_account_id . '/lists';
                    $res = $aweber->adapter->request('get', $url, array(), array());

                    if (isset($res['entries'])) {
                        //exit;
                        foreach ($res['entries'] as $value) {
                            $list_id = $value['id'];
                            $check_db = DB::table('autoresponder_lists')
                                ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'list_id' => $list_id , 'type' => 'aweber'])
                                ->first();
                            if (!$check_db)
                            {
                                DB::table('autoresponder_lists')
                                    ->insert([
                                        'list_name' => $value['name'],
                                        'account_id' => $item->id,
                                        'user_id' => $item->user_id,
                                        'list_id' => $list_id ,
                                        'type' => 'aweber'
                                     ]);
                            }
                        }
                    }
                } catch (\Exception $e) {
                    //echo $e->getMessage();
                }
            }
        }// end aweber

        /*
         * get response
         */
        $get_response = GetResponse_Setting::all();

        if (count($get_response) > 0) {
            foreach ($get_response as $item) {
                try{
                    $key = $item->get_response_key;

                    $url_newsletter_post = 'https://api.getresponse.com/v3/accounts?fields=accountId';
                    $response = $this->Curl($url_newsletter_post, $json = '', 'get', $key);
                    $res_decoded = json_decode($response);

                    if (isset($res_decoded->accountId)) {
                        //$url = 'https://api.getresponse.com/v3/contacts';
                        //$url = 'https://api.getresponse.com/v3/campaigns/nWig/contacts';
                        $url = 'https://api.getresponse.com/v3/campaigns/';
                        $method = 'get';
                        $response_lists = $this->Curl($url, '', $method, $key);
                        $response_lists = json_decode($response_lists);
                        //print_r($response_lists);

                        if (count($response_lists) > 0) {

                            foreach ($response_lists as $camp) {

                                $check_db = DB::table('autoresponder_lists')
                                    ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'list_id' => $camp->campaignId , 'type' => 'get_response'])
                                    ->first();
                                if (!$check_db)
                                {
                                    DB::table('autoresponder_lists')
                                        ->insert([
                                            'list_name' => $camp->name,
                                            'account_id' => $item->id,
                                            'user_id' => $item->user_id,
                                            'list_id' => $camp->campaignId ,
                                            'type' => 'get_response'
                                        ]);
                                }
                            }
                        }
                    }
                }catch(\Exception $e)
                {
                    echo  $e->getMessage();
                }
            }

        }// end get response

        /*
         * mailchimp fetch lists
         */

        $mailchimp = MailChimp_Setting::all();

        if (count($mailchimp) > 0) {
            foreach ($mailchimp as $item) {
                $api_key = $item->mailchimp_key;
                try {
                    $mc = new Mailchimp($api_key);
                    $result_cr = $mc->get('lists', [
                        'fields' => 'lists',
                        'count' => 100000
                    ]);
                } catch (\Exception $e) {
                    $err = $e->getMessage();
                    //echo $err;
                }

                if (isset($result_cr)) {
                    foreach ($result_cr as $value) {
                        $list_id = $value->id;
                        $list_name = $value->name;

                        $check_db = DB::table('autoresponder_lists')
                            ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'list_id' => $list_id , 'type' => 'mailchimp'])
                            ->first();
                        if (!$check_db)
                        {
                            DB::table('autoresponder_lists')
                                ->insert([
                                    'list_name' => $list_name,
                                    'account_id' => $item->id,
                                    'user_id' => $item->user_id,
                                    'list_id' => $list_id,
                                    'type' => 'mailchimp'
                                ]);
                        }
                    }
                }
            }
        }// end mailchimp


        /*
         * mailchimp fetch contact lists
         */
        $activecampaign_setting = Activecampaign_Setting::all();

        if (count($activecampaign_setting) > 0) {

            foreach ($activecampaign_setting as $item) {

                $api_key = $item->activecampaign_key;
                $domain = $item->activecampaign_domain;
                $url = $domain . '/admin/api.php';
                // fetch message list for ids

                $body_list = [
                    'api_key' => $api_key,
                    'api_action' => 'contact_list',
                    'api_output' => 'json',
                    'ids' => 'all'
                ];

                try {
                    $response = $this->Curl($url, $body_list, 'get', $get_response_key = '');
                    $response = json_decode($response);

                    if (isset($response)) {

                        foreach ($response as $value) {

                            if (isset($value->listid)) {

                                // check unique
                                $check_db = DB::table('autoresponder_lists')
                                    ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'list_id' => $value->listid , 'type' => 'activecampaign'])
                                    ->first();
                                if (!$check_db)
                                {
                                    DB::table('autoresponder_lists')
                                        ->insert([
                                            'list_name' => $value->listname,
                                            'account_id' => $item->id,
                                            'user_id' => $item->user_id,
                                            'list_id' => $value->listid,
                                            'type' => 'activecampaign'
                                        ]);
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $err = $e->getMessage();
                }
            }
        }
    }

    function aweberFetchContacts()
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);
        //error_reporting(0);

            $settings = Aweber_Setting::all();

            if (count($settings) > 0) {
                $count = 0;
                foreach ($settings as $item) { $count++;

                    require_once(getcwd() . '/aweber_api/aweber_api.php');

                    try {
                        $aweber = new \AWeberAPI($item->aweber_key, $item->aweber_secret);
                        $aweber->adapter->debug = false;

                        try {
                            $account = $aweber->getAccount($item->accessTokenKey, $item->accessTokenSecret);
                        }catch(\Exception $e)
                        {
                            echo  $e->getMessage();
                        }
                        //exit;
                        $list = $account->lists;
                        $response = $list->data['entries'];
                        $aweber_account_id = $account->url;
                        $url = $aweber_account_id . '/lists';
                        $res = $aweber->adapter->request('get', $url, array(), array());

                        if (isset($res['entries'])) {
                            //exit;

                            foreach ($res['entries'] as $value) {
                                $list_id = $value['id'];

                                // getting contact data by list id from api
                                $url = $aweber_account_id . '/lists/' . $list_id . '/subscribers';
                                $res = $aweber->adapter->request('get', $url, array(), array());

                                $check_db = DB::table('autoresponder_contact')
                                    ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'type' => 'aweber'])
                                    ->select('email')
                                    ->get()
                                    ->toArray();

                                $db_emails = array();

                                foreach($check_db as $email)
                                {
                                    array_push($db_emails, $email);
                                }

                                foreach ($res['entries'] as $value) {

                                    if (!in_array($value['email'], $db_emails)) {

                                        // insert data to contact table
                                        $insert = DB::table('autoresponder_contact')
                                            ->insert([
                                                'account_title' => $item->account_title,
                                                'account_id' => $item->id,
                                                'user_id' => $item->user_id,
                                                'list_name' => $value['name'],
                                                'type' => 'aweber',
                                                'list_id' => $list_id,
                                                'contact_id' => $value['id'],
                                                'name' => $value['name'],
                                                'email' => $value['email'],
                                                'ip_address' => $value['ip_address'],
                                                'area_code' => $value['area_code'],
                                                'country' => $value['country'],
                                                'region' => $value['region'],
                                                'longitude' => $value['longitude'],
                                                'status' => $value['status']
                                            ]);
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }// end aweber

    }

    function getresponseFetchContacts()
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);

echo ' start time =  ' . date( 'H:i:s');
echo '<br>';
            //$get_response = GetResponse_Setting::where('user_id', '=', 88)->get();
            $get_response = GetResponse_Setting::all();

            if (count($get_response) > 0) {
                foreach ($get_response as $item) {
                    try{
                    $key = $item->get_response_key;

                    $url_newsletter_post = 'https://api.getresponse.com/v3/accounts?fields=accountId';
                    $response = $this->Curl($url_newsletter_post, $json = '', 'get', $key);
                    $res_decoded = json_decode($response);

                    if (isset($res_decoded->accountId)) {
                        //$url = 'https://api.getresponse.com/v3/contacts';
                        //$url = 'https://api.getresponse.com/v3/campaigns/nWig/contacts';

                        $url = 'https://api.getresponse.com/v3/campaigns/';
                        $method = 'get';

                        $response_lists = $this->Curl($url, '', $method, $key);
                        $response_lists = json_decode($response_lists);
                        //print_r($response_lists);


                        if (count($response_lists) > 0) {

                            $response_lists = array_reverse($response_lists);

                            foreach ($response_lists as $camp) {

                                for ($page = 1; $page < 700; $page++) {
                                    // check

                                    // $url = 'https://api.getresponse.com/v3/campaigns/' . $camp->campaignId . '/contacts';
                                    $url = 'https://api.getresponse.com/v3/campaigns/' . $camp->campaignId . '/contacts?perPage=50&page=' . $page . '';
                                    //$url = 'https://api.getresponse.com/v3/campaigns/nWig/contacts?perPage=50&page='. $page.'';
                                    //echo '<br>';
                                    $method = 'get';
                                    $contacts_res = $this->Curl($url, '', $method, $key);
                                    $contacts_res = json_decode($contacts_res);


                                    if (count($contacts_res) > 0) {

                                        $check_page = DB::table('autoresponder_contact')
                                            ->where(['account_id' => $item->id, 'user_id' => $item->user_id, 'list_id' => $camp->campaignId, 'page_no' => $page, 'type' => 'get_response'])
                                            ->select('email')
                                            ->get()
                                            ->toArray();

                                        $db_email = array();
                                        foreach ($check_page as $emails) {
                                            array_push($db_email, $emails->email);
                                        }

                                        $contacts_res = array_reverse($contacts_res);
                                        //echo '<br>loop <br>';    27887

                                        foreach ($contacts_res as $value) {

                                            if (isset($value->campaign->campaignId)) {

                                                if (!in_array($value->email, $db_email)) {
                                                    $insert = DB::table('autoresponder_contact')
                                                        ->insert([
                                                            'account_title' => $item->account_title,
                                                            'account_id' => $item->id,
                                                            'user_id' => $item['user_id'],
                                                            'list_name' => $value->campaign->name,
                                                            'type' => 'get_response',
                                                            'list_id' => $value->campaign->campaignId,
                                                            'contact_id' => $value->contactId,
                                                            'name' => $value->name,
                                                            'email' => $value->email,
                                                            'ip_address' => $value->ipAddress,
                                                            'page_no' => (int)$page
                                                        ]);
                                                }

                                            }
                                        }
                                    } else {
                                        //echo 'here';
                                        break;
                                    }

                                }// end for loop

                            }
                        }
                    }
                }catch(\Exception $e)
                    {
                    echo  $e->getMessage();
                }

                }

                echo ' end time =  ' . date( 'H:i:s');
                echo '<br>';

            }// end get response

    }

    function mailchimpFetchContacts()
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);

        try {
            $mailchimp = MailChimp_Setting::all();

            if (count($mailchimp) > 0) {

                foreach ($mailchimp as $row) {
                    $api_key = $row->mailchimp_key;
                    try {

                        $mc = new Mailchimp($api_key);
                        $result_cr = $mc->get('lists', [
                            'fields' => 'lists',
                            'count' => 100000
                        ]);

                    } catch (\Exception $e) {
                        $err = $e->getMessage();
                        //echo $err;
                    }

                    if (isset($result_cr)) {

                        foreach ($result_cr as $value) {
                            $list_id = $value->id;
                            $list_name = $value->name;

                            // getting contact data by list id from api
                            $response = $mc->get('lists/'. $list_id.'/members', [
                                'fields' => 'members',
                                'count' => 1000000
                            ]);


                            $check_page = DB::table('autoresponder_contact')
                                ->where(['account_id' => $row->id,  'user_id' => $row->user_id, 'type' => 'mailchimp'])
                                ->select('email')
                                ->get()
                                ->toArray();


                            $db_email = array();
                            foreach ($check_page as $emails) {
                                array_push($db_email, $emails->email);
                            }


                            foreach ($response as $item) {

                                if (!in_array( $item->email_address, $db_email)) {

                                    $id = $item->id;
                                    $status = $item->status;
                                    $email = $item->email_address;
                                    $name = $item->merge_fields->FNAME;
                                    $ip = $item->ip_opt;

                                    $longi = $item->location->longitude;
                                    $lati = $item->location->latitude;
                                    $country = $item->location->country_code;


                                    // insert data to contact table
                                    $insert = DB::table('autoresponder_contact')
                                        ->insert([
                                            'account_title' => $row->account_title,
                                            'account_id' => $row->id,
                                            'user_id' => $row->user_id,
                                            'list_name' => $list_name,
                                            'type' => 'mailchimp',
                                            'list_id' => $list_id,
                                            'contact_id' => $id,
                                            'name' => $name,
                                            'email' => $email,
                                            'ip_address' => $ip,
                                            'area_code' => '',
                                            'country' => $country,
                                            'region' => '',
                                            'longitude' => $longi,
                                            'latitude' => $lati,
                                            'status' => $status
                                        ]);
                                }
                            }
                        }
                    }
                }
            }// end mailchimp

        }catch(\Exception $e)
        {
            //dd($e->getMessage());
            //echo $e->getMessage();
        }
    }

    function activecampaignFetchContacts()
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);

        try {
            $activecampaign_setting = Activecampaign_Setting::all();

            if (count($activecampaign_setting) > 0) {

                foreach ($activecampaign_setting as $row) {

                    $api_key = $row->activecampaign_key;
                    $domain = $row->activecampaign_domain;
                    $url = $domain . '/admin/api.php';
                    // fetch message list for ids

                    $body_list = [
                        'api_key' => $api_key,
                        'api_action' => 'contact_list',
                        'api_output' => 'json',
                        'ids' => 'all'
                    ];

                    try {
                        $response = $this->Curl($url, $body_list, 'get', $get_response_key = '');
                        $response = json_decode($response);
                    } catch (\Exception $e) {
                        $err = $e->getMessage();
                    }

                    $check_page = DB::table('autoresponder_contact')
                        ->where(['account_id' => $row->id,  'user_id' => $row->user_id, 'type' => 'activecampaign'])
                        ->select('email')
                        ->get()
                        ->toArray();


                    $db_email = array();
                    foreach ($check_page as $emails) {
                        array_push($db_email, $emails->email);
                    }

                    if (isset($response)) {

                        foreach ($response as $value) {

                            if (isset($value->listid)) {

                                if (!in_array( $value->email, $db_email)) {

                                    // insert data to contact table
                                    $insert = DB::table('autoresponder_contact')
                                        ->insert([
                                            'account_title' => $row->account_title,
                                            'account_id' => $row->id,
                                            'user_id' => $row->user_id,
                                            'list_name' => $value->listname,
                                            'type' => 'activecampaign',
                                            'list_id' => $value->listid,
                                            'contact_id' => $value->id,
                                            'name' => $value->name,
                                            'email' => $value->email,
                                            'ip_address' => $value->ip,
                                            'area_code' => '',
                                            'country' => '',
                                            'region' => '',
                                            'longitude' => '',
                                            'latitude' => '',
                                            'status' => $value->status,
                                            'subscribed_at' => $value->cdate,
                                            'phone' => $value->phone
                                        ]);
                                }
                            }
                        }
                    }
                }
            }

        }catch(\Exception $e)
        {
            //dd($e->getMessage());
        }
    }

    function fetchAutorespondersContacts(Request $request)
    {
        set_time_limit(2500000);
        ini_set('max_execution_time', 2500000);

        try {

        }catch(\Exception $e)
        {
            //
        }
    }

    function scheduleAutoresponders()
    {

        $data = DB::table('autoresponder_schedule')
            ->where(['status' => 1])
            ->where('type', '!=', 'aweber')
            ->get();

        foreach($data as $val)
        {

            if ($val->type == 'smtp_send')
            {
                $json = $val->json;
                $smtp_data = json_decode($json);
                ###

                $email_sender = DB::table('email_sender')
                    ->select('*')
                    ->where(['id' => $val->api_key, 'user_id' => $val->user_id ])
                    ->first();


                $from_name = $email_sender->from_name;
                $from_email = $email_sender->from_email;
                $api_key = $email_sender->api_key;
                $domain = $email_sender->domain;

                $list_id = $smtp_data->list_id;
                $body = $smtp_data->html;

                $broadcast_id = $val->email_brodcast_id;
                $subject_id = $val->subject_id;
                $email_campaign_title_id = $val->email_campaign_title_id;
                $email_category_id = $val->email_category_id;
                $subject = $val->subject;
                $user_id = $val->user_id;

                $type = 'smtp';

                $list_data = DB::table('autoresponder_contact')
                    ->join('autoresponder_lists', 'autoresponder_lists.list_id', '=', 'autoresponder_contact.list_id' )
                    ->select('autoresponder_contact.account_id', 'autoresponder_contact.list_id')
                    ->where(['autoresponder_contact.user_id' => $val->user_id])
                    ->whereIn('autoresponder_lists.id', $list_id)
                    ->groupBy('autoresponder_lists.list_id')
                    ->get();

                //print_r($list_data);
                foreach($list_data as $item)
                {
                    $ins = DB::table('campaign_send_list')
                        ->insert(['group_id' => $smtp_data->group_id, 'account_id' => $item->account_id, 'campaign_title_id' => $val->email_campaign_title_id, 'list_id' => $item->list_id, 'type' => 'smtp']);
                }
                $raw_email = DB::table('autoresponder_contact')
                        ->join('autoresponder_lists', 'autoresponder_lists.list_id', '=', 'autoresponder_contact.list_id' )
                        ->select('autoresponder_contact.email')
                        //->where(['autoresponder_contact.user_id' => $val->user_id])
                        //->whereIn('autoresponder_lists.id', $list_id)
                        ->groupBy('autoresponder_contact.email')
                        ->get();

                   // echo $to
                $error = '';
                    if ($email_sender->type == 'mailingreen') {
                        foreach ($raw_email as $value) {
                            $to = $value->email;
                            $tracking_img = "<img border='0' height='1' src='".asset('')."parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=".$to."' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];

                            $User = DB::table('users')->where('id', $val->user_id)->first();
                            $raw_name = explode(' ', $User->name);
                            $first_name = $last_name = '';
                            if(isset($raw_name[0])){
                                $first_name = $raw_name[0];
                            }
                            if(isset($raw_name[1])){
                                $last_name = $raw_name[1];
                            }
                            $str = str_replace('%first_name%',$first_name, $str);
                            $str = str_replace('%last_name%',$last_name, $str);
                            $str = str_replace('%email_address%',$User->email, $str);

                            // update click link tracker
                            if (count($link_tracking_id) > 0) {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            $str = trim(preg_replace('/\s+/', " ", $str));
                            $body = preg_replace("/\"/", "\\\"", $str);

                            $mailin = new Mailin("https://api.sendinblue.com/v2.0", $api_key);
                            $data = array("to" => array($to => $to_name),
                                "from" => array($from_email, $from_name),
                                "subject" => $subject,
                                "html" => $body
                            );

                            $result = $mailin->send_email($data);
                            if ($result['code'] == 'failure') {
                                $error =  $result['message'];
                                //break;
                            }

                            if ($result['code'] == 'success') {
                                if (strpos($result['message'], 'has not been activated') !== false) {
                                    $error =  $result['message'];
                                    //exit;
                                }
                            }
                        }

                        if ($error !="")
                        {
                            // error
                            DB::table('autoresponder_error')
                                ->insert(
                                    [
                                        'email_campaign_title_id' => $val->email_campaign_title_id,
                                        'error' => $result['message'],
                                        'user_id' => $val->user_id
                                    ]);
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 1]
                                );

                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            //exit;
                        }else{
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 2]
                                );

                            echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);
                            //exit;
                        }

                        // update status of crone
                        DB::table('autoresponder_schedule')
                            ->where(['id' => $val->id])
                            ->update(['status' => 0]);



                    }//end mailingreen

                    if ($email_sender->type == 'mailjet') {
                        // include library
                        require 'vendor/autoload.php';

                        $private_api_key = $email_sender->private_api_key;

                        foreach ($raw_email as $value) {
                            $to = $value->email;
                            $smtp_data->html;
                            $tracking_img = "<img border='0' height='1' src='".asset('')."parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];

                            $User = DB::table('users')->where('id', $val->user_id)->first();
                            $raw_name = explode(' ', $User->name);
                            $first_name = $last_name = '';
                            if(isset($raw_name[0])){
                                $first_name = $raw_name[0];
                            }
                            if(isset($raw_name[1])){
                                $last_name = $raw_name[1];
                            }
                            $str = str_replace('%first_name%',$first_name, $str);
                            $str = str_replace('%last_name%',$last_name, $str);
                            $str = str_replace('%email_address%',$User->email, $str);

                            // update click link tracker
                            if (count($link_tracking_id) > 0) {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            $str = trim(preg_replace('/\s+/', " ", $str));
                            $body = preg_replace("/\"/", "\\\"", $str);

                            //$mj = new \Mailjet\Client(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),
                            $mj = new \Mailjet\Client($api_key, $private_api_key,
                                true, ['version' => 'v3.1']);
                            $data = [
                                'Messages' => [
                                    [
                                        'From' => [
                                            'Email' => $from_email,
                                            'Name' => $from_name
                                        ],
                                        'To' => [
                                            [
                                                'Email' => $to,
                                                'Name' => $to_name
                                            ]
                                        ],
                                        'Subject' => $subject,
                                        'TextPart' => "",
                                        'HTMLPart' => $body
                                    ]
                                ]
                            ];
                            $response = $mj->post(Resources::$Email, ['body' => $data]);
                            //$response->success();

                            if ($response->success()) {
                                echo json_encode(array('id' => $subject_id, 'msg' => 'Email has been resend.'));

                            } else {
                                //print_r($response->getStatus());
                                $data = $response->getData();
                                if (isset($data['ErrorMessage'])) {
                                    // error
                                    $error =  $data['ErrorMessage'];
                                    //break;

                                } else {
                                    if (isset($data['Messages'][0]['Errors'][0]['ErrorMessage'])) {
                                        // error
                                      $error =  $data['Messages'][0]['Errors'][0]['ErrorMessage'];
                                        //break;
                                    }
                                }
                            }

                        }// end foreach

                        if ($error !="")
                        {
                            // error

                            DB::table('autoresponder_error')
                                ->insert(
                                    [
                                        'email_campaign_title_id' => $val->email_campaign_title_id,
                                        'error' => $error,
                                        'user_id' => $val->user_id
                                    ]);
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 1]
                                );

                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                        }else{
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 2]
                                );

                            echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);
                        }

                        // update status of crone
                        DB::table('autoresponder_schedule')
                            ->where(['id' => $val->id])
                            ->update(['status' => 0]);

                    }// end mailjet if

                    if ($email_sender->type == 'smtp') {

                        require 'vendor/autoload.php';
                        // creating variables
                        $host = $email_sender->host;
                        $port = $email_sender->port;
                        $user = $email_sender->user;
                        $password = $email_sender->password;
                        $ssl = $email_sender->smtp_ssl;


                        foreach ($raw_email as $value) {
                            $to = $value->email;
                            $smtp_data->html;
                            $tracking_img = "<img border='0' height='1' src='".asset('')."parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];

                            $User = DB::table('users')->where('id', $val->user_id)->first();


                            $raw_name = explode(' ', $User->name);
                            $first_name = $last_name = '';
                            if(isset($raw_name[0])){
                                $first_name = $raw_name[0];
                            }
                            if(isset($raw_name[1])){
                                $last_name = $raw_name[1];
                            }
                            $str = str_replace('%first_name%',$first_name, $str);
                            $str = str_replace('%last_name%',$last_name, $str);
                            $body = str_replace('%email_address%',$User->email, $str);

                            // update click link tracker
                            if (count($link_tracking_id) > 0) {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            try {
                                $mail = new PHPMailer();
                                //Enable SMTP debugging.
                                $mail->SMTPDebug = false;
                                //Set PHPMailer to use SMTP.
                                $mail->isSMTP();
                                //Set SMTP host name
                                $mail->Host = $host;
                                //Set this to true if SMTP host requires authentication to send email
                                $mail->SMTPAuth = true;
                                //Provide username and password
                                $mail->Username = $user;
                                $mail->Password = $password;
                                //If SMTP requires TLS encryption then set it
                                if ($ssl != 1) {
                                    $mail->SMTPSecure = "tls";
                                } else {
                                    $mail->SMTPSecure = "ssl";
                                }
                                //Set TCP port to connect to
                                $mail->Port = $port;
                                // $mail->test = '25';
                                $mail->SMTPOptions = array('ssl' => array('verify_host' => false, 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));

                                $mail->From = $from_email;
                                $mail->FromName = $from_name;
                                $mail->addAddress($to, $to_name);
                                $mail->isHTML(true);
                                $mail->Subject = $subject;
                                $mail->Body = $body;
                                $mail->AltBody = "";
                                $send = $mail->send();
                            } catch (\Exception $e) {
                                  $err = ($e->getMessage());
                            }
                            if (!$send) {
                                $err = explode('.', $mail->ErrorInfo);
                                // error
                              $error =  $err[0];
                                //break;
                            }
                        }

                        if ($error !="")
                        {
                            // error
                            DB::table('autoresponder_error')
                                ->insert(
                                    [
                                        'email_campaign_title_id' => $val->email_campaign_title_id,
                                        'error' => $error,
                                        'user_id' => $val->user_id
                                    ]);
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 1]
                                );


                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            //exit;
                        }else{
                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('boradcast_id', $val->email_brodcast_id)
                                ->update(['status' => 2]
                                );

                            echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);

                        }

                         //update status of crone
                        DB::table('autoresponder_schedule')
                            ->where(['id' => $val->id])
                            ->update(['status' => 0]);

                    } else {

                        if ($email_sender->type == 'sparkpost') {
                            // sparkpost
                            foreach ($raw_email as $value) {
                                $to = $value->email;
                                $smtp_data->html;
                                $tracking_img = "<img border='0' height='1' src='".asset('')."parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "' width='1' />";
                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];

                                $User = DB::table('users')->where('id', $val->user_id)->first();
                                $raw_name = explode(' ', $User->name);
                                $first_name = $last_name = '';
                                if(isset($raw_name[0])){
                                    $first_name = $raw_name[0];
                                }
                                if(isset($raw_name[1])){
                                    $last_name = $raw_name[1];
                                }
                                $str = str_replace('%first_name%',$first_name, $str);
                                $str = str_replace('%last_name%',$last_name, $str);
                                $str = str_replace('%email_address%',$User->email, $str);

                                // update click link tracker
                                if (count($link_tracking_id) > 0) {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }

                                $str = trim(preg_replace('/\s+/', " ", $str));
                                $b = preg_replace("/\"/", "\\\"", $str);
                                $uri = 'https://api.sparkpost.com/api/v1/transmissions';
                                $json = '{
        "options": {
            "open_tracking": true,
            "click_tracking": true
        },
        "metadata": {
            "some_useful_metadata": ""
        },
        "substitution_data": {
            "signature": ""
        },
        "recipients": [
            {
                "address": {
                    "email": "' . $to . '"
                },
                "tags": [
                    "learning"
                ],
                "substitution_data": {
                    "customer_type": "Platinum",
                    "first_name": "' . $to_name . '"
                }
            }
        ],
        "content": {
            "from": {
                "name": "' . $from_name . '",
                "email": "' . $from_email . '"
            },
            "subject": "' . $subject . '",
            "text": "",
            "html": "' . $b . '"
        }
    }';


                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: $api_key"));
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                                $result = curl_exec($ch);
                                curl_close($ch);

                                $res = json_decode($result);
                                if (isset($res->errors)) {
                                    $err = $res->errors;
                                   $error = $err[0]->message;
                                    //break;
                                }
                                // =================== END SPARKPOST ================= //
                            }
                            if ($error !="")
                            {
                                // error
                                DB::table('autoresponder_error')
                                    ->insert(
                                        [
                                            'email_campaign_title_id' => $val->email_campaign_title_id,
                                            'error' => $error,
                                            'user_id' => $val->user_id
                                        ]);
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 1]
                                    );

                                echo json_encode([ 'date_err' => 1, 'msg' => $error ]);
                            }else{
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 2]
                                    );
                                echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);
                            }

                            // update status of crone
                            DB::table('autoresponder_schedule')
                                ->where(['id' => $val->id])
                                ->update(['status' => 0]);
                            //exit;
                        } elseif ($email_sender->type == 'sendgrid') {


                            foreach ($raw_email as $value) {

                                $to = $value->email;
                                $smtp_data->html;
                                $tracking_img = "<img border='0' height='1' src='".asset('')."/parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "' width='1' />";

                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];

                                $User = DB::table('users')->where('id', $val->user_id)->first();
                                $raw_name = explode(' ', $User->name);
                                $first_name = $last_name = '';
                                if(isset($raw_name[0])){
                                    $first_name = $raw_name[0];
                                }
                                if(isset($raw_name[1])){
                                    $last_name = $raw_name[1];
                                }
                                $str = str_replace('%first_name%',$first_name, $str);
                                $str = str_replace('%last_name%',$last_name, $str);
                                $str = str_replace('%email_address%',$User->email, $str);

                                // update click link tracker
                                if (count($link_tracking_id) > 0) {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }

                                $str = trim(preg_replace('/\s+/', " ", $str));
                                //$b = trim(preg_replace('/\s+/', " ", $body));
                                $b = preg_replace("/\"/", "\\\"", $str);
                                $uri = 'https://api.sendgrid.com/v3/mail/send';
                            $json = '{
            "personalizations": [
                    {
                        "to": [{
                                "email": "' . $to . '",
                                "name":"' . $to_name . '"
                        }]
                    }
                ],
                "from": {
                    "email": "' . $from_email . '",
                    "name":"' . $from_name . '"
                },
                "subject": "' . $subject . '",
                "content": [
                    {
                    "type": "text/html",
                    "value": "' . $b . '"
                }
            ]
        }';

                                $headers = array(
                                    'Authorization: Bearer ' . $api_key . '',
                                    'Content-Type: application/json'
                                );


                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                                $result = curl_exec($ch);
                                curl_close($ch);

                                //$result = json_encode( array('df' => 1));
                                $res = json_decode($result);


                                if (isset($res->errors)) {
                                    $err = $res->errors;
                                 $error = $err[0]->message;
                                   //break;
                                }
                            }


                            if ($error !="")
                            {
                                // error
                                DB::table('autoresponder_error')
                                    ->insert(
                                        [
                                            'email_campaign_title_id' => $val->email_campaign_title_id,
                                            'error' => $error,
                                            'user_id' => $val->user_id
                                        ]);
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 1]
                                    );
                                //exit;

                                echo json_encode(['date_err' => 1, 'msg' => $error]);

                            }else{
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 2]
                                    );
                                echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);
                            }


                            // update status of crone
                            DB::table('autoresponder_schedule')
                                ->where(['id' => $val->id])
                                ->update(['status' => 0]);


                        } elseif ($email_sender->type == 'mailgun') {

                            define('MAILGUN_URL', 'https://api.mailgun.net/v3/' . $domain . '');
                            define('MAILGUN_KEY', $api_key);


                            foreach ($raw_email as $value) {
                                $to = $value->email;
                                $smtp_data->html;
                                $tracking_img = "<img border='0' height='1' src='".asset('')."parser/tracking.php?user_id=" . $user_id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "' width='1' />";

                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];

                                $User = DB::table('users')->where('id', $val->user_id)->first();
                                $raw_name = explode(' ', $User->name);
                                $first_name = $last_name = '';
                                if(isset($raw_name[0])){
                                    $first_name = $raw_name[0];
                                }
                                if(isset($raw_name[1])){
                                    $last_name = $raw_name[1];
                                }
                                $str = str_replace('%first_name%',$first_name, $str);
                                $str = str_replace('%last_name%',$last_name, $str);
                                $str = str_replace('%edmail_address%',$User->emailma, $str);

                                // update click link tracker
                                if (count($link_tracking_id) > 0) {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }


                                $str = trim(preg_replace('/\s+/', " ", $str));
                                //$b = preg_replace("/\"/", "\\\"", $str);


                                $toname = $to_name;
                                $mailfromname = $from_name;
                                $mailfrom = $from_email;
                                $subject = $subject;
                                $html = $str;
                                $text = '';
                                $tag = '';

                                $replyto = env('MAIL_FROM_ADDRESS');

                                $array_data = array(
                                    'from' => $mailfromname . '<' . $mailfrom . '>',
                                    'to' => $toname . '<' . $to . '>',
                                    'subject' => $subject,
                                    'html' => $html,
                                    'text' => $text,
                                    'o:tracking' => 'yes',
                                    'o:tracking-clicks' => 'yes',
                                    'o:tracking-opens' => 'yes',
                                    'o:tag' => $tag,
                                    'h:Reply-To' => $replyto
                                );
                                $session = curl_init(MAILGUN_URL . '/messages');
                                curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($session, CURLOPT_USERPWD, 'api:' . MAILGUN_KEY);
                                curl_setopt($session, CURLOPT_POST, true);
                                curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
                                $response = curl_exec($session);
                                curl_close($session);
                                $results = json_decode($response, true);

                                if (!isset($results['id'])) {
                                    // error
                                  $error =  $results['message'];
                                   //break;
                                } elseif ($results == "") {
                                    $error =  'Invalid credentials!';
                                    //break;
                                }
                            }

                            if ($error !="")
                            {
                                // error

                                DB::table('autoresponder_error')
                                    ->insert(
                                        [
                                            'email_campaign_title_id' => $val->email_campaign_title_id,
                                            'error' => $error,
                                            'user_id' => $val->user_id
                                        ]);
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 1]
                                    );

                                echo json_encode(['date_err' => 1, 'msg' => $error]);
                                //exit;
                            }else{
                                // update broadcast
                                DB::table('email_broadcasts')
                                    ->where('boradcast_id', $val->email_brodcast_id)
                                    ->update(['status' => 2]
                                    );

                                echo json_encode(['id' => 1, 'msg' => 'Email send successfully']);
                                //exit;
                            }

                            // update status of crone
                            DB::table('autoresponder_schedule')
                                ->where(['id' => $val->id])
                                ->update(['status' => 0]);
                        }
                    }// end mailgun

            }//end smtp

            $start_time = new Carbon( $val->date_time);
            if ($start_time <= Carbon::today()) {

              if ($val->type == 'get_response') {
                  $json = $val->json;
                  $url_newsletter_post = 'https://api.getresponse.com/v3/newsletters';
                  $response = $this->Curl($url_newsletter_post, $json, 'post', $val->api_key);
                  $res_decoded = json_decode($response);
                  if (!isset($res_decoded->newsletterId))
                  {
                      $res_decoded->message;
                      // error

                      DB::table('autoresponder_error')
                          ->insert(
                              [
                                  'email_campaign_title_id' => $val->email_campaign_title_id,
                                  'error' => $res_decoded->message,
                                  'user_id' => $val->user_id
                              ]);
                      // update broadcast
                      DB::table('email_broadcasts')
                          ->where('id', $val->email_brodcast_id)
                          ->update(['status' => 1]
                          );
                      // update status of crone
                      DB::table('autoresponder_schedule')
                          ->where(['id' => $val->id])
                          ->update(['status' => 0]);
                      //exit;
                  }

                  $broadCastId = $res_decoded->newsletterId;

                  if (isset($res_decoded->newsletterId)) {
                      DB::table('autoresponder_schedule')
                          ->where(['id' => $val->id])
                          ->update(['status' => 0]);

                      // update
                      DB::table('email_broadcasts')
                          ->where('id', $val->email_brodcast_id)
                          ->update(['json' => $response, 'type' => 'get_response', 'user_id' => Auth::User()->id, 'subject_id' => $val->subject_id, 'boradcast_id' => $broadCastId, 'status' => 2]
                          );
                      $link_tracking_id = json_decode($val->click_link_id);
                      // update click link tracker
                      if (count($link_tracking_id) > 0) {
                          DB::table('click_links')
                              ->whereIn('id', $link_tracking_id)
                              ->update(
                                  [
                                      'type' => 'get_response',
                                      'subject_id' => $val->subject_id,
                                      'broadcast_id' => $broadCastId,
                                      'email_category_id' => $val->email_category_id,
                                      'email_campaign_title_id' => $val->email_campaign_title_id
                                  ]
                              );
                      }
                  }
              }elseif ( $val->type == 'mailchimp')
              {

                  $api_key = MailChimp_Setting::where('id', $val->api_key)->value('mailchimp_key');
                  $link_tracking_id = json_decode($val->click_link_id);
                  $json = json_decode( $val->json );

                  $list_id = $json->list_id;
                  $html = $json->html;
                  $title = $json->title;
                  $subject_line = $json->subject_line;
                  $from_name = $json->from_name;
                  $reply_to = $json->reply_to;


                  try {
                      $mc = new Mailchimp($api_key);
                      $selectCamapgin = $list_id;

                      $result_cr = $mc->post('campaigns', [
                          'type' => 'regular',
                          'recipients' => [
                              'list_id' => $list_id,
                          ],
                          'settings' => [
                              'title' => $title,
                              'subject_line' => $subject_line,
                              'from_name' => $from_name,
                              'reply_to' => $reply_to
                          ]
                      ]);
                  } catch (\Exception $e) {
                      $err = $e->getMessage();
                  }

                  if (isset($err)) {
                      $err = json_decode($err);
                      if (isset($err->detail))
                      {
                          //echo $err->detail;
                          // email and status change to error also addd to autoresponder error table
                          DB::table('autoresponder_error')
                              ->insert(
                                  [
                                      'email_campaign_title_id' => $val->email_campaign_title_id,
                                      'error' => $err->detail,
                                      'user_id' => $val->user_id
                                ]);
                          // update broadcast
                          DB::table('email_broadcasts')
                              ->where('id', $val->email_brodcast_id)
                              ->update(['status' => 1]
                              );
                          // update status of crone
                          DB::table('autoresponder_schedule')
                              ->where(['id' => $val->id])
                              ->update(['status' => 0]);
                      }
                      //exit;
                  }

                  $campaign_id = isset($result_cr['id']) ? $result_cr['id'] : '';
                  $emails_sent = isset($result_cr['emails_sent']) ? $result_cr['emails_sent'] : '';

                  if ($campaign_id != '') {
                      $broadCastId = $campaign_id;
                      $post_cn = $mc->put("campaigns/$campaign_id/content", [
                          'html' => $html
                      ]);
                      try {
                          $post_sn = $mc->post("campaigns/$campaign_id/actions/send", []);
                      } catch (\Exception $e) {
                          $err = $e->getMessage();
                      }

                      if (isset($err)) {
                          $err = json_decode($err);
                          // insert error
                          DB::table('autoresponder_error')
                              ->insert(
                                  [
                                      'email_campaign_title_id' => $val->email_campaign_title_id,
                                      'error' => $err->detail,
                                      'user_id' => $val->user_id
                                  ]);
                          // update broadcast
                          DB::table('email_broadcasts')
                              ->where('id', $val->email_brodcast_id)
                              ->update(['status' => 1]
                              );
                          // update status of crone
                          DB::table('autoresponder_schedule')
                              ->where(['id' => $val->id])
                              ->update(['status' => 0]);
                          //exit;
                      }else{
                          // update status of crone
                          DB::table('autoresponder_schedule')
                              ->where(['id' => $val->id])
                              ->update(['status' => 0]);

                          // update broadcast
                          DB::table('email_broadcasts')
                              ->where('id', $val->email_brodcast_id)
                              ->update(['sent' => $emails_sent, 'json' => '', 'type' => 'mailchimp', 'user_id' => Auth::User()->id, 'subject_id' => $val->subject_id, 'boradcast_id' => $broadCastId,'status' => 2]
                              );


                          // update click link tracker
                          if (count($link_tracking_id) > 0) {

                              DB::table('click_links')
                                  ->whereIn('id', $link_tracking_id)
                                  ->update(
                                      [
                                          'type' => 'mailchimp',
                                          'subject_id' => $val->subject_id,
                                          'broadcast_id' => $broadCastId,
                                          'email_category_id' => $val->email_category_id,
                                          'email_campaign_title_id' => $val->email_campaign_title_id
                                      ]
                                  );
                          }
                      }
                  }
              }

            }else{
              //echo 'scheduled';
            }
        }

    }

    function scheduleAweberBroadcast()
    {

        $data = DB::table('autoresponder_schedule')
            ->where(['status' => 1])
            ->where('type', '=', 'aweber')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();

        foreach($data as $val) {

                // aweber
                $link_tracking_id = json_decode($val->click_link_id);
                $json = json_decode($val->json);
                $list_id = $json->list_id;
                $html = $json->html;
                $title = $json->title;
                $subject_line = $json->subject_line;

                $settings = Aweber_Setting::where('id', '=', $val->api_key)->first();

            try {

                require_once(getcwd() . '/aweber_api/aweber_api.php');
                $aweber = new \AWeberAPI($settings->aweber_key, $settings->aweber_secret);
                $aweber->adapter->debug = false;
                $account = $aweber->getAccount($settings->accessTokenKey, $settings->accessTokenSecret);
//                $list = $account->lists;//
//                $entries = $list->data['entries'];//
//                print_r($entries);
//                exit;

                $listId = $list_id;
                $account_id = $account->url;

                $url2 = "$account_id/lists/$listId/broadcasts";
                $parameters = array(
                    'subject' => $subject_line,
                    'body_html' => $html,
                    'is_archived' => false,
                    'click_tracking_enabled' => true
                );

                $res = $aweber->adapter->request('post', $url2, $parameters, array());
                $response = json_encode($res);

                if (isset($res['broadcast_id'])) {

                    $broadCastId = $res['broadcast_id'];
                    $url2 = "$account_id/lists/$listId/broadcasts/$broadCastId/schedule";

                    $time = new Carbon($val->date_time);
                    $time = $time->addMinutes(2);
                    $dte = strtotime($time);

                    $parameters = array('scheduled_for' => date('c', $dte));

                    $res2 = $aweber->adapter->request('post', $url2, $parameters, array());

                }
            }catch(\Exception $e)
            {
                $err = $e->getMessage();
            }

            if (isset( $err )) {
                // error

                //print_r($err);


                DB::table('autoresponder_error')
                    ->insert(
                        [
                            'email_campaign_title_id' => $val->email_campaign_title_id,
                            'error' => $err,
                            'user_id' => $val->user_id
                        ]);
                // update broadcast
                DB::table('email_broadcasts')
                    ->where('id', $val->email_brodcast_id)
                    ->update(['status' => 1]
                    );
                if ($err != 'The API is temporarily unavailable. please try again.')
                {
                    // update status of crone
                    DB::table('autoresponder_schedule')
                        ->where(['id' => $val->id])
                        ->update(['status' => 0]);
                }
            }else{

                DB::table('autoresponder_schedule')
                    ->where(['id' => $val->id])
                    ->update(['status' => 0]);

                //update broadcast
                DB::table('email_broadcasts')
                    ->where('id', $val->email_brodcast_id)
                    ->update(['json' => $response, 'type' => 'aweber', 'user_id' => $val->user_id, 'subject_id' => $val->subject_id, 'boradcast_id' => $broadCastId, 'status' => 2]
                    );

                // update click link tracker
                if (count($link_tracking_id) > 0) {
                    DB::table('click_links')
                        ->whereIn('id', $link_tracking_id)
                        ->update(
                            [
                                'type' => 'aweber',
                                'subject_id' => $val->subject_id,
                                'broadcast_id' => $broadCastId,
                                'email_category_id' => $val->email_category_id,
                                'email_campaign_title_id' => $val->email_campaign_title_id
                            ]
                        );
                }
            }
        }
    }// end aweber schedule cron

     function Curl($url, $body, $method, $get_response_key)
     {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        //Get-Response API Request
        if ($get_response_key!='')
        {
            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Auth-Token: api-key $get_response_key"));
            if ($method == "post")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","X-Auth-Token: api-key $get_response_key"));
            //Aweber API Request
        }else{
            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPGET, true );
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_PUT, true );

        if ($method == "post")
            curl_setopt($ch, CURLOPT_POST, true );
        if ($method == "get")
            curl_setopt($ch, CURLOPT_HTTPGET, true );
        if ($body!="")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $filecontents=curl_exec($ch);
        curl_close($ch);
        return $filecontents;
    }

    ##############################    TEST CRON ##############################

    function testCron()
    {

        //multilist_group
        $data = DB::table('email_subjects')->where('sub_category_id', 28)->select('id','user_id')->get()->toArray();

        $ids = array();
        foreach($data as $value)
        {
            array_push($ids, $value->id);
        }

       $contents = DB::table('email_subjects')->whereIn('id', $ids)->delete();
//
//        $data = DB::table('autoresponder_contact')
//            ->join('autoresponder_new_stats', 'autoresponder_contact.email', '=', 'autoresponder_new_stats.email' )
//            ->select('autoresponder_contact.list_name','autoresponder_contact.email', 'autoresponder_contact.list_id', 'autoresponder_contact.user_id')
//            ->where(['autoresponder_contact.user_id' => 88, 'autoresponder_contact.type' => 'get_response', 'account_id' => 16])
//            ->get();
//
//        echo '<pre>';
//
//        print_r($data);

    }

    private function makeLinks($text, $email) {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        // The Text you want to filter for urls
        // Check if there is a url in the text
        if (preg_match_all($reg_exUrl, $text, $url)) {

        }
        $reg_exUrl = '/(?<!src=[\'"])(((f|ht)
        {1}tps?:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i';
        if (preg_match_all($reg_exUrl, $text, $url)) {
            // make the urls hyper links

            $matches = array_unique($url[0]);
            $ids = array();
            foreach($matches as $match) {
                $redirect_url = $match;/////////////

                // insert your emails to database and get id
                $insert_id = DB::table('click_links')->insertGetId(
                    [
                        'user_id' => Auth::user()->id,
                        'redirect_link' => $redirect_url
                    ]
                );

                $link = asset('').'parser/click_tracking.php?id='.$insert_id.'&email='.$email.'';
                $ids[] = $insert_id;

                $replacement = '<a href="'.$link.'">'.$match.'</a>';

                $replacement = '<a href="'.$link.'">'.$match.'</a>';
                $text = str_replace($match,$replacement,$text);
                $text = str_replace('<a href="<a href="', '<a href="' , $text);
                $text = str_replace(''.$match.'</a>">', '' , $text);

                $text = str_replace('scr="'.$replacement.'"',"src='$match'",$text);
                $text = str_replace("scr='".$replacement."'",'src="'.$match.'"',$text);

            }
            $return = array('ids' => $ids, 'email_html' =>   str_replace('"', "'" , $text));
            return $return;
        } else {
            // if no urls in the text just return the text
            $return = array('ids' => array(), 'email_html' => $text);
            return $return;
        }
    }
}
