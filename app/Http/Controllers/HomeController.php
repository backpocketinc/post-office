<?php

namespace App\Http\Controllers;

use App\Categories;
use App\EmailTemplate;
use App\Http\Controllers\Admin\MailChimpController;
use App\Settings;
use App\SubjectTemplate;
use App\EmailBroadcast;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use App\Aweber_Setting;
use App\GetResponse_Setting;
use App\Activecampaign_Setting;
use App\MailChimp_Setting;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // sent error, qued stats

        $sent = DB::table('email_broadcasts')
           // ->leftjoin('email_broadcasts', 'email_broadcasts.email_campaign_title_id', '=', 'autoresponder_schedule.email_campaign_title_id')
            //->where(['email_broadcasts.status' => 2, 'autoresponder_schedule.user_id' => Auth::user()->id ])
            ->where(['status' => 2, 'user_id' => Auth::user()->id ])
            ->count();

        $que = DB::table('autoresponder_schedule')
            ->join('email_broadcasts', 'email_broadcasts.email_campaign_title_id', '=', 'autoresponder_schedule.email_campaign_title_id')
            ->where(['autoresponder_schedule.status' => 1, 'autoresponder_schedule.user_id' => Auth::user()->id ])
            ->count();

        $sent_error = DB::table('autoresponder_schedule')
            ->join('email_broadcasts', 'email_broadcasts.email_campaign_title_id', '=', 'autoresponder_schedule.email_campaign_title_id')
            ->where(['email_broadcasts.status' => 1, 'autoresponder_schedule.user_id' => Auth::user()->id ])
            ->count();

        //end to stats

        $total_opened =  DB::table('email_campaign_title')
            ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
            ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
            ->join('autoresponder_new_stats', 'email_campaign_title.id', '=', 'autoresponder_new_stats.email_campaign_title_id' )
            ->select(
                'email_campaign_title.user_id',
                'email_campaign_title.id',
                'email_campaign_title.email_category_id',
                'email_campaign_title.title',
                'email_campaign_title.created_at',
                'email_contents.subject_id',
                'email_broadcasts.boradcast_id as broadcast_id',
                'email_campaign_title.id as title_id'
            )
            ->where('autoresponder_new_stats.user_id', Auth::User()->id)
            ->whereColumn('email_broadcasts.boradcast_id', 'autoresponder_new_stats.email_brodcast_id' )
            ->where('autoresponder_new_stats.type', '!=', '' )
            //open
            ->where(['autoresponder_new_stats.stat_type'=> 'open'])
            ->where('autoresponder_new_stats.email', '!=', '')
            ->count();

        $total_clicked =  DB::table('email_campaign_title')
            ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
            ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
            ->join('autoresponder_new_stats', 'email_campaign_title.id', '=', 'autoresponder_new_stats.email_campaign_title_id' )
            ->select(
                'email_campaign_title.user_id',
                'email_campaign_title.id',
                'email_campaign_title.email_category_id',
                'email_campaign_title.title',
                'email_campaign_title.created_at',
                'email_contents.subject_id',
                'email_broadcasts.boradcast_id as broadcast_id',
                'email_campaign_title.id as title_id'
            )
           ->where('autoresponder_new_stats.user_id', Auth::User()->id)
            //->whereColumn('email_broadcasts.boradcast_id', 'autoresponder_new_stats.email_brodcast_id' )
           // ->where('autoresponder_new_stats.type', '!=', '' )
        //open
        ->where(['autoresponder_new_stats.stat_type'=> 'click'])
        //->where('autoresponder_new_stats.email', '!=', '')
        ->count();


        /*Config::set('mailchimp.apikey', Settings::where('user_id', Auth::User()->id)->value('mailchimp_key'));
        echo Config::get('mailchimp.apikey');*/


        if (Auth::user()->status == 0)
        {
            Auth::logout();
            //return redirect('/login')->with('error','Your status is block.');
            $error = array('error'=>'Your status is Blocked.');
            return redirect('/login')->withInput()->with($error);
        }
        //  combine counts
        $user_count     = User::all()->count();
        $email_count    = EmailTemplate::all()->count();
        $subj_count     = SubjectTemplate::all()->count();


        // your  templates
        $your_templates = DB::table('email_broadcasts')//
            ->where('user_id', Auth::User()->id)
            ->where('boradcast_id', '!=', '' )
            ->get()
            ->count('id');

        // sent
        $user_total_sent = DB::table('email_subjects')//
        ->join('email_broadcasts', 'email_subjects.id', '=', 'email_broadcasts.subject_id')
            ->where('email_subjects.user_id', Auth::User()->id)
            ->sum('email_broadcasts.sent');

        // un-subscribed
        $user_total_unsubscribed = DB::table('email_subjects')//
        ->join('email_broadcasts', 'email_subjects.id', '=', 'email_broadcasts.subject_id')
            ->where('email_subjects.user_id', Auth::User()->id)
            ->sum('email_broadcasts.unsubscribed');
        // chart
        $start_date = Carbon::now()->subDays(29);
        $end_date = Carbon::now();

        $broadcast_chart = $this->dateWiseStats($start_date, $end_date, $type='');
        $broadcast_type = $this->dateWiseStats($start_date, $end_date, $type='get_response');

        // Latest Campaigns
        $latest_campaigns = DB::table('email_subjects')//
            ->join('email_broadcasts', 'email_subjects.id', '=', 'email_broadcasts.subject_id')
            ->select('email_subjects.id',
                'email_subjects.subject',
                'email_broadcasts.opened',
                'email_broadcasts.clicked'
            )
            ->where('email_subjects.user_id', Auth::User()->id)
            ->orderBy('email_subjects.id', 'desc')
            ->limit(10)
            ->get();

        /* Start Top subject */
        $now                = Carbon::now();
        $today              = Carbon::today();
        $firstDay_PreMonth  = Carbon::today()->subMonth(1)->firstOfMonth();
        $lastDay_PreMonth   = Carbon::today()->subMonth(1)->lastOfMonth();
        $firstDay_thisMonth = Carbon::today()->firstOfMonth();
        $date_before7Days   = Carbon::today()->subDays(7);

        $topSubjects_today  = SubjectTemplate::select('id', 'subject', 'use_counter', 'updated_at')
                                    ->where('updated_at', '>',$today)
                                    ->where('use_counter','>', 0)
                                    ->orderBy('updated_at', 'desc')
                                    ->limit(20)->get();

        $topSubjects_last7Days  = SubjectTemplate::select('id', 'subject', 'use_counter', 'updated_at')
                                    ->whereBetween('updated_at', [$date_before7Days, $today])
                                    ->where('use_counter','>', 0)
                                    ->orderBy('updated_at', 'desc')
                                    ->limit(20)->get();

        $topSubjects_lastMonth  = SubjectTemplate::select('id', 'subject', 'use_counter', 'updated_at')
                                    ->whereBetween('updated_at', [$firstDay_PreMonth, $lastDay_PreMonth])
                                    ->where('use_counter','>', 0)
                                    ->orderBy('updated_at', 'desc')
                                    ->limit(20)->get();

        /* End Top subject */

        // fav open time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as open_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'open')
            ->where('email', '!=','')
            ->where('subject_id', '!=','')
            ->groupBy('email')
            ->get();
        $fav = array();
        foreach($fav_raw as $value){
            $fav[] =  $value->open_time;
        }

        if (count($fav) > 0)
        {
            $c = array_count_values($fav);
            $fav_open_time = array_search(max($c), $c);
        }else{
            $fav_open_time = '0:0';
        }

        // fav click time
        $fav_raw = DB::table('autoresponder_new_stats')
            ->select(DB::raw(" date_format(date_time, '%h %p') as click_time"))
            ->where('date_time', '!=','')
            ->where('user_id', '=', Auth::user()->id)
            ->where('stat_type', '=', 'click')
            ->get();
        $fav = array();
        foreach($fav_raw as $value){
            $fav[] =  $value->click_time;
        }

        if (count($fav) > 0)
        {
            $c = array_count_values($fav);
            $fav_click_time = array_search(max($c), $c);
        }else{
            $fav_click_time = '0:0';
        }

        // #############################    AUTORESPONDER STATS END #######################//

        $total_campaign = EmailBroadcast::where(['user_id' => Auth::user()->id])->count('id');

        return view('home',
            [   'user_total_sent'   =>  $user_total_sent,   'your_templates'    =>  $your_templates,
                'email_count'       =>  $email_count,       'subject_count'     =>  $subj_count,
                'user_count'        =>  $user_count,        'broadcast_chart'         =>  $broadcast_chart,
                'broadcast_type'    =>  $broadcast_type,    'start_date'        =>  $start_date,
                'end_date'          =>  $end_date,          'latest_campaigns'  =>  $latest_campaigns,
                'total_opened'      =>  $total_opened,      'total_clicked'     =>  $total_clicked,

                'user_total_unsubscribed'   => $user_total_unsubscribed,
                'topSubjects_today'         => $topSubjects_today,
                'topSubjects_last7Days'     => $topSubjects_last7Days,
                'topSubjects_lastMonth'     => $topSubjects_lastMonth
            ], compact(
                'total_campaign',  'fav_click_time','fav_open_time', 'sent','que','sent_error'
            )
        );
    }

    // broadcast charts
    function broadcastChart(Request $request){

        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $type = $request->type;

        $result = $this->dateWiseStats($start_date, $end_date, $type);
        echo $result;
    }

    function dateWiseStats($start_date, $end_date, $type)
    {

        //{"dated":"2017-08-11","clicked":0,"opened":0}
        $data = DB::table('autoresponder_new_stats');

          $data = $data->select(DB::raw('DATE_FORMAT(date_time, "%Y-%m-%d") as dated, date_time'));
          $data = $data->where('user_id', '=', Auth::user()->id);
          $data = $data->groupBy(DB::raw(' DATE_FORMAT( date_time, "%M %d %Y" ) '));

        if ($type!='')
        {
            $data = $data->where('type', $type);
        }

        if ($start_date == $end_date)
        {
            $data = $data->whereRaw(' date_time LIKE "'.$start_date.'%"');
        }else{
        $data = $data->whereBetween('date_time', array($start_date, $end_date ));
        }
        $data = $data->get();

        $result = array();
        foreach( $data as $value){


            $open_click =  DB::table('email_campaign_title')
                ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
                ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
                ->join('autoresponder_new_stats', 'email_campaign_title.id', '=', 'autoresponder_new_stats.email_campaign_title_id' )
                ->select(
                    'email_campaign_title.user_id',
                    'email_campaign_title.id',
                    'email_campaign_title.email_category_id',
                    'email_campaign_title.title',
                    'email_campaign_title.created_at',
                    'email_contents.subject_id',
                    'email_broadcasts.boradcast_id as broadcast_id',
                    'email_campaign_title.id as title_id'
                )
                ->where('email_contents.user_id', Auth::User()->id)
                ->where('autoresponder_new_stats.date_time', 'LIKE', "%$value->dated%")
                ->where('autoresponder_new_stats.subject_id','!=', '');

            //open
            $open = $open_click->where([

                ['autoresponder_new_stats.stat_type', '=',  'open'],
                ['autoresponder_new_stats.email', '!=',  '']

                ]
            );
            $total_opened = $open->count();

            $click = $open_click->where(['autoresponder_new_stats.stat_type'=> 'click']);
            $total_clicked = $click->count();

            array_push($result , ['dated' => $value->dated, 'opened' => $total_opened, 'clicked' => $total_clicked]);
        }
        return  json_encode($result);
    }


    public function linkTracker(){
        return view('link_tracker');
    }

    public function linkTrackerSettings(){

        $check  = DB::table('cpanels')->where('user_id', Auth::user()->id)->value('id');

        if ($check)
        {
            return redirect()->action('HomeController@index');
        }else {

            return view('link_tracker_settings');
        }
    }

    // post curl function
    function post_curl($url,$body,$method,$headers)
    {
        //  if (is_array($body)
        //)
        //$body=http_build_query($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
        //curl_setopt($ch, CURLOPT_NOBODY, true);    // we don't need body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        if ($method == "post"
        )
            curl_setopt($ch, CURLOPT_POST, true );
        if ($method == "get"
        )
            curl_setopt($ch, CURLOPT_HTTPGET, true );
        if ($method=="put"
        )
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if ($method=="delete"
        )
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        if ($body!=""
        )
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $filecontents=curl_exec($ch);

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //$header = substr($httpcode,0,$filecontents);
        //$body = substr($filecontents,$httpcode)
        curl_close($ch);
        return $filecontents;
    }


    public function linkTrackerSettingsStore(Request $request){


        $token = 'wBBl4cWzof0FXSBusGuuWnvQzbzzUtac';
        //$account_id =557;adnan
        $account_id =73887;
        //$contact_id =1197;adnan
        $contact_id =35237;
        $domain_name = $request->domains;
        //Set special headers
        $headers = [
            "Authorization: Bearer $token",
            "Accept: application/json",
            "Content-Type: application/json",
        ];
        // check if domain is available or not

        if ($request->action == 'check')
        {


            $url = 'https://api.dnsimple.com/v2/'.$account_id.'/registrar/domains/'.$domain_name.'/check';
            $method = 'get';

            $response = $this->post_curl($url,'',$method,$headers);
            $response = json_decode($response);

            if (isset($response->message))
            {
                $response = array('msg' => 3, 'err' => $response->message );
                echo json_encode($response);
            }else

            if ($response->data->available != "" )
            {
                $response = array('msg' => 1, 'domain' => $response->data->domain );

                echo json_encode($response);
            }else{
                $response = array('msg' => 2, 'domain' => $response->data->domain );
                echo json_encode($response);
            }
            exit;

            die();

        }else if ($request->action == 'register')
        {


//        $pass =  Auth::user()->password.'*123@_kl';
//        $cpanel_arr = array(
//            'user_id' => Auth::user()->id,
//            'domain'  => $request->domains,
//            'cpanel_user'    => 'shop'.Auth::user()->id,
//            'cpanel_pass'    => $pass,
//            'db_name' => 'shop'.Auth::user()->id.'_cmax',
//            'db_user' => 'shop'.Auth::user()->id.'_cmax',
//            'db_pass' => $pass
//        );
//
//        DB::table('cpanels')->insert($cpanel_arr);
//        $cpanel_arr['msg'] = 2;
//        $cpanel_arr['email'] = Auth::user()->email;
//        $cpanel_arr['cpanel_pass'] = $pass;
//
//        echo json_encode($cpanel_arr);
//        //End creating cpanel account in whm
//
//
//
//        exit;
//
//
//        die();


        $pass =  Auth::user()->password.'*123@_kl';
        //Set special headers
        $headers = [
            "Authorization: Bearer $token",
            "Accept: application/json",
            "Content-Type: application/json",
        ];
        //Sandbox api url you can remove sandbox for live account
        $url = 'https://api.dnsimple.com/v2/'.$account_id.'/registrar/domains/'.$domain_name.'/registrations';
        //im going to register domain against this contact id : 1196 . You can create in your account contact whome you want to assign this domain its required<br /><br /><br />
        //Also if there is an other .com TLD  then you have to set additional extended_attributes		"Required for TLDs that require extended attributes." you can read it here https://developer.dnsimple.com/v2/registrar/#register
        $method = 'post';
        $body = '{
         "registrant_id": '.$contact_id.'
        }';
        $response = $this->post_curl($url,$body,$method,$headers);
        $response = json_decode($response);

       if (isset( $response->message ) )
       {
           $response = array('msg' => $response->message);

           echo json_encode($response);

       }else if (isset( $response->data ))
       {
           // success
           //Access Token you can find it in your account
           //Set special headers
           $headers = [
               "Authorization: Bearer $token",
               "Accept: application/json",
               "Content-Type: application/json",
           ];

           //Sandbox api url you can remove sandbox for live account
           $url = 'https://api.dnsimple.com/v2/' . $account_id . '/registrar/domains/' . $domain_name . '/delegation';

           //Im going to update this domain azharrana.com
           $method = 'put';
           //assign dns
           $body = '[
                      "ns305.websitewelcome.com",
                      "ns306.websitewelcome.com"
                    ]';//json_encode($body);
           $response = $this->post_curl($url, $body, $method, $headers);

           //Start creating cpanel account in whm
           $cpanel_arr = array(
               'user_id' => Auth::user()->id,
               'domain' => $request->domains,
               'cpanel_user' => 'shop' . Auth::user()->id,
               'cpanel_pass' => $pass,
               'db_name' => 'shop' . Auth::user()->id . '_cmax',
               'db_user' => 'shop' . Auth::user()->id . '_cmax',
               'db_pass' => $pass
           );
           DB::table('cpanels')->insert($cpanel_arr);
           $cpanel_arr['msg'] = 2;
           $cpanel_arr['email'] = Auth::user()->email;
           $cpanel_arr['cpanel_pass'] = $pass;

           echo json_encode($cpanel_arr);
           //End creating cpanel account in whm
       }

       }

    }

    public function callBack(Request $request){

    }
}
