<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'plain_password' => $data['password'],
        ]);

        $from = env('MAIL_FROM_ADDRESS');

        $headersfrom='';
        $headersfrom .= 'MIME-Version: 1.0' . "\r\n";
        $headersfrom .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headersfrom .= 'From: '.$from.' '. "\r\n";

        $subject = 'Email UpShot Registration Notification';
        $to = $data['email'];

        $body = '<p>Dear '.$data['name'].'</p>

                <p>Thank you for registering at Email UpShot.</p>

                <p>Your Registration Informations are as:<p>
                <p>Name: '.$data['name'].'</p>
                <p>Email: '.$data['email'].'</p>
                <p>Password: '.$data['password'].'<p>
                <p>Application URL is '.asset('').'</p>';

        $mail = mail($to, $subject, $body, $headersfrom);

        return $user;


    }


    //comment this function if want to user register routes
//    public function showRegistrationForm(){
//        return redirect('login');
//    }
}
