<?php

namespace App\Http\Controllers\Admin;

use App\Aweber_Setting;
use App\EmailBroadcast;
use App\EmailTemplate;
use App\GetResponse_Setting;
use App\Activecampaign_Setting;
use App\SendLane_Setting;
use App\MailChimp_Setting;
use App\Settings;
use App\SubjectTemplate;
use App\UsedAutoResponder;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Categories;
use DB;
use Auth;

use Illuminate\Support\Facades\Config;
use App\Exceptions;
use App\EmailHeaderFooter;
//use PHPMailer;
use Mailchimp\Mailchimp;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use \Mailjet\Resources;

class NewEmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mc = new MailChimpController();
        $mc->updateStats();
        return view("admin.new_email_template.list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unopen = '';
        $email_sender = '';

        if (isset($_REQUEST['unopen']) || isset($_REQUEST['open']) )
        {

            if (isset($_REQUEST['unopen']))
            {
                $title_id = $_REQUEST['unopen'];
                $send_type = 'unopen';
            }
            if (isset($_REQUEST['open']))
            {
                $title_id = $_REQUEST['open'];
                $send_type = 'open';
            }

            $data = DB::table('email_campaign_title')
                ->join('email_broadcasts', 'email_campaign_title.id', 'email_broadcasts.email_campaign_title_id')
                ->select(
                    'email_campaign_title.title',
                    'email_broadcasts.email_category_id',
                    'email_broadcasts.email_campaign_title_id',
                    'email_broadcasts.boradcast_id'
                )
                ->where(['email_broadcasts.user_id' => Auth::user()->id])
                ->where('email_broadcasts.subject_id', '!=', "")
                ->where('email_broadcasts.email_campaign_title_id', $title_id)
                //->groupBy('autoresponder_new_stats.email')
                ->first();


            $title = DB::table('email_campaign_title')->where('id', $title_id)->value('title');
            $unopen = [
                'title_id' => $title_id,
                'title' => $data->title,
                'category' => $data->email_category_id,
                'broadcast_id' => $data->boradcast_id,
                'type' => $send_type
            ];
            //$cats = Categories::where([ 'user_id' => Auth::user()->id, 'id' => $category ])->get();
        }

        $contact_lists =  DB::table('autoresponder_lists')
            ->select('id', 'list_name', 'type')
            ->where(['user_id' => Auth::user()->id])
            ->get();

        $email_sender = DB::table('email_sender')->where('user_id', Auth::user()->id )->get();
        $cats = Categories::all()->where('user_id', Auth::user()->id);
        // email header footer
        $header_footer = EmailHeaderFooter::where('user_id', Auth::user()->id)->select('header', 'footer')->first();
        $data = DB::table('email_subjects')->select('sub_category_id', 'id')->where('sub_category_id', '!=', '')->where('user_id', Auth::user()->id)->groupBy('sub_category_id')->get();

        $count_cats = array();
        foreach($data as $value)
        {
            $category = DB::table('email_sub_category')->where('id', $value->sub_category_id)->select('category', 'id')->first();
            $total = DB::table('email_subjects')->where('sub_category_id', $value->sub_category_id)->count();
            if (isset($category->category))
                array_push($count_cats, ['id' => $category->id, 'category' => $category->category, 'total' => $total]);
        }
        // autoresponders

        $autorsponders = array();

        $aweber_acccount = Aweber_Setting::where('user_id', Auth::user()->id)->first();
        if ($aweber_acccount)
        {
            array_push($autorsponders, ['value' =>'aweber', 'name' => 'Aweber']);
        }

        $getresponse_acccount = GetResponse_Setting::where('user_id', Auth::user()->id)->first();
        if ($getresponse_acccount)
        {
            array_push($autorsponders, [ 'value' => 'get_response', 'name' => 'Get Response']);
        }

        $mailchimp_acccount = MailChimp_Setting::where('user_id', Auth::user()->id)->first();
        if ($mailchimp_acccount)
        {
            array_push($autorsponders, [ 'value' =>  'mailchimp', 'name' => 'MailChimp']);
        }

        $activecamp_acccount = Activecampaign_Setting::where('user_id', Auth::user()->id)->first();
        if ($activecamp_acccount)
        {
            array_push($autorsponders, ['value' => 'activecampaign', 'name' => 'Active Campaign']);
        }

        return view("admin.new_email_template.add",
            [
                'autorsponders' => $autorsponders,
                'email_sender' => $email_sender ,
                'unopenemails' => $unopen,
                'cats'=>$cats,
                'count_cats' => $count_cats,
                'header_footer' => $header_footer,
                'contact_lists' => $contact_lists
            ]);
    }

    public function searchSubject(Request $request)
    {
        /*
         * new query to show subject only
         * */
        $data = DB::table('email_subjects');
        $data = $data->join('email_contents', 'email_subjects.id', '=', 'email_contents.subject_id');
        $data = $data->select(
            'email_subjects.email_category_id',
            'email_subjects.sub_category_id',
            'email_subjects.subject',
            'email_subjects.id as subject_id',
            'email_subjects.status',
            'email_contents.email_category_id',
            'email_contents.id as contents_id'
        );
        //$data = $data->where('email_subjects.subject', 'LIKE', "%$request->subject%");
        $data = $data->where('email_subjects.status', '=', 1);
        $data = $data->where('email_contents.is_private', 0);
        // if noting is assigned to user
        if (Auth::user()->email_category == 0 && Auth::user()->email_shared_id == null )
        {
            $data = $data->where('email_contents.user_id', Auth::user()->id);
        }
        // if both value are assigned
        if (Auth::user()->email_category != 0 && Auth::user()->email_shared_id != null )
        {
            $data = $data->where(function ($query)
            {
                $query->whereIn('email_subjects.email_category_id', explode(',', Auth::User()->email_category))
                    ->orWhere( DB::raw( 'email_subjects.sub_category_id IN ( '.Auth::User()->email_shared_id.'  )'))
                    ->orWhere('email_contents.user_id', Auth::user()->id);
            });
        }
        // if email cat is assigned and other is empty
        if (Auth::user()->email_category != 0  && Auth::user()->email_shared_id == null)
        {
            $data = $data->where(function ($query)
            {
                $query->whereIn('email_subjects.email_category_id', explode(',', Auth::User()->email_category))
                    ->orWhere('email_contents.user_id', Auth::user()->id);
            });
        }
        // email cat is null and email shared is assigned
        if (Auth::user()->email_category == 0 && Auth::user()->email_shared_id != null )
        {
            $data = $data->where(function ($query)
            {
                $query->whereIn('email_subjects.sub_category_id', explode(',', Auth::User()->email_shared_id))
                    ->orWhere('email_contents.user_id', Auth::user()->id);
            });
        }


        $rawData = $data->get();
        $data = array();
        foreach( $rawData as $value )
        {
            if ( $value->sub_category_id == "" )
            {
                $category = DB::table('email_category')->where('id', $value->email_category_id )->value('category');
            }else{
                $category = DB::table('email_sub_category')->where('id', $value->sub_category_id )->value('category');
            }

            array_push($data,
                [
                    'email_category_id' => $value->sub_category_id,
                    'subject' => $value->subject,
                    'subject_id' => $value->subject_id,
                    'status' => $value->status,
                    'contents_id' => $value->contents_id,
                    'category' => $category
                ]
            );
        }

        $index = 0;
        //print_r($data);

        return Datatables::of($data)
            ->addColumn('subject', function ($data)
            {

                global $index;
                $index++;
                $subject = str_replace( ' ', "", $data['subject'] );
                $subject = str_replace( 'ï»¿', "", $data['subject'] );
                $subject = str_replace( 'â€™', "'", $data['subject'] );
                $subject = str_replace( '“', '"', $data['subject'] );
                $subject = str_replace( '�?', '"', $data['subject'] );
                $subject = str_replace( '—', '-', $data['subject'] );
                $subject = str_replace( '…', '', $data['subject'] );
                $html = '
                    <div class="radio">
                        <label for="sub_'.$index.'">
                            <input type="radio" id="sub_'.$index.'" name="sub" aria-label="Single radio One" data-content="cont_'.$data['contents_id'].'" data-index="'.$index.'"   onclick="load_sub(this)">
                              
                         <b id="s_'.$data['subject_id'].'" class="cls_'.$index.'">'. $subject .'</b>
                         
                        </label>
                    </div>
               
                    
                ';

                return $html;
            })
            ->rawColumns(['subject'])
            ->make(true);
    }

    public function loadEmailContentByID(Request $resuest)
    {
        $email_content = EmailTemplate::where('subject_id', '=', $resuest->id)->first();
        return $email_content->content;
    }

    //
    function viewEmailContent(Request $request)
    {

        if ($request->id !="")
        {
            $data = EmailTemplate::where('subject_id', '=', $request->id)->value('content');

            if ($data)
            {
                $data =  $data;
            }else{
                $data =  'Data Not Found!';
            }
            $data = str_replace(' ', ' ', $data);
            echo $data;
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //date_default_timezone_set(Auth::user()->time_zone);

            $minute = $request->minute;
            $hour = $request->hour;

            $meridian = $request->meridian;
            $date = $request->active_date;

            if ($date != "")
            {
                $exp = explode('/', $date);
                $year = $exp[2];
                $month = $exp[0];
                $day = $exp[1];
                $start_time = Carbon::create($year, $month, $day, $hour, $minute)->addMinutes(10);
            } else {
                $start_time = Carbon::now();
            }
            $now = Carbon::now();

            $title_id = '';
            $email_campaign_title = $request->email_campaign_title;
            $email_category_id = $request->email_category;

            $broadcast_id = $request->broadcast_id;
            $Broadcast_id = time();
            $type = $request->type;
            $title_id = $request->resend_emails_id;

            $subject = $request->subject;
            $subject = trim($subject);
            $subject = rtrim($subject);

            $err = '';

            if ($subject == "")
            {
                $err .= 'Subject Required!';
            }

            $email_resend = false;
            if ($request->send_type != "")
            {
                $email_resend = true;

                if ($request->email_sender == "")
                {
                    $err .= ' Email sender required!, ';
                }
                if ($email_category_id == "")
                {
                    $err .= 'Category is required!,<br>';
                }
                if ($request->send_type == 'smtp_broad' && !isset($request->contact_lists))
                {
                    $err .= ' Select List is required!, ';
                }
            }

            if ($err != '')
            {
                echo json_encode(array('date_err' => 1, 'msg' => $err));
                exit;
            }

            $subject_id = SubjectTemplate::where('subject', '=', $subject)->value('id');

            if (!$subject_id)
            {
                $subject_id = DB::table('email_subjects')->insertGetId(
                    ['user_id' => Auth::User()->id, 'subject' => $subject, 'status' => 1, 'email_category_id' => 3]
                );
            }

            // grouping data
            $group_id = DB::table('multilist_group')->insertGetId(
                ['user_id' => Auth::User()->id, 'subject_id' => $subject_id]
            );

            // ############### email sender ###############
            // ###########################################

            if ($email_resend == true)
            {
                $email_campaign_title_id = DB::table('email_campaign_title')->insertGetId(
                    ['user_id' => Auth::User()->id, 'title' => $email_campaign_title, 'email_category_id' => $email_category_id, 'parent_id' => $title_id]
                );

                $obj = new EmailTemplate();
                $updating = 0;
                if ($request->id != '')
                {
                    $updating = 1;
                    $obj = $obj->findOrFail($request->id);
                }
                $obj->fill($request->all());

                $obj->category_id = $request->category_id;
                $obj->subject_id = $subject_id;
                $obj->content = $request->contents;
                $obj->email_category_id = $email_category_id;
                $obj->email_campaign_title_id = $email_campaign_title_id;
                $obj->user_id = Auth::User()->id;

                if ($obj->save())
                {

                    $stat_id = DB::table('autoresponder_new_stats')->insertGetId(
                        ['type' => '', 'user_id' => Auth::User()->id, 'email_campaign_title_id' => $email_campaign_title_id, 'email' => 'fake_email@gmail.com']
                    );

                    $email_sender = DB::table('email_sender')
                        ->where(['id' => $request->email_sender, 'user_id' => Auth::user()->id])
                        ->first();

                    $from_name = $email_sender->from_name;
                    $from_email = $email_sender->from_email;
                    $api_key = $email_sender->api_key;
                    $domain = $email_sender->domain;

                    // broadcast
                    // insert empty data to email broadcast
                    $emailBroadCast = new EmailBroadcast();
                    $emailBroadCast->json = '';
                    $emailBroadCast->group_id = $group_id;
                    $emailBroadCast->type = 'smtp';
                    $emailBroadCast->user_id = Auth::User()->id;
                    $emailBroadCast->subject_id = $subject_id;
                    $emailBroadCast->email_category_id = $email_category_id;
                    $emailBroadCast->email_campaign_title_id = $email_campaign_title_id;
                    $emailBroadCast->boradcast_id = $Broadcast_id;
                    $emailBroadCast->status = 0;

                    $emailBroadCast->save();

                    /*
                     *  SMTP SEND
                     */
//echo 1;
                    if ($request->send_type == 'smtp_broad' && isset($request->contact_lists))
                    {
                        //echo 2;
                        $body = $request->contents;
                        $json = array(
                            'list_id' => $request->contact_lists,
                            'title' => $request->email_campaign_title,
                            'subject_line' => $request->subject,
                            'html' => $body,
                            'group_id' => $group_id
                        );

                        $json = json_encode($json);

                        $insert = DB::table('autoresponder_schedule')
                            ->insertGetId([
                                'subject' => $subject,
                                'email_campaign_title_id' => $email_campaign_title_id,
                                'subject_id' => $subject_id,
                                'email_category_id' => $email_category_id,
                                'user_id' => Auth::user()->id,
                                'email_brodcast_id' => $Broadcast_id,
                                'type' => 'smtp_send',
                                'campaign_id' => '',
                                'click_link_id' => '',
                                'status' => 1,
                                'date_time' => '',
                                'json' => $json,
                                'api_key' => $request->email_sender
                            ]);

                        if ($insert)
                        {
                            echo json_encode(array('id' => $subject_id, 'msg' => 'Email has been queued.'));
                        }
                        exit;
                    }
                    $error = '';
                    $type = 'resend';
                    $groups_id = DB::table('email_broadcasts')->where('email_campaign_title_id', $title_id)->value('group_id');
                    $title_ids = DB::table('email_broadcasts')->select('email_campaign_title_id')->where('group_id', $groups_id)->get()->toArray();


                    $id = array();
                    foreach ($title_ids as $ids)
                    {
                        array_push($id, $ids->email_campaign_title_id);
                    }

                    $resend_emails = DB::table('campaign_send_list')
                        ->join('autoresponder_new_stats', 'campaign_send_list.campaign_title_id', 'autoresponder_new_stats.email_campaign_title_id')
                        ->select(
                            'autoresponder_new_stats.email',
                            'autoresponder_new_stats.email_category_id',
                            'autoresponder_new_stats.email_campaign_title_id',
                            'campaign_send_list.type',
                            'campaign_send_list.list_id',
                            'campaign_send_list.type as autores_type',
                            'campaign_send_list.account_id',
                            'autoresponder_new_stats.email_category_id',
                            'autoresponder_new_stats.email_brodcast_id'
                        )
                        ->where(['autoresponder_new_stats.user_id' => Auth::user()->id])
                        ->where('autoresponder_new_stats.email', '!=', '')
                        ->where('autoresponder_new_stats.subject_id', '!=', "")
                        ->whereIn('autoresponder_new_stats.email_campaign_title_id', $id)
                        //->groupBy('autoresponder_new_stats.email')
                        ->groupBy('campaign_send_list.type')
                        ->get();

                    $unOpenEmails = $list_id = $email = $auto_type = $account_id = array();
                    $category = $broadcast_id = '';

                    $count = 0;
                    foreach ($resend_emails as $value)
                    {
                        if ($count == 0)
                        {
                            $category = $value->email_category_id;
                            $broadcast_id = $value->email_brodcast_id;
                        }
                        array_push($list_id, $value->list_id);
                        array_push($email, $value->email);
                        array_push($auto_type, $value->autores_type);
                        array_push($account_id, $value->account_id);
                        $count++;
                    }

                    $list_id = array_unique($list_id);
                    $email = array_unique($email);
                    $auto_type = array_unique($auto_type);
                    $account_id = array_unique($account_id);


                    $raw_email = DB::table('autoresponder_contact');
                    $raw_email = $raw_email->select('email', 'account_id');
                    $raw_email = $raw_email->whereIn('list_id', $list_id);
                    $raw_email = $raw_email->whereIn('account_id', $account_id);
                    $raw_email = $raw_email->whereIn('type', $auto_type);
                    if ($request->send_type == 'unopen')
                    {
                        $raw_email = $raw_email->whereNotIn('email', $email);
                    } else if ($request->send_type == 'open')
                    {
                        $raw_email = $raw_email->whereIn('email', $email);
                    }
                    $raw_email = $raw_email->where(['user_id' => Auth::user()->id]);
                    $raw_email = $raw_email->groupby('email');
                    $raw_email = $raw_email->get();

                    // insert data to list table

                    $ins = DB::table('campaign_send_list')
                        ->insert(
                            ['group_id' => $group_id,
                                'account_id' => $raw_email[0]->account_id,
                                'campaign_title_id' => $email_campaign_title_id,
                                'list_id' => $resend_emails[0]->list_id,
                                'type' => $resend_emails[0]->type
                            ]
                        );


                    if ($email_sender->type == 'mailingreen')
                    {

                        foreach ($raw_email as $value)
                        {
                            $to = $value->email;
                            $body = $request->contents;
                            $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];
                            // update click link tracker
                            if (count($link_tracking_id) > 0)
                            {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }
                            $str = trim(preg_replace('/\s+/', " ", $str));
                            $body = preg_replace("/\"/", "\\\"", $str);

                            $mailin = new Mailin("https://api.sendinblue.com/v2.0", $api_key);
                            $data = array("to" => array($to => $to_name),
                                "from" => array($from_email, $from_name),
                                "subject" => $subject,
                                "html" => $body
                            );

                            $result = $mailin->send_email($data);
                            if ($result['code'] == 'failure')
                            {
                                $error = $result['message'];
                                exit;
                            }

                            if ($result['code'] == 'success')
                            {
                                if (strpos($result['message'], 'has not been activated') !== false)
                                {
                                    $error = '';
                                }
                            }
                        }

                        if ($error != "")
                        {
                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            exit;
                        } else {
                            echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                            exit;
                        }


                    }//end mailingreen

                    if ($email_sender->type == 'mailjet')
                    {
                        // include library
                        require 'vendor/autoload.php';

                        $private_api_key = $email_sender->private_api_key;

                        foreach ($raw_email as $value)
                        {
                            $to = $value->email;
                            $body = $request->contents;
                            $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];
                            // update click link tracker
                            if (count($link_tracking_id) > 0)
                            {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            $str = trim(preg_replace('/\s+/', " ", $str));
                            $body = preg_replace("/\"/", "\\\"", $str);

                            //$mj = new \Mailjet\Client(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),
                            $mj = new \Mailjet\Client($api_key, $private_api_key,
                                true, ['version' => 'v3.1']);
                            $data = [
                                'Messages' => [
                                    [
                                        'From' => [
                                            'Email' => $from_email,
                                            'Name' => $from_name
                                        ],
                                        'To' => [
                                            [
                                                'Email' => $to,
                                                'Name' => $to_name
                                            ]
                                        ],
                                        'Subject' => $subject,
                                        'TextPart' => "",
                                        'HTMLPart' => $body
                                    ]
                                ]
                            ];
                            $response = $mj->post(Resources::$Email, ['body' => $data]);
                            //$response->success();

                            if ($response->success())
                            {
                                $error = '';
                            } else {
                                //print_r($response->getStatus());
                                $data = $response->getData();
                                if (isset($data['ErrorMessage']))
                                {
                                    $error = $data['ErrorMessage'];

                                } else {
                                    if (isset($data['Messages'][0]['Errors'][0]['ErrorMessage']))
                                    {
                                        $error = $data['Messages'][0]['Errors'][0]['ErrorMessage'];
                                    }
                                }
                            }

                        }// end foreach

                        if ($error != "")
                        {
                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            exit;
                        } else {
                            echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                            exit;
                        }


                    }// end mailjet if

                    if ($email_sender->type == 'smtp')
                    {

                        require 'vendor/autoload.php';
                        // creating variables
                        $host = $email_sender->host;
                        $port = $email_sender->port;
                        $user = $email_sender->user;
                        $password = $email_sender->password;
                        $ssl = $email_sender->smtp_ssl;

                        foreach ($raw_email as $value)
                        {

                            $to = $value->email;
                            $body = $request->contents;
                            $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                            $to_name = explode('@', $to);
                            $to_name = $to_name[0];

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];
                            // update click link tracker
                            if (count($link_tracking_id) > 0)
                            {
                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => $type,
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadcast_id,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            $str = trim(preg_replace('/\s+/', " ", $str));
                            $body = preg_replace("/\"/", "\\\"", $str);

                            try {
                                $mail = new PHPMailer();
                                //Enable SMTP debugging.
                                $mail->SMTPDebug = false;
                                //Set PHPMailer to use SMTP.
                                $mail->isSMTP();
                                //Set SMTP host name
                                $mail->Host = $host;
                                //Set this to true if SMTP host requires authentication to send email
                                $mail->SMTPAuth = true;
                                //Provide username and password
                                $mail->Username = $user;
                                $mail->Password = $password;
                                //If SMTP requires TLS encryption then set it
                                if ($ssl != 1)
                                {
                                    $mail->SMTPSecure = "tls";
                                } else {
                                    $mail->SMTPSecure = "ssl";
                                }
                                //Set TCP port to connect to
                                $mail->Port = $port;
                                // $mail->test = '25';
                                $mail->SMTPOptions = array('ssl' => array('verify_host' => false, 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));

                                $mail->From = $from_email;
                                $mail->FromName = $from_name;

                                $mail->addAddress($to, $to_name);

                                $mail->isHTML(true);

                                $mail->Subject = $subject;
                                $mail->Body = $body;
                                $mail->AltBody = "";
                                $send = $mail->send();


                            } catch (\Exception $e)
                            {
                                echo $err = $e->getMessage();
                            }

                            if (!$send)
                            {
                                $err = explode('.', $mail->ErrorInfo);
                                $error = $err[0];
                                exit;
                            } else {
                                $error = '';
                            }
                        }

                        if ($error != "")
                        {
                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            exit;
                        } else {
                            echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                            exit;
                        }
                    } else {

                        if ($email_sender->type == 'sparkpost')
                        {
                            // sparkpost
                            foreach ($raw_email as $value)
                            {
                                $to = $value->email;
                                $body = $request->contents;
                                $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];
                                // update click link tracker
                                if (count($link_tracking_id) > 0)
                                {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }

                                $str = trim(preg_replace('/\s+/', " ", $str));
                                $b = preg_replace("/\"/", "\\\"", $str);
                                $uri = 'https://api.sparkpost.com/api/v1/transmissions';
                                $json = '{
        "options": {
            "open_tracking": true,
            "click_tracking": true
        },
        "metadata": {
            "some_useful_metadata": ""
        },
        "substitution_data": {
            "signature": ""
        },
        "recipients": [
            {
                "address": {
                    "email": "' . $to . '"
                },
                "tags": [
                    "learning"
                ],
                "substitution_data": {
                    "customer_type": "Platinum",
                    "first_name": "' . $to_name . '"
                }
            }
        ],
        "content": {
            "from": {
                "name": "' . $from_name . '",
                "email": "' . $from_email . '"
            },
            "subject": "' . $subject . '",
            "text": "",
            "html": "' . $b . '"
        }
    }';

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: $api_key"));
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                                $result = curl_exec($ch);
                                curl_close($ch);

                                $res = json_decode($result);

                                if (isset($res->errors))
                                {
                                    $err = $res->errors;
                                    $error = $err[0]->message;
                                    exit;
                                } else {
                                    $error = '';
                                }

                                // =================== END SPARKPOST ================= //

                            }
                            if ($error != "")
                            {
                                echo json_encode(['date_err' => 1, 'msg' => $error]);
                                exit;
                            } else {
                                echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                                exit;
                            }


                        } elseif ($email_sender->type == 'sendgrid')
                        {

                            foreach ($raw_email as $value)
                            {
                                $to = $value->email;

                                $body = $request->contents;
                                $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];
                                // update click link tracker
                                if (count($link_tracking_id) > 0)
                                {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }
                                $b = trim(preg_replace('/\s+/', " ", $str));
                                //$b = trim(preg_replace('/\s+/', " ", $body));
                                $b = preg_replace("/\"/", "\\\"", $b);
                                $uri = 'https://api.sendgrid.com/v3/mail/send';
                                $json = '{
                                            "personalizations": [
                                                    {
                                                        "to": [{
                                                                "email": "' . $to . '",
                                                                "name":"' . $to_name . '"
                                                        }]
                                                    }
                                                ],
                                                "from": {
                                                    "email": "' . $from_email . '",
                                                    "name":"' . $from_name . '"
                                                },
                                                "subject": "' . $subject . '",
                                                "content": [
                                                    {
                                                    "type": "text/html",
                                                    "value": "' . $b . '"
                                                }
                                            ]
                                        }';

                                $headers = array(
                                    'Authorization: Bearer ' . $api_key . '',
                                    'Content-Type: application/json'
                                );

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                                $result = curl_exec($ch);
                                curl_close($ch);

                                //$result = json_encode( array('df' => 1));
                                $res = json_decode($result);

                                if (isset($res->errors))
                                {
                                    $err = $res->errors;
                                    $error = $err[0]->message;

                                } else {
                                    $error = '';
                                }
                            }

                            if ($error != "")
                            {
                                echo json_encode(['date_err' => 1, 'msg' => $error]);
                                exit;
                            } else {
                                echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                                exit;
                            }

                        } elseif ($email_sender->type == 'mailgun')
                        {

                            define('MAILGUN_URL', 'https://api.mailgun.net/v3/' . $domain . '');
                            define('MAILGUN_KEY', $api_key);


                            foreach ($raw_email as $value)
                            {
                                $to = $value->email;
                                $body = $request->contents;
                                $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broadcast_id . "&subject_id=" . $subject_id . "&type=" . $type . "&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=" . $to . "&unopened=1' width='1' />";

                                $to_name = explode('@', $to);
                                $to_name = $to_name[0];

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($body . $tracking_img, $to);
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];
                                // update click link tracker
                                if (count($link_tracking_id) > 0)
                                {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => $type,
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadcast_id,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }


                                $str = trim(preg_replace('/\s+/', " ", $str));
                                //$b = preg_replace("/\"/", "\\\"", $str);


                                $toname = $to_name;
                                $mailfromname = $from_name;
                                $mailfrom = $from_email;
                                $subject = $subject;
                                $html = $str;
                                $text = '';
                                $tag = '';

                                $replyto = env('MAIL_FROM_ADDRESS');

                                $array_data = array(
                                    'from' => $mailfromname . '<' . $mailfrom . '>',
                                    'to' => $toname . '<' . $to . '>',
                                    'subject' => $subject,
                                    'html' => $html,
                                    'text' => $text,
                                    'o:tracking' => 'yes',
                                    'o:tracking-clicks' => 'yes',
                                    'o:tracking-opens' => 'yes',
                                    'o:tag' => $tag,
                                    'h:Reply-To' => $replyto
                                );
                                $session = curl_init(MAILGUN_URL . '/messages');
                                curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($session, CURLOPT_USERPWD, 'api:' . MAILGUN_KEY);
                                curl_setopt($session, CURLOPT_POST, true);
                                curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
                                curl_setopt($session, CURLOPT_HEADER, false);
                                curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
                                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
                                $response = curl_exec($session);
                                curl_close($session);
                                $results = json_decode($response, true);

                                if (!isset($results['id']))
                                {
                                    $error = $results['message'];
                                    exit;
                                } elseif ($results == "")
                                {
                                    $error = 'Invalid credentials!';
                                    exit;
                                } elseif (isset($results['id']))
                                {
                                    $error = '';
                                }
                            }
                        }

                        if ($error != "")
                        {
                            echo json_encode(['date_err' => 1, 'msg' => $error]);
                            exit;
                        } else {
                            echo json_encode(['id' => 1, 'msg' => 'Email resend successfully']);
                            exit;
                        }


                    }// end mailgun

                }
            } else {
                ##### broadcast script  #####

                //updating use counter
                SubjectTemplate::where('id', '=', $subject_id)->increment('use_counter');

                $broadCastId = '';
                $selectCamapgin = '';
                $replyto = null;
                $response = '';

                $str = preg_replace('/\s+/', " ", trim($request->content_copy));
                //$html = preg_replace("/\"/", "\\\"", $str);

                //send news letter by Get-Response

                if ($request->send_get_response == '1')
                { // multiselect list supported

                    $email_campaign_title_id = DB::table('email_campaign_title')->insertGetId(
                        ['user_id' => Auth::User()->id, 'title' => $email_campaign_title, 'email_category_id' => $email_category_id, 'parent_id' => $title_id]
                    );

                    $obj = new EmailTemplate();
                    $updating = 0;
                    if ($request->id != '')
                    {
                        $updating = 1;
                        $obj = $obj->findOrFail($request->id);
                    }
                    $obj->fill($request->all());
                    $obj->content = $request->contents;
                    $obj->category_id = $request->category_id;
                    $obj->subject_id = $subject_id;
                    $obj->email_category_id = $email_category_id;
                    $obj->email_campaign_title_id = $email_campaign_title_id;
                    $obj->user_id = Auth::User()->id;

                    if ($obj->save())
                    {

                        $stat_id = DB::table('autoresponder_new_stats')->insertGetId(
                            ['type' => 'get_response', 'user_id' => Auth::User()->id, 'email_campaign_title_id' => $email_campaign_title_id, 'email' => 'fake_email@gmail.com']
                        );

                        foreach ($request->api_key_type_get_response as $key => $value)
                        {

                            // insert empty data to email broadcast
                            $emailBroadCast = new EmailBroadcast();
                            $emailBroadCast->json = '';
                            $emailBroadCast->group_id = $group_id;
                            $emailBroadCast->type = '';
                            $emailBroadCast->user_id = Auth::User()->id;
                            $emailBroadCast->subject_id = 0;
                            $emailBroadCast->email_category_id = $email_category_id;
                            $emailBroadCast->email_campaign_title_id = $email_campaign_title_id;
                            if ($broadCastId != '')
                            {
                                $emailBroadCast->boradcast_id = '';
                            }
                            $emailBroadCast->save();
                            $broad_last_id = $emailBroadCast->id;

                            $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broad_last_id . "&subject_id=" . $subject_id . "&type=get_response&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=[[email]]' width='1' />";

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($request->contents, '[[email]]');
                            $link_tracking_id = $link_tracking['ids'];

                            $str = $link_tracking['email_html'];

                            $str = preg_replace('/\s+/', " ", trim($str . $tracking_img));
                            // %first_name%  %last_name%  %email_address%

                            $str = str_replace('%first_name%', '[[firstname]]', $str);
                            $str = str_replace('%last_name%', '[[lastname]]', $str);
                            $str = str_replace('%email_address%', '[[email]]', $str);
                            $str = str_replace('"', "'", $str);
                            // initializing arrays to variables

                            $reply_to = $request->replyto_email;
                            $news_title = $request->newsletter_title;
                            $compaign_id = $request->campaignId;
                            $from_field_id = $request->fromFieldId;
                            $api_key = $request->api_key_get_response_id;
                            $selectedCampaigns = $request->selectedCampaigns;
                            //

                            foreach ($selectedCampaigns[$api_key[$key]] as $list)
                            {
                                $ins = DB::table('campaign_send_list')
                                    ->insert(['group_id' => $group_id, 'account_id' => $api_key[$key], 'campaign_title_id' => $email_campaign_title_id, 'list_id' => $list, 'type' => 'get_response']);
                            }

                            $replyto = ($reply_to[$key] != '') ? $reply_to[$key] : 'null';

                            if (count($selectedCampaigns[$api_key[$key]]) > 0)
                            {
                                foreach ($selectedCampaigns[$api_key[$key]] as $k => $v)
                                {
                                    $selectCamapgin .= '"' . $v . '",';
                                }
                            }  //
                            $selectCamapgin = rtrim($selectCamapgin, ",");

                            $json = '{
                                "name": "' . $email_campaign_title . '",
                                "type": "broadcast",
                                "subject": "' . $request->subject . '",
                                "flags": [
                                    "openrate"
                                ],
                                "editor": "custom",
                                "campaign": {
                                    "campaignId": "' . $compaign_id[$key] . '"
                                },

                                "fromField": {
                                    "fromFieldId": "' . $from_field_id[$key] . '"
                                },
                                "replyTo": "' . $replyto . '",
                                "content": {
                                    "html": "' . $str . '"
                                },
                                "sendSettings": {
                                    "timeTravel": "false",
                                    "perfectTiming": "false",
                                    "selectedCampaigns": [
                                        ' . $selectCamapgin . '
                                    ]
                                }
                            }';


                            $get_response_key = GetResponse_Setting::where('id', '=', $api_key[$key])->value("get_response_key");
                            ///////////////
                            $url_newsletter_post = 'https://api.getresponse.com/v3/newsletters';


                            if ($start_time > $now)
                            {
                                // create crone
                                // broadcast_id, click_id, subject_id, responder_type, api_key,title_id, list_id

                                $insert = DB::table('autoresponder_schedule')
                                    ->insertGetId([
                                        'email_campaign_title_id' => $email_campaign_title_id,
                                        'subject_id' => $subject_id,
                                        'email_category_id' => $email_category_id,
                                        'user_id' => Auth::user()->id,
                                        'email_brodcast_id' => $broad_last_id,
                                        'type' => 'get_response',
                                        'campaign_id' => $selectCamapgin,
                                        'click_link_id' => json_encode($link_tracking_id),
                                        'status' => 1,
                                        'date_time' => date('Y-m-d H:i', strtotime($date . ' ' . $hour . ':' . $minute)),
                                        'json' => $json,
                                        'api_key' => $get_response_key,
                                    ]);

                            } else {

                                $response = $this->Curl($url_newsletter_post, $json, 'post', $get_response_key);
                                $res_decoded = json_decode($response);

                                if (!isset($res_decoded->newsletterId))
                                {
                                    // delete inserted recoreds
                                    $dele = DB::table('click_links')->whereIn('id', $link_tracking_id)->delete();
                                    $broadcaset_delete = EmailBroadcast::where('id', $broad_last_id)->delete();
                                    echo json_encode(array('date_err' => 1, 'msg' => $res_decoded->message));
                                    exit;
                                }

                                $broadCastId = $res_decoded->newsletterId;

                                // update
                                DB::table('email_broadcasts')
                                    ->where('id', $broad_last_id)
                                    ->update(['json' => $response, 'type' => 'get_response', 'user_id' => Auth::User()->id, 'subject_id' => $subject_id, 'boradcast_id' => $broadCastId, 'status' => 2]
                                    );
                                // update click link tracker
                                if (count($link_tracking_id) > 0)
                                {
                                    DB::table('click_links')
                                        ->whereIn('id', $link_tracking_id)
                                        ->update(
                                            [
                                                'type' => 'get_response',
                                                'subject_id' => $subject_id,
                                                'broadcast_id' => $broadCastId,
                                                'email_category_id' => $email_category_id,
                                                'email_campaign_title_id' => $email_campaign_title_id
                                            ]
                                        );
                                }

                                $usedAR = new UsedAutoResponder();
                                $usedAR->type = 'get_response';
                                $usedAR->replyto_email = $replyto;
                                $usedAR->fromFieldId = $from_field_id[$key];
                                $usedAR->campaignId = $compaign_id[$key];
                                $usedAR->selectedCampaigns = $selectCamapgin;
                                $usedAR->user_id = Auth::User()->id;
                                $usedAR->subject_id = $subject_id;
                                if ($broadCastId != '')
                                {
                                    $usedAR->newsletterId = $broadCastId;
                                }
                                $usedAR->save();
                            }

                        }// end foreach
                    }
                }

                /*
                 * sending MailChimp Campaign to multiple list using loop
                 * as MailChimp supports single List in one request
                 * */
                if ($request->send_mailchimp_broadcast == '1')
                {

                    // initializing arrays to variables
                    $api_key = $request->api_key_mailchimp_id;
                    $list_id = $request->list_id;
                    $title = $request->email_campaign_title;
                    $from_name = $request->from_name;
                    $reply_to = $request->reply_to;
                    $compaign_id = $request->campaignId;

                    foreach ($request->api_key_type_mailchimp as $key => $value)
                    {

                        foreach ($list_id[$api_key[$key]] as $k => $list_val)
                        {

                            $email_campaign_title_id = DB::table('email_campaign_title')->insertGetId(
                                ['user_id' => Auth::User()->id, 'title' => $email_campaign_title, 'email_category_id' => $email_category_id, 'parent_id' => $title_id]
                            );

                            $obj = new EmailTemplate();
                            $updating = 0;
                            if ($request->id != '')
                            {
                                $updating = 1;
                                $obj = $obj->findOrFail($request->id);
                            }
                            $obj->fill($request->all());
                            $obj->content = $request->contents;
                            $obj->category_id = $request->category_id;
                            $obj->subject_id = $subject_id;
                            $obj->email_category_id = $email_category_id;
                            $obj->email_campaign_title_id = $email_campaign_title_id;
                            $obj->user_id = Auth::User()->id;
                            $obj->save();


                            $stat_id = DB::table('autoresponder_new_stats')->insertGetId(
                                ['type' => 'mailchimp', 'user_id' => Auth::User()->id, 'email_campaign_title_id' => $email_campaign_title_id, 'email' => 'fake_email@gmail.com']
                            );


                            // insert empty data to email broadcast
                            $emailBroadCast = new EmailBroadcast();
                            $emailBroadCast->json = '';
                            $emailBroadCast->group_id = $group_id;
                            $emailBroadCast->type = '';
                            $emailBroadCast->user_id = Auth::User()->id;
                            $emailBroadCast->subject_id = 0;
                            $emailBroadCast->email_category_id = $email_category_id;
                            $emailBroadCast->email_campaign_title_id = $email_campaign_title_id;

                            if ($broadCastId != '')
                            {
                                $emailBroadCast->boradcast_id = '';
                            }

                            $emailBroadCast->save();
                            $broad_last_id = $emailBroadCast->id;

                            $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broad_last_id . "&subject_id=" . $subject_id . "&type=mailchimp&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=*|EMAIL|*' width='1' />";

                            // insert url to click_urls table   #########  LINK TRACKING
                            $link_tracking = $this->makeLinks($request->contents, '*|EMAIL|*');
                            $link_tracking_id = $link_tracking['ids'];
                            $str = $link_tracking['email_html'];

                            // *|EMAIL|* *|FNAME|* *|LNAME|*

                            $str = str_replace('%first_name%', '*|FNAME|*', $str);
                            $str = str_replace('%last_name%', '*|LNAME|*', $str);
                            $str = str_replace('%email_address%', '*|EMAIL|*', $str);

                            $ins = DB::table('campaign_send_list')
                                ->insert(['group_id' => $group_id, 'account_id' => $api_key[$key], 'campaign_title_id' => $email_campaign_title_id, 'list_id' => $list_val, 'type' => 'mailchimp']);

                            // insert data to schedule broadcast table

                            $json = array(
                                'list_id' => $list_val,
                                'title' => $title,
                                'subject_line' => $request->subject,
                                'from_name' => $from_name[$key],
                                'reply_to' => $reply_to[$key],
                                'html' => $str . $tracking_img
                            );
                            $json = json_encode($json);

                            $insert = DB::table('autoresponder_schedule')
                                ->insertGetId([
                                    'email_campaign_title_id' => $email_campaign_title_id,
                                    'subject_id' => $subject_id,
                                    'email_category_id' => $email_category_id,
                                    'user_id' => Auth::user()->id,
                                    'email_brodcast_id' => $broad_last_id,
                                    'type' => 'mailchimp',
                                    'campaign_id' => $selectCamapgin,
                                    'click_link_id' => json_encode($link_tracking_id),
                                    'status' => 1,
                                    'date_time' => date('Y-m-d H:i', strtotime($date . ' ' . $hour . ':' . $minute)),
                                    'json' => $json,
                                    'api_key' => $api_key[$key],

                                ]);


//                    $api_key = MailChimp_Setting::where('id', '=', $api_key[$key])->value('mailchimp_key');
//
//                        if ($start_time > $now)
// {
//                        // create crone
//                        // broadcast_id, click_id, subject_id, responder_type, api_key,title_id, list_id
//
//                        $json = array(
//                            'list_id' => $list_val,
//                            'title' => $title,
//                            'subject_line' => $request->subject,
//                            'from_name' => $from_name[$key],
//                            'reply_to' => $reply_to[$key],
//                            'html' => $str . $tracking_img
//                        );
//                        $json = json_encode($json);
//
//                        $insert = DB::table('autoresponder_schedule')
//                            ->insertGetId([
//                                'email_campaign_title_id' => $email_campaign_title_id,
//                                'subject_id' => $subject_id,
//                                'email_category_id' => $email_category_id,
//                                'user_id' => Auth::user()->id,
//                                'email_brodcast_id' => $broad_last_id,
//                                'type' => 'mailchimp',
//                                'campaign_id' => $selectCamapgin,
//                                'click_link_id' => json_encode($link_tracking_id),
//                                'status' => 1,
//                                'date_time' => date('Y-m-d H:i', strtotime($date. ' ' . $hour . ':' . $minute)),
//                                'json' => $json,
//                                'api_key' => $api_key,
//
//                            ]);
//
//                    } else {
//
//                        try {
//                            $mc = new Mailchimp($api_key);
//                            $selectCamapgin = $list_val;
//
//                            $result_cr = $mc->post('campaigns', [
//                                'type' => 'regular',
//                                'recipients' => [
//                                    'list_id' => $list_val
//                                ],
//                                'settings' => [
//                                    'title' => $title,
//                                    'subject_line' => $request->subject,
//                                    'from_name' => $from_name[$key],
//                                    'reply_to' => $reply_to[$key]
//                                ]
//                            ]);
//                        } catch (\Exception $e)
// {
//                            $err = $e->getMessage();
//                        }
//
//                        if (isset($err))
// {
//                            // delete inserted data
//                            $dele = DB::table('click_links')->whereIn('id', $link_tracking_id)->delete();
//                            $broadcaset_delete = EmailBroadcast::where('id', $broad_last_id)->delete();
//
//                            $err = json_decode($err);
//                            $err = $err->errors;
//                            $err = $err[0]->message;
//
//                            echo json_encode(array('date_err' => 1, 'msg' => $err));
//                            exit;
//
//                        }
//
//                        $campaign_id = isset($result_cr['id']) ? $result_cr['id'] : '';
//                        $emails_sent = isset($result_cr['emails_sent']) ? $result_cr['emails_sent'] : '';
//
//                        if ($campaign_id != '')
// {
//                            $broadCastId = $campaign_id;
//                            $post_cn = $mc->put("campaigns/$campaign_id/content", [
//                                'html' => $str . $tracking_img
//                            ]);
//                            try {
//                                $post_sn = $mc->post("campaigns/$campaign_id/actions/send", []);
//                            } catch (\Exception $e)
// {
//                                $err = $e->getMessage();
//                            }
//
//                            if (isset($err))
// {
//                                // delete inserted data
//                                $dele = DB::table('click_links')->whereIn('id', $link_tracking_id)->delete();
//                                $broadcaset_delete = EmailBroadcast::where('id', $broad_last_id)->delete();
//
//                                $err = json_decode($err);
//                                echo json_encode(array('date_err' => 1, 'msg' => $err->detail));
//                                exit;
//                            }
//
//                        }
//                        // update broadcast
//                        DB::table('email_broadcasts')
//                            ->where('id', $broad_last_id)
//                            ->update(['sent' => $emails_sent, 'json' => $response, 'type' => 'mailchimp', 'user_id' => Auth::User()->id, 'subject_id' => $subject_id, 'boradcast_id' => $broadCastId]
//                            );
//
//                        // update click link tracker
//                        if (count($link_tracking_id) > 0)
// {
//
//                            DB::table('click_links')
//                                ->whereIn('id', $link_tracking_id)
//                                ->update(
//                                    [
//                                        'type' => 'mailchimp',
//                                        'subject_id' => $subject_id,
//                                        'broadcast_id' => $broadCastId,
//                                        'email_category_id' => $email_category_id,
//                                        'email_campaign_title_id' => $email_campaign_title_id
//                                    ]
//                                );
//                        }
//
//                        $usedAR = new UsedAutoResponder();
//                        $usedAR->type = 'mailchimp';
//                        $usedAR->replyto_email = $replyto;
//                        //$usedAR->fromFieldId    = $from_field_id[$key];
//                        $usedAR->campaignId = $compaign_id[$key];
//                        $usedAR->selectedCampaigns = $selectCamapgin;
//                        $usedAR->user_id = Auth::User()->id;
//                        $usedAR->subject_id = $subject_id;
//                        if ($broadCastId != '')
// {
//                            $usedAR->newsletterId = $broadCastId;
//                        }
//                        $usedAR->save();
//                    }
                        }
                    }// end foreach
                }

                /*
                 * sending Aweber Campaign to multiple list using loop
                 * as Aweber supports single List in one request
                 * */
                if ($request->send_aweber_broadcast == '1')
                {


                    if ($start_time < $now)
                    {
                        echo json_encode(array('date_err' => 1, 'msg' => 'Schedule date is in past!'));
                        exit;
                    } else {

                        $api_key = $request->api_key_aweber_id;
                        $selected_list = $request->selectedList;
                        $title = $request->email_campaign_title;

                        $compaign_id = $request->campaignId;
                        $from_field_id = '';

                        foreach ($request->api_key_type_aweber as $key => $value)
                        {
                            $add_min = 2;
                            foreach ($selected_list[$api_key[$key]] as $k => $list_val)
                            {
                                $email_campaign_title_id = DB::table('email_campaign_title')->insertGetId(
                                    ['user_id' => Auth::User()->id, 'title' => $email_campaign_title, 'email_category_id' => $email_category_id, 'parent_id' => $title_id]
                                );

                                $obj = new EmailTemplate();
                                $updating = 0;
                                if ($request->id != '')
                                {
                                    $updating = 1;
                                    $obj = $obj->findOrFail($request->id);
                                }
                                $obj->fill($request->all());
                                $obj->content = $request->contents;
                                $obj->category_id = $request->category_id;
                                $obj->subject_id = $subject_id;
                                $obj->email_category_id = $email_category_id;
                                $obj->email_campaign_title_id = $email_campaign_title_id;
                                $obj->user_id = Auth::User()->id;

                                $obj->save();


                                $stat_id = DB::table('autoresponder_new_stats')->insertGetId(
                                    ['type' => 'aweber', 'user_id' => Auth::User()->id, 'email_campaign_title_id' => $email_campaign_title_id, 'email' => 'fake_email@gmail.com']
                                );

                                $ins = DB::table('campaign_send_list')
                                    ->insert(['group_id' => $group_id, 'account_id' => $api_key[$key], 'campaign_title_id' => $email_campaign_title_id, 'list_id' => $list_val, 'type' => 'aweber']);

                                // insert empty data to email broadcast
                                $emailBroadCast = new EmailBroadcast();
                                $emailBroadCast->json = '';
                                $emailBroadCast->group_id = $group_id;
                                $emailBroadCast->type = '';
                                $emailBroadCast->user_id = Auth::User()->id;
                                $emailBroadCast->subject_id = 0;
                                $emailBroadCast->email_category_id = $email_category_id;
                                $emailBroadCast->email_campaign_title_id = $email_campaign_title_id;
                                if ($broadCastId != '')
                                {
                                    $emailBroadCast->boradcast_id = '';
                                }
                                $emailBroadCast->save();
                                $broad_last_id = $emailBroadCast->id;

                                $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broad_last_id . "&subject_id=" . $subject_id . "&type=aweber&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email={!email}' width='1' />";

                                // insert url to click_urls table   #########  LINK TRACKING
                                $link_tracking = $this->makeLinks($request->contents, '{!email}');
                                $link_tracking_id = $link_tracking['ids'];
                                $str = $link_tracking['email_html'];

                                //{!firstname_fix}{!lastname_fix}{!email}

                                $str = str_replace('%first_name%', '{!firstname_fix}', $str);
                                $str = str_replace('%last_name%', '{!lastname_fix}', $str);
                                $str = str_replace('%email_address%', '{!email}', $str);

                                // insert in schedule
                                $json = array(
                                    'list_id' => $list_val,
                                    'title' => $title,
                                    'subject_line' => $request->subject,
                                    'html' => $str . $tracking_img
                                );
                                $json = json_encode($json);

                                $time = new Carbon($start_time);
                                $time = $time->addMinutes($add_min);

                                $insert = DB::table('autoresponder_schedule')
                                    ->insertGetId([
                                        'email_campaign_title_id' => $email_campaign_title_id,
                                        'subject_id' => $subject_id,
                                        'email_category_id' => $email_category_id,
                                        'user_id' => Auth::user()->id,
                                        'email_brodcast_id' => $broad_last_id,
                                        'type' => 'aweber',
                                        'campaign_id' => $selectCamapgin,
                                        'click_link_id' => json_encode($link_tracking_id),
                                        'status' => 1,
                                        'date_time' => $time,
                                        'json' => $json,
                                        'api_key' => $api_key[$key],

                                    ]);
                                $add_min++;
                            }
                        }// end foreach
                    }
                }

                if ($request->activecompaign == '1')
                {

                    // initializing arrays to variables
                    $api_key = $request->api_key_activecampaign_id;
                    $subject = $request->active_title;
                    $from_name = $request->active_from_name;
                    $from = $request->active_from;
                    $reply2 = $request->active_reply_to;
                    $priority = $request->priority;
                    $list_ids = $request->list_ids;
                    // list ids
                    $list_ids_ = array();
                    $count = 0;

                    $filter_unique = array_count_values($request->api_key_activecampaign_id);
                    foreach ($filter_unique as $key => $value)
                    {

                        $email_campaign_title_id = DB::table('email_campaign_title')->insertGetId(
                            ['user_id' => Auth::User()->id, 'title' => $email_campaign_title, 'email_category_id' => $email_category_id, 'parent_id' => $title_id]
                        );

                        $stat_id = DB::table('autoresponder_new_stats')->insertGetId(
                            ['type' => 'activecompaign', 'user_id' => Auth::User()->id, 'email_campaign_title_id' => $email_campaign_title_id, 'email' => 'fake_email@gmail.com']
                        );


                        $obj = new EmailTemplate();
                        $updating = 0;
                        if ($request->id != '')
                        {
                            $updating = 1;
                            $obj = $obj->findOrFail($request->id);
                        }
                        $obj->fill($request->all());
                        $obj->content = $request->contents;
                        $obj->category_id = $request->category_id;
                        $obj->subject_id = $subject_id;
                        $obj->email_category_id = $email_category_id;
                        $obj->email_campaign_title_id = $email_campaign_title_id;
                        $obj->user_id = Auth::User()->id;

                        $obj->save();

                        // insert empty data to email broadcast
                        $emailBroadCast = new EmailBroadcast();
                        $emailBroadCast->json = '';
                        $emailBroadCast->group_id = $group_id;
                        $emailBroadCast->type = '';
                        $emailBroadCast->user_id = Auth::User()->id;
                        $emailBroadCast->subject_id = 0;
                        $emailBroadCast->email_category_id = $email_category_id;
                        $emailBroadCast->email_campaign_title_id = $email_campaign_title_id;
                        if ($broadCastId != '')
                        {
                            $emailBroadCast->boradcast_id = '';
                        }
                        $emailBroadCast->save();
                        $broad_last_id = $emailBroadCast->id;

                        $tracking_img = "<img border='0' height='1' src='" . asset('') . "parser/tracking.php?user_id=" . Auth::user()->id . "&broadPId=" . $broad_last_id . "&subject_id=" . $subject_id . "&type=activecampaign&campaign_title_id=" . $email_campaign_title_id . "&campaign_cat_id=" . $email_category_id . "&email=%EMAIL%' width='1' />";

                        // insert url to click_urls table   #########  LINK TRACKING
                        $link_tracking = $this->makeLinks($request->contents, '%EMAIL%');
                        $link_tracking_id = $link_tracking['ids'];
                        $str = $link_tracking['email_html'];

                        // %FIRSTNAME%    %LASTNAME% %EMAIL%

                        $str = str_replace('%first_name%', '%FIRSTNAME%', $str);
                        $str = str_replace('%last_name%', '%LASTNAME%', $str);
                        $str = str_replace('%email_address%', '%EMAIL%', $str);

                        foreach ($list_ids[$key] as $k => $list_val)
                        {
                            // insert data to list table
                            $ins = DB::table('campaign_send_list')
                                ->insert(['group_id' => $group_id, 'account_id' => $key, 'campaign_title_id' => $email_campaign_title_id, 'list_id' => $list_val, 'type' => 'activecampaign']);

                            $list_ids_['p[' . $list_val . ']'] = $list_val;
                        }

                        $date = $request->active_date;

                        $schedule = $hour . ':' . $minute . ':00';
                        $settings = Activecampaign_Setting::where('id', '=', $key)->first();
                        // message add to system of api

                        $url = $settings->activecampaign_domain . '/admin/api.php';
                        $method = 'post';
                        $body = [
                            'api_key' => $settings->activecampaign_key,
                            'api_action' => 'message_add',
                            'api_output' => 'json',
                            'format' => 'html',
                            'subject' => $request->subject,
                            'fromname' => $from_name[$count],
                            'fromemail' => $from[$count],
                            'reply2' => $reply2[$count],
                            'priority' => 3,
                            'charset' => 'utf-8',
                            'encoding' => 'quoted-printable',
                            'htmlconstructor' => 'editor',
                            'html' => $str . $tracking_img,
                            "text" => "",
                            "p[1]" => 1
                        ];

                        $response = $this->Curl($url, $body, $method, $get_response_key = '');
                        $message = json_decode($response);

                        // print_r($message);
                        // if success
                        if ($message->result_code == 1)
                        {
                            $message_id = $message->id;

                            $body = [
                                'api_key' => $settings->activecampaign_key,
                                'api_action' => 'campaign_create',
                                'api_output' => 'json',
                                "type" => "single",
                                "segmentid" => "0",
                                "name" => $from_name[$count],
                                "sdate" => $date . ' ' . $schedule,
                                "status" => "1",
                                "public" => "1",
                                "tracklinks" => "all",
                                "trackreads" => "1",
                                "trackreplies" => "0",
                                "trackreadsanalytics" => "0",
                                "tweet" => "0",
                                "facebook" => "0",
                                "embed_images" => "0",
                                "htmlunsub" => "1",
                                "textunsub" => "1",
                                "p[1]" => "1",
                                "m[$message_id]" => 100000

                            ];
                            // merge list id and body
                            $body = array_merge($body, $list_ids_);

                            $response = $this->Curl($url, $body, $method, $get_response_key = '');
                            $data = json_decode($response);

                            // print_r($data);

                            if (!isset($data->id))
                            {

                                // delete inserted data
                                $dele = DB::table('click_links')->whereIn('id', $link_tracking_id)->delete();
                                $broadcaset_delete = EmailBroadcast::where('id', $broad_last_id)->delete();

                                $message = "Invalid Account Or unknown error!";
                                if (isset($data->result_message))
                                {
                                    $message = $data->result_message;
                                }

                                echo json_encode(array('date_err' => 1, 'msg' => $message));
                                exit;

                            }
                            $broadCastId = $data->id;

                            // update broadcast
                            DB::table('email_broadcasts')
                                ->where('id', $broad_last_id)
                                ->update(['json' => $response, 'message_id' => $message_id, 'type' => 'activecampaign', 'user_id' => Auth::User()->id, 'subject_id' => $subject_id, 'boradcast_id' => $broadCastId, 'status' => 2]
                                );
                            // update click link tracker
                            if (count($link_tracking_id) > 0)
                            {

                                DB::table('click_links')
                                    ->whereIn('id', $link_tracking_id)
                                    ->update(
                                        [
                                            'type' => 'activecampaign',
                                            'subject_id' => $subject_id,
                                            'broadcast_id' => $broadCastId,
                                            'email_category_id' => $email_category_id,
                                            'email_campaign_title_id' => $email_campaign_title_id
                                        ]
                                    );
                            }

                            $usedAR = new UsedAutoResponder();
                            $usedAR->type = 'activecampaign';

                            $usedAR->user_id = Auth::User()->id;
                            $usedAR->subject_id = $subject_id;
                            if ($broadCastId != '')
                            {
                                $usedAR->newsletterId = $broadCastId;
                            }
                            $usedAR->save();
                        }
                        $count++;
                    }
                }

                if ($broadCastId != '')
                {
                    echo json_encode(array('id' => $subject_id, 'msg' => 'Email has been queued.'));
                } else {
                    $obj = new EmailTemplate();
                    $updating = 0;
                    if ($request->id != '')
                    {
                        $updating = 1;
                        $obj = $obj->findOrFail($request->id);
                    }
                    $obj->fill($request->all());
                    $obj->content = $request->contents;
                    $obj->category_id = $request->category_id;
                    $obj->subject_id = $subject_id;
                    $obj->email_category_id = $email_category_id;
                    $obj->email_campaign_title_id = $request->email_campaign_title;
                    $obj->user_id = Auth::User()->id;

                    if ($obj->save())
                    {
                        echo json_encode(array('id' => $subject_id, 'msg' => 'Email has been saved.'));
                    }
                }
            }

//        }else{
//            echo json_encode(array('id'=>0, 'msg'=>'Something went wrong. Please reload page and try again.'));
//        }
        }catch (\Exception $e)
        {
            echo json_encode(array('id'=>0,'msg'=> $e->getLine() ) );

        }
    }

    function makeLinks($text, $email)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        // The Text you want to filter for urls
        // Check if there is a url in the text
        if (preg_match_all($reg_exUrl, $text, $url))
        {

        }
        $reg_exUrl = '/(?<!src=[\'"])(((f|ht)
        {1}tps?:\/\/)[-a-zA-Z0-9@:%_\+.~#?&\/\/=]+)/i';
        if (preg_match_all($reg_exUrl, $text, $url))
        {
            // make the urls hyper links

            $matches = array_unique($url[0]);
            $ids = array();
            foreach($matches as $match)
            {
                $redirect_url = $match;/////////////

                // insert your emails to database and get id
                $insert_id = DB::table('click_links')->insertGetId(
                    [
                        'user_id' => Auth::user()->id,
                        'redirect_link' => $redirect_url
                    ]
                );

                $link = asset('').'parser/click_tracking.php?id='.$insert_id.'&email='.$email;
                $ids[] = $insert_id;

                $replacement = '<a href="'.$link.'">'.$match.'</a>';

                $replacement = '<a href="'.$link.'">'.$match.'</a>';
                $text = str_replace($match,$replacement,$text);
                $text = str_replace('<a href="<a href="', '<a href="' , $text);
                $text = str_replace(''.$match.'</a>">', '' , $text);

                $text = str_replace('scr="'.$replacement.'"',"src='$match'",$text);
                $text = str_replace("scr='".$replacement."'",'src="'.$match.'"',$text);

            }
            $return = array('ids' => $ids, 'email_html' =>   str_replace('"', "'" , $text));
            return $return;
        } else {
            // if no urls in the text just return the text
            $return = array('ids' => array(), 'email_html' => $text);
            return $return;
        }
    }

    // download file
    function downloadEmail( Request $request )
    {

        /*$file_name = str_replace(" ", "-", $request->file_subject);
        $data = 'Subject: '.$request->file_subject;
        $data .= "\n\n ".PHP_EOL." \n\n";
        $data .= $request->file_content;*/

        if ($request->file_type=='html')
        {
            $data = $request->file_content;
        }else{
            $data = strip_tags($request->file_content);
        }
        $file_name = $request->file_subject;
        if ($file_name == '')
        {
            $file_name = 'Email content';
        }

        header('Content-Disposition: attachment; filename="'.$file_name.'.txt"');
        header('Content-Type: text/plain');
        header('Content-Length: ' . strlen($data));
        header('Connection: close');

        echo $data;
    }

    function loadSearchedEmail(Request $request)
    {
        if (isset($request->use_me_content_id))
        {

            $content = EmailTemplate::where('subject_id', $request->use_me_content_id)->value('content');
        }
        if (isset($request->content_id))
        {
            //$content = EmailTemplate::where('id', $request->contents_id)->value('content');
            $subject_id = str_replace("cont_", "", $request->content_id);
            $content = EmailTemplate::where('id', $subject_id)->value('content');
        }
        // donate change or remove this line, its replacing
        echo str_replace(' ', " ",$content);
    }

    public function storeImport(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**we
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        $data = EmailTemplate::findOrFail($id);
        $subject = DB::table('email_subjects')->where('id', $id)->first();
        $content = DB::table('email_contents')->where('subject_id', $id)->first();

        return view("admin.new_email_template.add", ['content'=>$content, 'subject' => $subject] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function selectArApi(Request $request)
    {

        $api_type = $request->type;

        if ($api_type=='aweber')
        {
            $api_list = Aweber_Setting::where('user_id','=', Auth::User()->id)->get();
        }

        if ($api_type=='get_response')
        {
            $api_list = GetResponse_Setting::where('user_id','=', Auth::User()->id)->get();
        }

        if ($api_type=='mailchimp')
        {
            $api_list = MailChimp_Setting::where('user_id','=', Auth::User()->id)->get();
        }

        if ($api_type=='sendlane')
        {
            $api_list = SendLane_Setting::where('user_id','=', Auth::User()->id)->get();
        }

        if ($api_type=='activecampaign')
        {
            $api_list = Activecampaign_Setting::where('user_id','=', Auth::User()->id)->get();
        }

        return view("admin.new_email_template.select_api_key", ['api_list'=>$api_list]);

    }

    public function loadArCampaings(Request $request)
    {


        $type = $request->type;

        if ($type=='get_response')
        {
            $get_response_key = GetResponse_Setting::where('id', '=', $request->api_key)->value("get_response_key");

            if ($get_response_key=='' || $get_response_key==null)
            {
                echo 'get_response_api_failed';
                return '';
            }

            //method get
            $url = 'https://api.getresponse.com/v3/campaigns';
            $from_fields_url = 'https://api.getresponse.com/v3/from-fields';
            $url2 = 'https://api.getresponse.com/v3/campaigns/TXPaY';

            $result      = $this->Curl($url, '', 'get', $get_response_key);
            $from_fields = $this->Curl($from_fields_url, '', 'get', $get_response_key);
            //print_r($result);
            $return =  view("admin.new_email_template.get_response_campaigns",
                ['data'=>json_decode($result), 'from_fields'=>json_decode($from_fields), 'id' => $request->api_key]
            );

        }

        if ($type=='aweber')
        {
            //aweber access token
            $settings = Aweber_Setting::where('id', '=', $request->api_key)->first();

            if ($settings->accessTokenKey=='' || $settings->accessTokenKey==null)
            {
                echo 'aweber_credentials_failed';
                return '';
            }

            require_once(getcwd().'/aweber_api/aweber_api.php');
            try{

                $aweber = new \AWeberAPI($settings->aweber_key, $settings->aweber_secret);
                $aweber->adapter->debug = false;

                $account = $aweber->getAccount($settings->accessTokenKey, $settings->accessTokenSecret);
                $list    = $account->lists;
                $entries = $list->data['entries'];
            }catch(\Exception $exc)
            {
                $err = $exc->getMessage();
            }

            if (isset($err))
            {
                $err = explode('.', $err);
                $return =  view("admin.new_email_template.aweber_lists",['err' => $err[0]]);
            }else {
                $return = view("admin.new_email_template.aweber_lists", ['data' => $entries, 'id' => $request->api_key]);
            }

        }

        if ($type=='mailchimp')
        {


            $api_key = MailChimp_Setting::where('id', '=', $request->api_key)->value('mailchimp_key');

            if ($api_key=='' || $api_key==null)
            {
                echo 'mailchimp_credentials_failed';
                return '';
            }

            // get exception

            try {

                $mc = new Mailchimp($api_key);
                $result = $mc->request('lists', []);
                $data = $result['lists'];
            }catch(\Exception $e)
            {
                $err = $e->getMessage();
            }

            if (isset($err))
            {
                $err = json_decode($err);

                if ( !isset($err->title))
                {
                    $err = 'Invalid Account';
                }elseif ( isset($err->title) )
                {
                    $err = $err->title;
                }

                $return =  view("admin.new_email_template.mailchimp_lists",['err' => $err ]);
            }else {

                $return = view("admin.new_email_template.mailchimp_lists", ['data' => $data, 'id' => $request->api_key]);
            }
        }

        // sendlane
        if ($type == 'activecampaign')
        {

            $id = $request->api_key;

            //sendlane access token
            $settings = Activecampaign_Setting::where('id', '=', $id)->first();

            if ($settings->activecampaign_key == '' || $settings->activecampaign_domain == '')
            {
                echo 'sendlane_credentials_failed';
                return '';
            }
            // list view

            $url = $settings->activecampaign_domain . '/admin/api.php';
            $method = 'post';
            $body = [
                'api_key' => $settings->activecampaign_key,
                'api_action' => 'list_list',
                'api_output' => 'json',
                'ids' => 'all',
                'full' => '1'
            ];

            $response = $this->Curl($url, $body, $method, $get_response_key = '');
            $list = json_decode($response);

            if ($list->result_code != 0)
            {
                $return = view("admin.new_email_template.activecampaign_lists", compact('list', 'id'));
            }else{
                $return = view("admin.new_email_template.activecampaign_lists");
            }
        }

        return $return;

    }


    private function Curl($url, $body, $method, $get_response_key)
    {


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );

        //Get-Response API Request
        if ($get_response_key!='')
        {

            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Auth-Token: api-key $get_response_key"));

            if ($method == "post")
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","X-Auth-Token: api-key $get_response_key"));

            //Aweber API Request
        }else{

            if ($method == "post")
                curl_setopt($ch, CURLOPT_POST, true );

            if ($method == "get")
                curl_setopt($ch, CURLOPT_HTTPGET, true );
        }


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_PUT, true );

        if ($method == "post")
            curl_setopt($ch, CURLOPT_POST, true );

        if ($method == "get")
            curl_setopt($ch, CURLOPT_HTTPGET, true );


        if ($body!="")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $filecontents=curl_exec($ch);

        curl_close($ch);

        return $filecontents;
    }
}
