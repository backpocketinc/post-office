<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CreateCpanelAccountController extends Controller
{

    public Function createAccount($newDomain = "", $newUser = "", $newPass = "", $newPlan = "", $newEmail = "")
    {

        $whm_user = "ranksol";
        $whm_pass = "Sk8GXcD1YacS";
        $whm_host = "bmw.websitewelcome.com";

        $newDomain = $this->fixData($newDomain);
        $newUser = $this->fixData($newUser);
        $newPlan = $this->fixData($newPlan);

        If (!empty($newUser))
        {
            // Get new session ID
            $sessID = $this->newSession($whm_host, $whm_user, $whm_pass);

            //$script = "https://www." . $whm_host . ":2087" . $sessID . "/scripts/wwwacct";
            //$sData = "?plan={$newPlan}&domain={$newDomain}&username={$newUser}&password={$newPass}&contactemail={$newEmail}";
            $script = "https://" . $whm_host . ":2087" . $sessID . "/json-api/createacct?api.version=1";
            $sData = "&username=$newUser";
            $sData .= "&domain=$newDomain";
            $sData .= "&plan=$newPlan";
            $sData .= "&contactemail=$newEmail";
            $sData .= "&password=$newPass";
            $sData .= "&featurelist=default";

            $sData .= "&quota=0";
            $sData .= "&ip=n";
            $sData .= "&cgi=1";
            $sData .= "&hasshell=2";
            $sData .= "&cpmod=paper_lantern";
            $sData .= "&maxftp=10";
            $sData .= "&maxsql=10";
            $sData .= "&maxpop=10";
            $sData .= "&maxlst=5";
            $sData .= "&maxsub=5";
            $sData .= "&maxpark=5";
            $sData .= "&maxaddon=5";
            $sData .= "&bwlimit=500";
            $sData .= "&language=en";
            $sData .= "&useregns=1";
            $sData .= "&hasuseregns=1";
            $sData .= "&reseller=1";
            $sData .= "&forcedns=0";

            //Whether to overwrite an existing DNS zone with the new account's information,
            //if a matching DNS zone already exists. This parameter defaults to 0.
            // 1 � Overwrite.
            // 0 � Do not overwrite.

            $sData .= "&mailbox_format=mdbox";
            $sData .= "&mxcheck=local";
            $sData .= "&max_email_per_hour=500";
            $sData .= "&max_defer_fail_percentage=80";
            //$sData .= "&owner=root";



            // Authenticate User for WHM access
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Authorization: Basic " . base64_encode("{$whm_user}:{$whm_pass}"),
                ),
            ));

            // Get result
            $result = file_get_contents($script.$sData, false, $context);
            //echo "URL: " . $script.$sData . "<br /><br />";
            //echo "Result: " . $result;
            return $result;

        } Else {
            return "Error: Username is empty!";
        }
    }


    private Function newSession($nHost, $nUser, $nPass)
    { // Example details

        $ip         = $nHost;
        $cp_user    = $nUser;
        $cp_pwd     = $nPass;
        $url = "https://$ip:2087/login";

        // Create new curl handle
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$cp_user&pass=$cp_pwd");
        curl_setopt($ch, CURLOPT_TIMEOUT, 100020);

        // Execute the curl handle and fetch info then close streams.
        $f = curl_exec($ch);
        $h = curl_getinfo($ch);
        curl_close($ch);

        // If we had no issues then try to fetch the cpsess
        if ($f == true and strpos($h['url'],"cpsess")) {
            // Get the cpsess part of the url
            $pattern="/.*?(\/cpsess.*?)\/.*?/is";
            $preg_res=preg_match($pattern,$h['url'],$cpsess);
        }

        // If we have a session then return it otherwise return empty string
        return (isset($cpsess[1])) ? $cpsess[1] : "";
    }



    private Function fixData($data)
    {
        $data = str_replace("-","%2D",$data);
        $data = str_replace(".","%2E",$data);
        $data = str_replace(" ","%20",$data);

        return $data;
    }



    private Function returnData($data)
    {
        $data = str_replace("%2D","-",$data);
        $data = str_replace("%2E",".",$data);
        $data = str_replace("%20"," ",$data);

        return $data;
    }


}
