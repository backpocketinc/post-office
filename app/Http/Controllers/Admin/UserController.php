<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Auth;
use DB;
use App\Categories;
use App\EmailTemplate;
use App\SubjectTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;

class UserController extends Controller
{
    private  $notification_subject, $notification_to_email = "";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$domains = DB::table('cpanels')->select('id')->where('domain', '=', $v)->first();
        $email_category = DB::table('email_category')->select('*')->get();
        return view("admin.user.list", compact('email_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function create()
    {

    }



    public function loadProfileSetting()
    {
        $user = User::all()->where('id', Auth::user()->id)->first();
        return view("admin.user.profile_settings")->with('user', $user);
    }

    // profile setting
    function profileSetting(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,bmp,png'
        ]);
        if ($validator->fails()) {
            return response()->json(['msg' => 2, 'error'=> 'The image must be a file of type: jpeg, bmp, png.']);
        }



        $is_file = false;
        $old_file = Auth::user()->image;
        $user =   User::find(Auth::user()->id);

        /* just for demo */
        if(Auth::user()->type=='a'){
            $password = 'password';
        }else{
            $password = $request->password;
        }

        if ($user)
        {
            if ($request->hasFile('image')) {
                $is_file = true;
                $file_name = $input['image'] = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(base_path() . '/assets/images/users/', $input['image']);
                $update_data = array('time_zone' => $request->time_zone ,'password' => bcrypt($password), 'name' => $request->name, 'plain_password' => $password, 'image' => $file_name);
            }else{
                $file_name = $old_file;
                $update_data = array('time_zone' => $request->time_zone ,'password' => bcrypt($password), 'name' => $request->name, 'plain_password' => $password );
            }

            $update = DB::table('users')
                ->where('id', Auth::user()->id)
                ->update( $update_data );

            if ($update)
            {
                // Delete old image
                if ($is_file) {
                    if (file_exists(base_path('assets/images/users/' . $old_file)) && $old_file != null ) {
                        unlink(base_path('assets/images/users/' . $old_file));
                    }
                }
                return response()->json(['msg' => 1, 'file_name'=> $file_name ]);
            }else{
                return response()->json(['msg' => 2]);
            }
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * also receiving request on add-user
     */
    public function store(Request $request)
    {
        $email_category = implode(',', $request->email_category);

        $obj = new User();
        if ($request->id!='')
        {
            $obj = $obj->findOrFail($request->id);
        }

        /* Start for request on add-user */
        $status = 'Inactive';
        if ($request->status==1)
        {
            $obj->status = $request->status;
            $status = 'Active';
        }
        /* Ends for request on add-user */

        $obj->fill($request->all());
        $obj->password = bcrypt($request->password);
        $obj->plain_password = $request->password;
        $obj->email_category = $email_category;

        if ($obj->save())
        {

            $email_temp = DB::table('email_settings')->select('registration_subject','registration_content')->where('user_id', Auth::user()->id)->first();
//            if (!$email_temp)
//{
//                echo json_encode(array('msg'=> 2 ));
//                exit;
//            }
            $subject = $email_temp->registration_subject;
            $content = $email_temp->registration_content;

            $content = str_replace('%email%', $request->email, $content);
            $content = str_replace('%name%', $request->name, $content);
            $content = str_replace('%status%', $status, $content);
            $content = str_replace('%password%', $request->password, $content);

            $this->notification_to_email = $request->email;
            $this->notification_subject = $subject;
//             view('admin.user.email_notification',['subject' => $subject, 'content' => $content]);

            $data = array('email' => $request->email, 'content' => $content, 'subject' => $subject );

            Mail::send('admin.user.email_notification', $data, function($msg)
            {
                $msg->subject($this->notification_subject);
                $msg->to($this->notification_to_email);
            });

            /*$c_info = '';
            $cpanel_data = json_decode($createCpanel);
            $c_info = $cpanel_data->metadata->output->raw;*/
            //echo $createCpanel;
            echo json_encode(array('msg'=>1));

            //echo json_encode(array('response'=>'success', 'cpanel_info'=>$createCpanel));

        }else{
            //echo json_encode(array('response'=>'error', 'cpanel_info'=>''));
            echo json_encode(array('msg'=>0));
        }
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function loadEdit(Request $request)
    {
        $data = User::findOrFail($request->id);
        if ($data)
        {
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function userSendPassword(Request $request)
    {
        $user = User::where('email', $request->email)->select('email', 'name', 'plain_password')->first();


        if (count($user) > 0 )
        {
            $subject = 'Email UpShot Password Recover Notification';
            $to =$user->email;
            $from = env(MAIL_FROM_ADDRESS);

            $headersfrom='';
            $headersfrom .= 'MIME-Version: 1.0' . "\r\n";
            $headersfrom .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headersfrom .= 'From: '.$from.' '. "\r\n";

            $body = '

                <h2>Hello!</h2>
                <p> '.ucwords( $user->name ).'</p>

                <p>You are receiving this email because we received a password reset request for your account.<p>
                <p>Email: '.$user->email.'</p>
                <p>Password: '.$user->plain_password.'<p>
                <p>Application URL is '.asset('').' </p>';

                $mail = mail($to, $subject, $body, $headersfrom);

            if ($mail)
            {
                echo 1;
            }

        }else{
            echo 2;
        }
    }
    // user stats
    function userStats(Request $request)
    {

        $user_id = $request->user_id;
        // opened
        $total_opened =  DB::table('email_campaign_title')
            ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
            ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
            ->join('autoresponder_new_stats', 'email_campaign_title.id', '=', 'autoresponder_new_stats.email_campaign_title_id' )
            ->select(
                'email_campaign_title.user_id',
                'email_campaign_title.id',
                'email_campaign_title.email_category_id',
                'email_campaign_title.title',
                'email_campaign_title.created_at',
                'email_contents.subject_id',
                'email_broadcasts.boradcast_id as broadcast_id',
                'email_campaign_title.id as title_id'
            )
            ->where('autoresponder_new_stats.user_id', $user_id)
            ->whereColumn('email_broadcasts.boradcast_id', 'autoresponder_new_stats.email_brodcast_id' )
            ->where('autoresponder_new_stats.type', '!=', '' )
            //open
            ->where(['autoresponder_new_stats.stat_type'=> 'open'])
            ->where('autoresponder_new_stats.email', '!=', '')
            ->count();
        // clicked
        $total_clicked =  DB::table('email_campaign_title')
            ->join('email_contents', 'email_campaign_title.id', '=', 'email_contents.email_campaign_title_id' )
            ->join('email_broadcasts', 'email_campaign_title.id', '=', 'email_broadcasts.email_campaign_title_id' )
            ->join('autoresponder_new_stats', 'email_campaign_title.id', '=', 'autoresponder_new_stats.email_campaign_title_id' )
            ->select(
                'email_campaign_title.user_id',
                'email_campaign_title.id',
                'email_campaign_title.email_category_id',
                'email_campaign_title.title',
                'email_campaign_title.created_at',
                'email_contents.subject_id',
                'email_broadcasts.boradcast_id as broadcast_id',
                'email_campaign_title.id as title_id'
            )
            ->where('autoresponder_new_stats.user_id', $user_id)

            ->where(['autoresponder_new_stats.stat_type'=> 'click'])
            ->count();

        $templates = EmailTemplate::where('user_id', $request->user_id)->count('id');
        return view('admin.user.stats', ['opened'=> $total_opened, 'clicked'=> $total_clicked, 'templates' => $templates]);
    }
}
