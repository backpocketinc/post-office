<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Aweber;
use Auth;
use App\Settings;
use DB;
//use PHPMailer;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use \Mailjet\Resources;

use Dyn\MessageManagement;
use Dyn\MessageManagement\Mail;
use SimpleEmailService;
use Sendinblue\Mailin;
use Leadersend;
use Tipimail\Tipimail;
use Tipimail\Exceptions;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // profile
    public function create()
    {
        $data_mailjet = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'mailjet' ])
            ->get();

        $data_sendgrid = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'sendgrid' ])
            ->get();

        $data_sparkpost = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'sparkpost' ])
            ->get();

        $data_smtp = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'smtp' ])
            ->get();

        $data_leadersend = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'leadersend' ])
            ->get();

        $data_dyn = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'dyn' ])
            ->get();

        $data_amazon = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'amazon' ])
            ->get();

        $data_mailgnu = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'mailgun' ])
            ->get();

        $data_sendinblue = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'sendinblue' ])
            ->get();

        $data_tipimail = DB::table('email_sender')
            ->where([ 'user_id' => Auth::user()->id, 'type' => 'tipimail' ])
            ->get();

        return view('admin.email_sender.settings', compact('data_tipimail', 'data_leadersend','data_sendinblue', 'data_amazon', 'data_dyn','data_mailjet', 'data_mailgnu','data_smtp', 'data_sendgrid', 'data_sparkpost'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj = new Settings();

        if ($request->id!='') {
            $obj = $obj->findOrFail($request->id);
        }else{
            $obj->user_id = Auth::User()->id;
        }

        $obj->fill($request->all());
        if ($obj->save())
        {
            echo 1;
        }else{
            echo 0;
        }
    }

    function emailSenderSettings(Request $request)
    {

        $api_key = $request->api_key;
        $title = $request->title;
        $from_name = $request->from_name;
        $from_email = $request->from_email;
        $test_email = $request->test_email;
        $object = $request->object;
        $domain = $request->domain;

        $subject = env('APP_NAME').' test email';
        $to = $test_email;
        $to_name = $from_name;

        $type = str_replace('_settings', '', $object);

        $body = '

    <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
<tbody>
<tr>
<td  style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
    <h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">Hello!</h1>
<p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">It is '.ucfirst( $type ).' test email please ignore.</p>
    <table  align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:30px auto;padding:0;text-align:center;width:100%"><tbody><tr>
<td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
<td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
    <table border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
    </tr>
    </tbody>
    </table>
</td>
    </tr></tbody></table>
</td>
    </tr></tbody></table>
</td>
    </tr>
<tr>
<td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
        <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:0 auto;padding:0;text-align:center;width:570px">
        <tbody>
        <tr>
            <td  align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;line-height:1.5em;margin-top:0;color:#aeaeae;font-size:12px;text-align:center"> © '. date('Y') .' '.env('APP_NAME').'</p>
                </td>
            </tr>
            </tbody>
            </table>
</td>
</tr>
</tbody>
</table>
</td>
        </tr>
    </tbody>
</table>
        <div class="yj6qo">
        </div>
        <div class="adL">
</div></div>';

        // ===============  EMAIL SENDER API'S CONFIGRATION   ================

        // tipimail
        if ($object == 'tipimail_settings')
        {
            $host = $request->host;

            $tipimail = new Tipimail( $host, $api_key );

            print_r($tipimail);
        }
        // leadersend
        if ($object == 'leadersend_settings')
        {
            include_once(getcwd() . '/leadersend/inc/Leadersend.class.php');
            $LS = new Leadersend($api_key);
            $response = $LS->ping();
            if ($LS->errorCode)
            {
                printf("Error (%s): %s", $LS->errorCode, $LS->errorMessage);
            } else {
                //print_r($response);
                $request = array(
                    'to' => array(    // required. A list of recipients
                        // recipient #1 with name and email
                        array(
                            'name'  => $to_name,
                            'email' => $to
                        ),
                        // recipinet #2 with just email
                        //'test@example.com'
                    ),
                    'raw' => $body,
                    'signing_domain' => $domain
                );

                //print_r($request);
                $response = $LS->messagesSendRaw($request);
                if ($LS->errorCode)
                {
                    echo json_encode([ 'err' => 1,  'message' =>  $LS->errorMessage ]);
                    exit;
                } else {
                    //
                }
            }
        }
        // sendingreen
        if ($object == 'sendinblue_settings')
        {

            //require_once('vendor/autoload.php');

            $mailin = new Mailin("https://api.sendinblue.com/v2.0", $api_key);
            $data = array( "to" => array( $to => $to_name),
                "from" => array($from_email, $from_name),
                "subject" => $subject,
                "html" => $body
            );

            $result = $mailin->send_email($data);
            if ($result['code'] == 'failure')
            {
                echo json_encode([ 'err' => 1,  'message' =>  $result['message'] ]);
                exit;
            }

            if ( $result['code'] == 'success' )
            {
                if (strpos( $result['message'], 'has not been activated') !== false)
                {
                    echo json_encode([ 'err' => 1,  'message' =>  $result['message'] ]);
                    exit;
                }else{
                    // insert data
                    $insert = DB::table('email_sender')
                        ->insert([
                            'title' => $title,
                            'user_id' => Auth::user()->id,
                            'api_key' => $request->public_api_key,
                            'from_name' => $from_name,
                            'from_email' => $from_email,
                            'type' => 'mailinblue',
                            'status' => 1,
                        ]);

                    if ($insert)
                    {
                        echo json_encode(['success' => 1]);
                    }
                }
            }
        }

        if ($object == 'amazon_settings')
        {

            require_once('vendor/autoload.php');

            $ses = new SimpleEmailService(
                $request->access_key, // your AWS access key id
                $request->access_key, // your AWS secret access key
                'us-west-2' // AWS region, default is us-east-1
            );

            $identities = $ses->listIdentities(); // string[]

            echo json_encode([ 'err' => 1,  'message' =>  $identities->code ]);
            exit;
        }


        if ($object == 'dyn_settings')
        {
            $mm = new MessageManagement($api_key);

            // setup the message

            try {
                $mail = new Mail();

                $mail->setFrom($from_email, $from_name)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->setBody($body);

                // send it
                $mm->send($mail);
            }catch(\Exception $e)
            {

             $err = $e->getMessage();
            }

            if (isset($err))
            {
                echo json_encode([ 'err' => 1,  'message' =>  $err ]);
                exit;
            }

        }

        if ($object == 'mailjet_settings')
        {

            require 'vendor/autoload.php';

            //$mj = new \Mailjet\Client(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),
            $mj = new \Mailjet\Client($request->public_api_key, $request->private_api_key,
                true,['version' => 'v3.1']);
            $data = [
                'Messages' => [
                    [
                        'From' => [
                            'Email' => $from_email,
                            'Name' => $from_name
                        ],
                        'To' => [
                            [
                                'Email' => $to,
                                'Name' => $to_name
                            ]
                        ],
                        'Subject' => $subject,
                        'TextPart' => "",
                        'HTMLPart' => $body
                    ]
                ]
            ];
            $response = $mj->post(Resources::$Email, ['body' => $data ]);
            //$response->success();

            if ( $response->success() )
            {
                // insert data
                $insert = DB::table('email_sender')
                    ->insert([
                        'title' => $title,
                        'user_id' => Auth::user()->id,
                        'api_key' => $request->public_api_key,
                        'private_api_key' => $request->private_api_key,
                        'from_name' => $from_name,
                        'from_email' => $from_email,
                        'type' => 'mailjet',
                        'status' => 1,
                    ]);

                if ($insert)
                {
                    echo json_encode(['success' => 1]);
                }
            }else{
                //print_r($response->getStatus());
               $data = $response->getData();

                if (isset( $data['ErrorMessage'] ))
                {

                    echo json_encode([ 'err' => 1,  'message' =>  $data['ErrorMessage'] ]);
                    exit;
                }else{
                    if (isset($data['Messages'][0]['Errors'][0]['ErrorMessage']))
                    {
                        echo json_encode([ 'err' => 1, 'message' => $data['Messages'][0]['Errors'][0]['ErrorMessage'] ]);
                    }
                }
            }
        }// end mailjet




        // SMTP Configration
        if ($object == 'smtp_settings')
        {

            $host = $request->host;
            $port = $request->port;
            $user = $request->username;
            $password = $request->password;
            $ssl = $request->ssl;


            require 'vendor/autoload.php';
            try {
                $mail = new PHPMailer();
                //Enable SMTP debugging.
                $mail->SMTPDebug = false;
                //Set PHPMailer to use SMTP.
                $mail->isSMTP();
                //Set SMTP host name
                $mail->Host = $host;

                //Set this to true if SMTP host requires authentication to send email
                $mail->SMTPAuth = true;
                //Provide username and password
                $mail->Username = $user;
                $mail->Password = $password;
                //If SMTP requires TLS encryption then set it
                if ($ssl == "")
                {
                    $mail->SMTPSecure = "tls";
                }else{
                    $mail->SMTPSecure = "ssl";
                }
                //Set TCP port to connect to
                $mail->Port = $port;
                // $mail->test = '25';
                $mail->SMTPOptions = array('ssl' => array('verify_host' => false, 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));

                $mail->From = $from_email;
                $mail->FromName = $from_name;

                $mail->addAddress( $to , $to_name);

                $mail->isHTML(true);

                $mail->Subject = $subject;
                $mail->Body = $body;
                $mail->AltBody = "";
                $send = $mail->send();
            }
            catch(\Exception $e)
            {
                //dd($e->getMessage());
            }
            if (!$send)
            {
                $err = explode('.', $mail->ErrorInfo );
                echo json_encode([ 'err' => 1,  'message' =>  $err[0].'!' ]);
            }
            else
            {
                $insert = DB::table('email_sender')
                    ->insert([
                        'title' => $request->title,
                        'host' =>$request->host,
                        'port' =>$request->port,
                        'user' =>$request->username,
                        'password' =>$request->password,
                        'smtp_ssl' => $request->ssl!=""? 1 : 0,
                        'user_id' => Auth::user()->id,
                        'from_name' => $from_name,
                        'from_email' => $from_email,
                        'type' => 'smtp',
                        'status' => 1,
                    ]);

                if ($insert)
                {
                    echo json_encode(['success' => 1]);
                }
            }
        }

        if ($object == 'sendgrid_settings')
        {

                $str = trim(preg_replace('/\s+/'," ",$body));
                $b = preg_replace("/\"/", "\\\"", $str);
                $uri = 'https://api.sendgrid.com/v3/mail/send';
                $json = '{
            "personalizations": [
                    {
                        "to": [{
                                "email": "'.$to.'",
                                "name":"'.$to_name.'"
                        }]
                    }
                ],
                "from": {
                    "email": "'.$from_email.'",
                    "name":"'.$from_name.'"
                },
                "subject": "'.$subject.'",
                "content": [
                    {
                    "type": "text/html",
                    "value": "'.$b.'"
                }
            ]
        }';

            $headers = array(
                'Authorization: Bearer '.$api_key.'',
                'Content-Type: application/json'
            );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $uri);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                curl_close($ch);

            //$result = json_encode( array('df' => 1));
            $res = json_decode($result);

                if (isset($res->errors))
                {
                    $err = $res->errors;
                    echo json_encode(['err' => 1, 'message' => $err[0]->message]);
                }else{
                    // insert data
                    $insert = DB::table('email_sender')
                        ->insert([
                            'title' => $title,
                            'user_id' => Auth::user()->id,
                            'api_key' => $api_key,
                            'from_name' => $from_name,
                            'from_email' => $from_email,
                            'type' => 'sendgrid',
                            'status' => 1,
                        ]);

                    if ($insert)
                    {
                        echo json_encode(['success' => 1]);
                    }
                }
            }

        if ($object == 'sparkpost_settings')
        {

            $str = trim(preg_replace('/\s+/'," ",$body));
            $b = preg_replace("/\"/", "\\\"", $str);
            $uri = 'https://api.sparkpost.com/api/v1/transmissions';
            $json = '{
        "options": {
            "open_tracking": true,
            "click_tracking": true
        },
        "metadata": {
            "some_useful_metadata": ""
        },
        "substitution_data": {
            "signature": ""
        },
        "recipients": [
            {
                "address": {
                    "email": "'.$to.'"
                },
                "tags": [
                    "learning"
                ],
                "substitution_data": {
                    "customer_type": "Platinum",
                    "first_name": "'.$to_name.'"
                }
            }
        ],
        "content": {
            "from": {
                "name": "'.$from_name.'",
                "email": "'.$from_email.'"
            },
            "subject": "'.$subject.'",
            "text": "",
            "html": "'.$b.'"
        }
    }';
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $uri);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',"Authorization: $api_key"));

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $result = curl_exec($ch);
            curl_close($ch);
            //$result = json_encode( array('df' => 1));
            $res = json_decode($result);
            if (isset($res->errors))
            {
                $err = $res->errors;
                echo json_encode(['err' => 1, 'message' => $err[0]->message]);
            }else{
                // insert data
                $insert = DB::table('email_sender')
                    ->insert([
                        'title' => $title,
                        'user_id' => Auth::user()->id,
                        'api_key' => $api_key,
                        'from_name' => $from_name,
                        'from_email' => $from_email,
                        'type' => 'sparkpost',
                        'status' => 1,
                    ]);

                if ($insert)
                {
                    echo json_encode(['success' => 1]);
                }
            }

            }

        if ($object == 'mailgun_settings')
        {

            define('MAILGUN_URL', 'https://api.mailgun.net/v3/'.$domain.'');
            define('MAILGUN_KEY', $api_key);

            $to = $to;
            $toname = $to_name;
            $mailfromname = $from_name;
            $mailfrom = $from_email;
            $subject = $subject;
            $html = $body;
            $text = '';
            $tag = '';
            $replyto = env('MAIL_FROM_ADDRESS');

            $array_data = array(
                'from'=> $mailfromname .'<'.$mailfrom.'>',
                'to'=>$toname.'<'.$to.'>',
                'subject'=>$subject,
                'html'=>$html,
                'text'=>$text,
                'o:tracking'=>'yes',
                'o:tracking-clicks'=>'yes',
                'o:tracking-opens'=>'yes',
                'o:tag'=>$tag,
                'h:Reply-To'=>$replyto
            );
            $session = curl_init(MAILGUN_URL.'/messages');
            curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($session);
            curl_close($session);
            $results = json_decode($response, true);

            if (!isset($results['id']))
            {
                echo json_encode( ['err' => 1, 'message' => $results['message'] ] );
                exit;
            }elseif ($results == "")
            {
                echo json_encode( ['err' => 1, 'message' => 'Invalid credentials!' ]);
                exit;
            }
            elseif (isset($results['id']))
            {
                // insert data
                $insert = DB::table('email_sender')
                    ->insert([
                        'title' => $title,
                        'user_id' => Auth::user()->id,
                        'domain' => $domain,
                        'api_key' => $api_key,
                        'from_name' => $from_name,
                        'from_email' => $from_email,
                        'type' => 'mailgun',
                        'status' => 1,
                    ]);

                if ($insert)
                {
                    echo json_encode(['success' => 1]);
                }
            }

           //$results['message'];

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
