<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Categories;
use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class SubEmailCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.sub_category.list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.sub_category.add");
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Categories::where(['category' =>  $request->category, 'user_id' => Auth::user()->id])->value('id');

        if ($category){
            echo 'al';
        }else {


            $obj = new Categories();
            if ($request->id != '')
            {
                $obj = $obj->findOrFail($request->id);
            }
            $obj->fill($request->all());
            $obj->category_id = 3;
            $obj->type = 'manual';
            $obj->user_id = Auth::User()->id;
            if ($obj->save())
            {
                echo 1;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Categories::select('category', 'id', 'category_id')->where(['id' => $id, 'user_id' => Auth::user()->id])->first();

        return view("admin.sub_category.show", compact('category'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function loadEdit(Request $request)
    {
        $data = Categories::findOrFail($request->id);
        if ($data){
            echo json_encode($data);
        }else{
            echo 0;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // assign category

    function assignCategory(Request $request)
    {

        $category = Categories::select(['id', 'category'])->where('user_id', Auth::user()->id)->where('id', '!=', $request->id )->get();
        return view("admin.sub_category.select_category", compact('category'));
    }

    function saveAssignCategory(Request $request)
    {

         EmailTemplate::where(['email_category_id' => $request->tobedelete, 'user_id' => Auth::user()->id ])->update(['email_category_id' => $request->assigned]);
         DB::table('autoresponder_new_stats')->where(['email_category_id' => $request->tobedelete, 'user_id' => Auth::user()->id ])->update(['email_category_id' => $request->assigned]);
         DB::table('email_broadcasts')->where(['email_category_id' => $request->tobedelete, 'user_id' => Auth::user()->id ])->update(['email_category_id' => $request->assigned]);
         DB::table('email_subjects')->where(['sub_category_id' => $request->tobedelete, 'user_id' => Auth::user()->id ])->update(['sub_category_id' => $request->assigned]);
         Categories::where('user_id', Auth::user()->id)->where('id', '=', $request->tobedelete )->delete();

       echo 1;

    }

}
