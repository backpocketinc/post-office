<?php

namespace App\Http\Controllers\Admin;

use App\SubjectTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;



class SubjectTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubjectTemplate::all();
        $data = array();

        $email_category = DB::table('email_category')->select('*')->get();

        return view("admin.subject_template.list", ['data'=>$data, 'email_category' => $email_category ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.subject_template.add");
    }


    public function importForm()
    {
        return view("admin.subject_template.Import");
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj = new SubjectTemplate();
        if ($request->id!='')
        {
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());
        $obj->user_id = Auth::User()->id;
        if ($obj->save())
        {
            echo 1;
        }else{
            echo 0;
        }
    }


    public function storeImport(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SubjectTemplate::findOrFail($id);
        return view("admin.new_email_template.add", ['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
