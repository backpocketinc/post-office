<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        /*'category',
        'category/loadEdit',
        'users',
        'user/loadEdit',
        'delete',
        'changeStatus',
        'choose-subject',
        'email_content_by_id',
        'browse-gallery',
        'download-email',
        'profile-settings',
        'load-searched-email',
        'email-settings',
        'view-email-content',// 5-5
        'load-broadcast-chart',//5-5
        'email_content_by_id2', //
        'download-email2',//
        'load-searched-email2',//
        'callback',
        'get-aweber-token',
        'aweber-callback',
        'get-aweber-auth',
        'aweber-callback-auth',
        'contact_info',
        //adding user from outer form request get/post any
        'add-user',
        'user-stats',
        'fetch-emails',
        'sharing-code-gen',
        'fetch-shared-emails',
        'delete-email',
        'email-sub-category/loadEdit',
        'email-sub-category',
        'private-email',
        'type-wise-stats',
        'save-assign-category',
        'send-password',
        'email_sender_settings',
        'sync_stats'*/
    ];
}
