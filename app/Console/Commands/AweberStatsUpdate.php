<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\AweberController;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AweberStatsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aweber:update_stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating Aweber Statics after sending.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        $txt = "John Doe\n";
        fwrite($myfile, $txt);
        $txt = "Jane Doe\n";
        fwrite($myfile, $txt);
        fclose($myfile);

       /*$obj = new AweberController();
        $obj->UpdateStats();*/
    }
}
