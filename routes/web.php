<?php

/*
clear cache
*/

Route::get('/clear-cache', function() {
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    echo 'view, cache, route cache cleared successfully!';
});


Route::get('/', function () {
    if(!Auth::User()){
        if(file_exists(storage_path('installed')))
        {
            return view('auth.login');
            die();
        }else{
            header("location: public/install");
            die();
        }

    }else{
        return redirect('home');
    }
});

Route::any('callback', 'CallBackController@callBack');
Route::any('add-user', 'Admin\UserController@store');

Route::post('send-password', 'Admin\UserController@userSendPassword')->name('send-password');

Route::get('find-synonyms/{word?}', 'Admin\WordApiController@index');
Route::get('mailchimp', 'Admin\MailChimpController@index');
Route::get('cron',      'CronController@index');
Route::get('fetch-emails-cron',      'CronController@fetchEmailsCron')->name('fetch-emails-cron');
Route::get('cronejob-autoresponder-contacts', 'CronController@fetchAutorespondersContacts')->name('cronejob-autoresponder-contacts');

Route::get('schedule-autoresponders', 'CronController@scheduleAutoresponders');
Route::get('schedule-aweber-broadcast', 'CronController@scheduleAweberBroadcast');
Route::get('test-cron', 'CronController@testCron');
Route::get('aweber-fetch-contacts', 'CronController@aweberFetchContacts');
Route::get('getresponse-fetch-contacts', 'CronController@getresponseFetchContacts');
Route::get('mailchimp-fetch-contacts', 'CronController@mailchimpFetchContacts');
Route::get('activecampaign-fetch-contacts', 'CronController@activecampaignFetchContacts');
// auth autoresponders
Route::get('autoresponders-lists', 'CronController@autoresponderFetchLists');

// test

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('home', 'HomeController@index')->name('home');

    Route::get('help', 'Admin\HelpController@index')->name('help');
    Route::get('link_tracker', 'HomeController@linkTracker');
    Route::get('link_tracker_settings', 'HomeController@linkTrackerSettings');
    Route::post('link_tracker_settings_store', 'HomeController@linkTrackerSettingsStore');
});
// home chart
Route::post('load-broadcast-chart',  'HomeController@broadcastChart')->name('load-broadcast-chart');

// common routes for admin and user
Route::group(['namespace'=>'Admin','middleware' => ['auth']], function(){

    Route::resource('category',  'CategoryController');
    Route::resource('new-template',  'NewEmailTemplateController');
    Route::resource('new-template2',  'NewEmailTemplateController2');
    Route::resource('settings',  'SettingController');
    Route::resource('gen_settings',  'SettingNewController');
    Route::resource('gallery',  'GalleryController');

    Route::resource('email-header-footer-settings',  'EmailHeaderFooterController');
    Route::resource('contact_info',  'ContactInfoController');
    Route::resource('report',  'ReportController');
    Route::resource('ar-campaigns',  'ArCampaignController');
    Route::resource('email-sub-category',  'SubEmailCategoryController');
    Route::resource('contacts',  'ContactsController');
    Route::resource('app-settings',  'AppSettingsController');

    Route::resource('contact-list',  'ContactListController');
    Route::get('load_list', 'ContactListController@loadList')->name('load_list');

    /* update */
    Route::get('/update-version', function (){
        return view('admin.update.index');
    });
    Route::post('updates', 'AppSettingsController@serverUpdate');


    Route::get('profile',  'UserController@loadProfileSetting')->name('profile');
    Route::post('profile-settings',  'UserController@profileSetting')->name('profile-settings');

    Route::post('browse-gallery',  'GalleryController@browseGallery')->name('browse-gallery');

    Route::get('choose-subject',       'NewEmailTemplateController@searchSubject')->name('choose-subject');
    Route::post('email_content_by_id',  'NewEmailTemplateController@loadEmailContentByID')->name('email_content_by_id');
    Route::post('download-email',       'NewEmailTemplateController@downloadEmail')->name('download-email');
    Route::post('load-searched-email',  'NewEmailTemplateController@loadSearchedEmail')->name('load-searched-email');
    Route::post('view-email-content',  'NewEmailTemplateController@viewEmailContent')->name('view-email-content'); /// added  5-5

    Route::post('fetch-emails',  'SettingNewController@fetchEmails')->name('fetch-emails'); //setting controller
    Route::post('sharing-code-gen',  'SettingNewController@sharingCodeGenerator')->name('sharing-code-gen'); //setting controller

    Route::get('cpanel-account',  'SettingNewController@cpanelAccount')->name('cpanel-account'); //setting controller # server creation

    // my emails
    Route::get('shared-emails',  'ArCampaignController@sharedEmails')->name('shared-emails');
    Route::post('fetch-shared-emails',  'ArCampaignController@fetchSharedEmails')->name('fetch-shared-emails');

    // your email contents
    Route::get('load-your-email-contents',  'AjaxController@loadYourEmailContent')->name('load-your-email-content');

    Route::get('load-my-emails',  'AjaxController@loadMyEmails')->name('load-my-emails');
    Route::get('load-shared-emails',  'AjaxController@loadSharedEmails')->name('load-shared-emails');
    Route::get('shared-emails-detail',  'AjaxController@sharedEmailsDetail')->name('shared-emails-detail');
    Route::get('load-contacts',  'AjaxController@loadContacts')->name('load-contacts');
    Route::get('load-lists',  'AjaxController@loadLists')->name('load-lists');
    Route::get('load-open-by_emails',  'AjaxController@loadOpenByEmail')->name('load-open-by_emails');
    Route::get('load-unopen-by_emails',  'AjaxController@loadUnopenByEmail')->name('load-unopen-by_emails');

    Route::get('opened-report',  'ContactsController@showOpenReport');
    Route::get('unopened-report',  'ContactsController@showUnOpenReport');

    // load_ar_campains list of get-response/aweber
    Route::get('load_ar_api_key',  'NewEmailTemplateController@selectArApi')->name('load_ar_api_key');
    Route::get('load_ar_campaings',  'NewEmailTemplateController@loadArCampaings')->name('load_ar_campaings');


    Route::post('get-aweber-token',  'AweberController@getAccessToken')->name('get-aweber-token');
    Route::any('aweber-callback',  'AweberController@aweberCallback')->name('aweber-callback');

    Route::post('get-aweber-auth',  'AweberNewController@getAccessToken')->name('get-aweber-auth');
    Route::any('aweber-callback-auth',  'AweberNewController@aweberCallback')->name('aweber-callback-auth');

    Route::post('delete',  'AjaxController@delete')->name('delete');
    Route::post('delete-email',  'AjaxController@deleteEmail')->name('delete-email');

    // test
    Route::get('report',  'ReportController@index')->name('report');

    Route::get('autoresponder-report',  'ReportController@reportById')->name('autoresponder-report');
    Route::get('type-wise-stats',  'ReportController@typewiseReport')->name('type-wise-stats');

    Route::get('testreport',  'ReportController@test')->name('testreport');

    Route::post('private-email', 'AjaxController@privateEmail')->name('private-email');
    Route::get('load-sub-categories',  'AjaxController@loadSubCategories')->name('load-sub-categories');
    Route::get('load-sub-categories-emails',  'AjaxController@loadSubCategoriesEmails')->name('load-sub-categories-emails');
    Route::get('show_boradcast_error',  'AjaxController@showBoradcastError')->name('show_boradcast_error');
    Route::post('sync_stats',  'AjaxController@syncStats')->name('sync_stats');

    Route::post('email-sub-category/loadEdit',  'SubEmailCategoryController@loadEdit');
    Route::get('assign-category',  'SubEmailCategoryController@assignCategory')->name('assign-category');
    Route::post('save-assign-category',  'SubEmailCategoryController@saveAssignCategory')->name('save-assign-category');

    Route::post('email_sender_settings',  'SettingController@emailSenderSettings')->name('email_sender_settings');

});

Route::group(['namespace'=>'Admin','middleware' => ['auth','checkAdmin']], function(){

    Route::resource('library', 'SubjectTemplateController');
    Route::resource('email-template',  'EmailTemplateController');
//    Route::resource('category',  'CategoryController');
    Route::resource('users',  'UserController');
    Route::resource('email-settings',  'UserEmailSettings');


    Route::post('user/loadEdit',  'UserController@loadEdit');
    Route::post('category/loadEdit',  'CategoryController@loadEdit');

    Route::get('load-categories',  'AjaxController@loadCategories')->name('load-categories');

    Route::get('load-users',  'AjaxController@loadUsers')->name('load-users');

    Route::get('load-subjects',  'AjaxController@loadSubjects')->name('load-subjects');
    Route::get('load-email-content',  'AjaxController@loadEmailContent')->name('load-email-content');

    Route::post('changeStatus',  'AjaxController@changeStatus')->name('changeStatus');
    Route::get('load-emailtracking-stats',  'AjaxController@loadTrackingStats')->name('load-emailtracking-stats');

    Route::post('user-stats',  'UserController@userStats')->name('user-stats');

    Route::post('email-settings-store', 'UserEmailSettings@saveEmailSettings')->name('email-settings-store');
    Route::post('test-email', 'UserEmailSettings@testEmail')->name('test-email');

});